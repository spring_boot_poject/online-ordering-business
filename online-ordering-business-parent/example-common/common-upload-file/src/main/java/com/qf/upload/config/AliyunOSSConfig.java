package com.qf.upload.config;


import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * @author 51993
 */
@Configuration
public class AliyunOSSConfig {

    @Resource
    private OSSConfigProperties properties;

    /**
     * OSS对象注入到容器
     *
     * @return
     */
    @Bean
    public OSS oss() {
        return new OSSClientBuilder()
                .build(properties.getEndpoint(), properties.getAccessKeyId(), properties.getAccessKeySecret());
    }
}
