package com.qf.locked.aspect;

public enum LockTypeEnum {
    //可重入锁。公平锁、联锁、红锁、读写锁
    REENTRANT, FAIR, MULTI, RED, READ_WRITE
}
