package com.qf.locked.aspect.annotation;

import com.qf.locked.aspect.LockTypeEnum;
import com.qf.locked.aspect.ReadWriteEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface DistributedLock {

    //锁的类型
    LockTypeEnum lockType() default LockTypeEnum.REENTRANT;

    //锁的名称（redis键的名称）
    String lockName() default "";

    //锁的名称组合（紧联锁红锁）
    String[] lockNames() default "";

    //lockType为读写锁判断是读锁还是写锁
    ReadWriteEnum ReadWrite() default ReadWriteEnum.READ;

    //加锁的超时时间
    long waitTime() default 0;

    //（看门狗）守护线程最长等待时间
    long LeaseTime() default 0;

    //时间单位（默认秒）
    TimeUnit unit() default TimeUnit.SECONDS;
}
