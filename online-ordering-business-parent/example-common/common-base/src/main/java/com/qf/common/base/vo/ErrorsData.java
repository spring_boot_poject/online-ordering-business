package com.qf.common.base.vo;

import lombok.Data;

@Data
public class ErrorsData {
    private String msg;
    private String fildName;

    public ErrorsData() {
    }

    public ErrorsData(String msg, String fildName) {
        this.msg = msg;
        this.fildName = fildName;
    }
}
