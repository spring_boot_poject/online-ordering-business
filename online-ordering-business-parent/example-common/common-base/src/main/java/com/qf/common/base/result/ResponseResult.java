package com.qf.common.base.result;


import lombok.Data;

/**
 * 返回结果封装类
 *
 * @author zhangwei
 */
@Data
public class ResponseResult<T> {

    /**
     * 返回码
     */
    private int status;

    /**
     * 返回说明
     */
    private String msg;
    /**
     * 返回数据
     */
    private T data;

    private ResponseResult() {
    }

    private ResponseResult(T data) {
        this();
        this.data = data;
    }

    private ResponseResult(ResultCode code, T data) {
        this.status = code.retCode;
        this.msg = code.retMsg;
        this.data = data;
    }

    private ResponseResult(ResultCode code) {
        this.status = code.retCode;
        this.msg = code.retMsg;
    }

    /**
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponseResult<T> success(T data) {
        return success(ResultCode.SUCCESS, data);
    }

    /**
     * @param codeEnum
     * @param data
     * @param <T>
     * @return
     */
    public static <T> ResponseResult<T> success(ResultCode codeEnum, T data) {
        return new ResponseResult<>(codeEnum, data);
    }

    public static <T> ResponseResult<T> error() {
        return new ResponseResult<T>(ResultCode.ERROR);
    }


    public static <T> ResponseResult<T> error(ResultCode codeEnum) {
        return error(codeEnum, null);
    }

    public static <T> ResponseResult<T> error(ResultCode codeEnum, T data) {
        return new ResponseResult<>(codeEnum, data);
    }
}
