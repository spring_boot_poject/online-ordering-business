package com.qf.validation.config;

import org.hibernate.validator.HibernateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.validation.Validation;
import javax.validation.Validator;

@Configuration
public class ValidationConfig {

    /**
     * 开启快速校验模式（性能好一点）
     * @return
     */
    @Bean
    public Validator validator(){
       return Validation.byProvider(HibernateValidator.class).
               configure().failFast(true)
               .buildValidatorFactory()
               .getValidator();

    }
}
