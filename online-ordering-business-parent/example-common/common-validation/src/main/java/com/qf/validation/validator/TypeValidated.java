package com.qf.validation.validator;


import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.Objects;

/**
 * A entends Annotation  表示关联的校验注解
 * T 校验的数据类型
 */
public class TypeValidated implements ConstraintValidator<TypeValidator, Objects> {

    private String[] values;
    @Override
    public void initialize(TypeValidator constraintAnnotation) {

        //使用注解时固定的值
        this.values= constraintAnnotation.value().split(",");
    }

    @Override
    public boolean isValid(Objects s, ConstraintValidatorContext constraintValidatorContext) {

        return Arrays.asList(values).contains(s);
    }
}
