package com.qf.validation.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD,ElementType.PARAMETER})
@Constraint(validatedBy = StateValidated.class)
public @interface StateValidator {
    int[] value();
    String message() default "错误参数";
    Class CLASS() default String.class;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
