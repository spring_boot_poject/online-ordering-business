package com.qf.validation.validator;


import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD,ElementType.METHOD,ElementType.PARAMETER})
@Constraint(validatedBy = TypeValidated.class)
public @interface TypeValidator {

    String value();
    String message() default "必须是规定的字段";
    Class CLASS() default String.class;
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
