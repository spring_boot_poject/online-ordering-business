package com.qf.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;

/**
 * @ClassName StateValidated
 * @Description 自定义注解规定状态字段
 * @Author zhenyang
 * @Date 2022/4/1 9:43
 **/
public class StateValidated implements ConstraintValidator<StateValidator,Integer> {

  int[] values=null;
    @Override
    public void initialize(StateValidator constraintAnnotation) {
       this.values=constraintAnnotation.value();

       Arrays.sort(this.values);
    }

    @Override
    public boolean isValid(Integer integer, ConstraintValidatorContext constraintValidatorContext) {
        return Arrays.binarySearch(values,integer)<0? false:true;
    }
}
