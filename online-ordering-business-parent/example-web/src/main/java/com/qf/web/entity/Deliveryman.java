package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "deliveryman")
public class Deliveryman {
    public static final String COL_DELIVERYMAN_SCORE = "deliveryman_score";
    @TableId(value = "deliveryman_id", type = IdType.INPUT)
    private Integer deliverymanId;

    /**
     * 订单编号
     */
    @TableField(value = "order_id")
    private Integer orderId;

    /**
     * 配送员名称
     */
    @TableField(value = "deliveryman_name")
    private String deliverymanName;

    /**
     * 送达时间
     */
    @TableField(value = "delivery_time")
    private Date deliveryTime;

    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_DELIVERYMAN_ID = "deliveryman_id";

    public static final String COL_ORDER_ID = "order_id";

    public static final String COL_DELIVERYMAN_NAME = "deliveryman_name";

    public static final String COL_DELIVERY_TIME = "delivery_time";

    public static final String COL_IS_DELETE = "is_delete";
}