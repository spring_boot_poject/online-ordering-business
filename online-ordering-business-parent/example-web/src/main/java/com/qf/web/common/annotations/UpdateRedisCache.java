package com.qf.web.common.annotations;


import java.lang.annotation.*;

/**
 * 注解用于标识当更新记录时清除redis中的缓存信息
 * @author 谢积威
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface UpdateRedisCache{
    /**
     * key的前缀
     */
    String keyPrefix() default "";

}
