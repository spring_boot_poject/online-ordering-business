package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.GoodsPicture;
import org.apache.ibatis.annotations.Param;

public interface GoodsPictureMapper extends BaseMapper<GoodsPicture> {

    /**
     * 添加商品id 和商品主图(is_delete = 1 代表为主图)。
     * @param goodsPicture
     * @return
     */
    int insertPicture(GoodsPicture goodsPicture);


    /**
     * 添加商品id ，并添加商品副图 (is_delete = 2 代表为副图)
     * @param goodsPicture
     * @return
     */
    int insertAuxiliaryPicture(GoodsPicture goodsPicture);

    /**
     * 根据商品id 伪删除对应的该商品的所有图片
     * @param goodsId
     * @return
     */
    int deletePictureByGoodsId(int goodsId);


    /**
     * 根据商品id 恢复对应的该商品的所有图片
     * @param goodsId
     * @return
     */
    int regainPictureByGoodsId(int goodsId);



    /**
     * 根据商品id查询对应的商品图片信息
     * @return
     */
    GoodsPicture selectGoodsInfoByGoodsId(@Param("goodsId") int goodsId);

    /**
     * 修改图片信息
     * @param
     * @return
     */
    int updatePicture(GoodsPicture goodsPicture);


    /**
     * 根据商品图片id 将主图变为副图
     * @param goodsPictureId
     * @return
     */
    int updateMasterPicture(int goodsPictureId);


    /**
     * 根据商品图片id 将副图变为主图
     * @param goodsPictureId
     * @return
     */
    int updateAuxiliaryPicture(int goodsPictureId);



}