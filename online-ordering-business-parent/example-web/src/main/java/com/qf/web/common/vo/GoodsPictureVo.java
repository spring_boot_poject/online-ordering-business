package com.qf.web.common.vo;

import lombok.Data;

@Data
public class GoodsPictureVo {
    /**
     * 图片url
     */
    private String goodsPictureImg;
}
