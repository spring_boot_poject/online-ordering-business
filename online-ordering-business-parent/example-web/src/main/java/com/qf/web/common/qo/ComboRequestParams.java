package com.qf.web.common.qo;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class ComboRequestParams {

    /**
     * 主键
     */
    private Integer comboId;

    /**
     * 套餐名
     */
    private String comboName;

    /**
     * 套餐单人价格
     */
    private BigDecimal comboSinglePrice;

    /**
     * 套餐多人价格
     */
    private BigDecimal comboMultiPrice;

    /**
     * 套餐类型，1单人团，0多人团
     */
    private Integer comboType;

    /**
     * 套餐多人团人数
     */
    private Integer comboNumber;

    /**
     * 套餐开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date comboStartDate;

    /**
     * 套餐结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date comboEndDate;

    /**
     * 套餐排除开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date comboExcludeStartDate;

    /**
     * 套餐排除结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date comboExcludeEndDate;

    /**
     * 使用星期
     */
    private String comboUseWeek;

    /**
     * 使用时间类型，1全天，0自定义
     */
    private Integer useTimeType;

    /**
     * 是否需要预约，1需要，0不需要
     */
    private Integer isOrder;

    /**
     * 套餐
     */
    private List<GoodsQo> goodsList;

    /**
     * 店铺ID
     */
    private Integer shopId;

    /**
     * 图片ID数组
     */
    private List<ComboImageRequestParams> images;

    /**
     * 使用时间
     */
    private String useTime;

    /**
     * 使用规则
     */
    private List<Integer> ruleIds;

    /**
     * 是否开启优惠
     */
    private Integer enable;

}
