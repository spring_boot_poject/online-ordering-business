package com.qf.web.service;

import com.qf.web.common.qo.FlowingWaterRequestParams;
import com.qf.web.common.qo.ShopRequestParams;
import com.qf.web.common.qo.ShopkeeperQo;
import com.qf.web.common.vo.FlowingWaterVo;

import java.util.List;

public interface FlowingWaterService {

    /**
     * 流水注册
     * @param shopRequestParams
     * @return
     */
    Integer registerFlowingWater(ShopRequestParams shopRequestParams);

    /**
     * 根据店铺id查询提现流水
     * @param flowingWaterRequestParams
     * @return
     */
    List<FlowingWaterVo> selectFlowingWater(FlowingWaterRequestParams flowingWaterRequestParams);

    /**
     * 根据店铺id自定义日期查询提现流水
     * @param flowingWaterRequestParams
     * @return
     */
    List<FlowingWaterVo> selectDateFlowingWater(FlowingWaterRequestParams flowingWaterRequestParams);

    /**
     * 流水注册
     * @param shopkeeperQo
     * @return
     */
    Integer registerUpFlowingWater(ShopkeeperQo shopkeeperQo);

    /**
     * 根据店铺id查询提现充值
     * @param flowingWaterRequestParams
     * @return
     */
    List<FlowingWaterVo> selectUpFlowingWater(FlowingWaterRequestParams flowingWaterRequestParams);

    /**
     * 根据店铺id自定义日期查询提现流水
     * @param flowingWaterRequestParams
     * @return
     */
    List<FlowingWaterVo> selectDateUpFlowingWater(FlowingWaterRequestParams flowingWaterRequestParams);

}
