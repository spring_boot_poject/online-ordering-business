package com.qf.web.mapper;
import cn.hutool.core.date.DateTime;
import com.qf.web.common.vo.ScoreAllVo;
import com.qf.web.common.vo.ScoreInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface ScoreAllMapper {
    //1根据商店id查看所有的评价
    List<ScoreAllVo> checkScoreAllByShopId(int shopId);

    //2根据评论id查看评论详情
    ScoreInfoVo selectScoreInfoByScoreId(int scoreId);

    //3根据店铺id查看未回复的所有评价
    List<ScoreAllVo> selectScoreAllByShopIdNo(int shopId);

    //4根据评价id查询未回复的评论详情
    ScoreInfoVo selectNoScoreInfoByScoreId(int scoreId);

    //5根据店铺id获取最近30天的平均分（店铺评分）
    ScoreAllVo selectScoreAvg(int shopId);

    //6根据店铺id查看指定时间段的所有评论
    List<ScoreAllVo> selectScoreAllTimeByShopId(@Param("shopId") int shopId, @Param("endDate") String endDate, @Param("beginDate") String beginDate);

    //7根据店铺id查看指定的星级评价（所有评价）
    List<ScoreAllVo> selectScoreByShopId(@Param("shopId") int shopId, @Param("scoreStarId") int scoreStarId);

    //8根据店铺id查看指定的星级评价（未回复的评价）
    List<ScoreAllVo> selectScoreNoByShopId(@Param("shopId") int shopId, @Param("scoreStarId") int scoreStarId);

    //9根据店铺id查看中差评的总数
    Integer selectBadScoreCountByShopId(@Param("shopId") int shopId);

    //10根据店铺id查看好评的总数
    Integer selectGoodScoreCountByShopId(@Param("shopId") int shopId);


}
