package com.qf.web.common.vo;

import com.qf.web.entity.GoodsPicture;
import com.qf.web.entity.GoodsType;
import lombok.Data;

import java.util.List;

/**
 * 显示指定的店铺所有商品信息
 */
@Data
public class ShopGoodsVo {
    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品名
     */
    private String goodsName;

    /**
     * 商品简介
     */
    private String goodsIntroduce;

    /**
     * 店铺id
     */
    private Integer shopId;

    /**
     * 商品价格
     */
    private Integer goodsPrice;

    /**
     * 规格模板id
     */
    private Integer templateId;

    /**
     * 包装费
     */
    private Integer goodsPackingFee;

    /**
     * 商品类别对象
     */
    private GoodsType goodsType;

    /**
     * 图片对象信息
     */
    private List<GoodsPicture> goodsPicture;
}
