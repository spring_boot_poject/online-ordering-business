package com.qf.web.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class ComboRuleVo {

    /**
     * 主键
     */
    @TableId(value = "rule_id", type = IdType.INPUT)
    private Integer ruleId;

    /**
     * 店铺ID，记录哪个店铺创建的规则
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 规则内容
     */
    @TableField(value = "rule_context")
    private String ruleContext;


}
