package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "score")
public class Score {
    /**
     * 评价id
     */
    @TableId(value = "score_id", type = IdType.INPUT)
    private Integer scoreId;

    /**
     * 订单id
     */
    @TableField(value = "order_id")
    private Integer orderId;

    /**
     * 用户id
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 用户名字
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * 评价时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "score_time")
    private Date scoreTime;

    /**
     * 评价内容
     */
    @TableField(value = "score_message")
    private String scoreMessage;

    /**
     * 味道评价
     */
    @TableField(value = "score_taste")
    private Double scoreTaste;

    /**
     * 包装评价
     */
    @TableField(value = "score_pack")
    private Double scorePack;

    /**
     * 配送评分
     */
    @TableField(value = "score_delivery")
    private Double scoreDelivery;

    /**
     * 用户评分
     */
    @TableField(value = "score")
    private Double score;


    /**
     * 回复内容
     */
    @TableField(value = "reply_message")
    private String replyMessage;

    /**
     * 回复状态：1=已回复；0=未回复
     */
    @TableField(value = "reply_message_state")
    private Integer replyMessageState;

    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_SCORE_ID = "score_id";

    public static final String COL_ORDER_ID = "order_id";

    public static final String COL_USER_ID = "user_id";

    public static final String COL_USER_NAME = "user_name";

    public static final String COL_SCORE_TIME = "score_time";

    public static final String COL_SCORE_MESSAGE = "score_message";

    public static final String COL_SCORE_TASTE = "score_taste";

    public static final String COL_SCORE_PACK = "score_pack";

    public static final String COL_SCORE_DELIVERY = "score_delivery";

    public static final String COL_SCORE = "score";

    public static final String COL_REPLY_MESSAGE = "reply_message";

    public static final String COL_REPLY_MESSAGE_STATE = "reply_message_state";

    public static final String COL_IS_DELETE = "is_delete";
}