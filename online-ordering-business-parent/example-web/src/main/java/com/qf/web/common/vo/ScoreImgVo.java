package com.qf.web.common.vo;

import lombok.Data;

@Data
public class ScoreImgVo {
    //评价图片的id
    private Integer scoreImgId;
    //评论图片
    private String img;

}
