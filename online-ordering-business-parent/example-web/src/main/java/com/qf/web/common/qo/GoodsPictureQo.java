package com.qf.web.common.qo;

import lombok.Data;

/**
 * 添加图片的Qo 对象
 */

@Data
public class GoodsPictureQo {
    /**
     * 商品id
     */
    private Integer goodsId;
    /**
     * 图片url
     */
    private String goodsPictureImg;

}
