package com.qf.web.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.web.common.qo.OrdersRequestParams;
import com.qf.web.common.vo.ShopOrdersVo;

import java.util.List;
import com.qf.web.common.vo.OrderBeanListVo;

public interface OrderService {

    /**
     * 生成一个订单的详细信息
     * @param shopId
     * @return
     */
    ResponseResult selectOrderBean(Integer shopId);

    /**
     * 生成一个未完成订单的详细信息
     * @param shopId
     * @return
     */
    ResponseResult selectOrderBeanList(Integer shopId);

    /**
     * 定义状态生成订单的所有的状态
     * @param state
     * @return
     */
    ResponseResult<OrderBeanListVo> selectOrderByState(Integer shopId, Integer state);

    /**
     * 定义订单的退款状态
     *
     * @param shopId
     * @param refund 退款状态 退款状态 0 正常订单  1 顾客退款  2 商家拒绝退款  3 商家同意退款
     * @return
     */
    ResponseResult<OrderBeanListVo> selectOrderByRefund(Integer shopId, Integer refund);

    /**
     * 查询历史订单完成交易
     * @param shopId
     * @return
     */
    ResponseResult<OrderBeanListVo> selectOrderSuccess(Integer shopId);

    /**
     * 商家未处理或者取消的订单
     * @param shopId
     * @return
     */
    ResponseResult<OrderBeanListVo> selectOrderIgnore(Integer shopId);

    /**
     * 查看配送的历史订单数据
     * @param shopId
     * @return
     */
    ResponseResult<OrderBeanListVo> selectHistoryOrder(Integer shopId);



    /**
     * 生成一个订单的详细信息
     * @param shopId
     * @return
     */
    ResponseResult selectOrderBeanListTake(Integer shopId);

    /**
     * 定义状态生成订单的所有的状态
     * @param state
     * @return
     */
    ResponseResult<OrderBeanListVo> selectOrderByStateTake(Integer shopId, Integer state);

    /**
     * 定义订单的退款状态
     *
     * @param shopId
     * @param refund 退款状态 退款状态 0 正常订单  1 顾客退款  2 商家拒绝退款  3 商家同意退款
     * @return
     */
    ResponseResult<OrderBeanListVo> selectOrderByRefundTake(Integer shopId, Integer refund);

    /**
     * 查询历史订单完成交易
     * @param shopId
     * @return
     */
    ResponseResult<OrderBeanListVo> selectOrderSuccessTake(Integer shopId);

    /**
     * 商家未处理或者取消的订单
     * @param shopId
     * @return
     */
    ResponseResult<OrderBeanListVo> selectOrderIgnoreTake(Integer shopId);

    /**
     * 查看配送的历史订单数据
     * @param shopId
     * @return
     */
    ResponseResult<OrderBeanListVo> selectHistoryOrderTake(Integer shopId);

    /**
     * 商户取消订单
     * @param orderId
     * @return
     */
    ResponseResult updateOrderCancel(Integer orderId);

    /**
     * 商家接受订单
     * @param orderId
     * @return
     */
    ResponseResult updateOrderAccept(Integer orderId);

    /**
     * 商家同意退款
     * @param orderId
     * @return
     */
    ResponseResult updateOrderAgree(Integer orderId);

    /**
     * 商家不同意退款
     * @param orderId
     * @return
     */
    ResponseResult updateOrderDisagree(Integer orderId);

    /**
     * 根据店铺id查询每日交易流水信息
     * @param ordersRequestParams
     * @return
     */
    List<ShopOrdersVo> selectShopOrders(OrdersRequestParams ordersRequestParams);

    /**
     * 根据时间查询当日店铺流水信息
     * @param ordersRequestParams
     * @return
     */
    List<ShopOrdersVo> selectDayOrders(OrdersRequestParams ordersRequestParams);

    /**
     * 根据店铺id查询当日的预收入
     * @param ordersRequestParams
     * @return
     */
    ShopOrdersVo selectIncomeAdvance(OrdersRequestParams ordersRequestParams);

    /**
     * 根据店铺id查询范围内的订单流水
     * @param ordersRequestParams
     * @return
     */
    List<ShopOrdersVo> selectRangeOrders(OrdersRequestParams ordersRequestParams);

    /**
     * 根据店铺id查询近一个月的订单流水
     * @param ordersRequestParams
     * @return
     */
    List<ShopOrdersVo> selectMonthOrders(OrdersRequestParams ordersRequestParams);
}
