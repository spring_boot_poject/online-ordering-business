package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.ScoreDeliveryman;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ScoreDeliverymanMapper extends BaseMapper<ScoreDeliveryman> {
    //1根据评价id删除指定的评价
    Integer updateScoreDeliverymanByScoreId(int ScoreDeliverymanId);
}