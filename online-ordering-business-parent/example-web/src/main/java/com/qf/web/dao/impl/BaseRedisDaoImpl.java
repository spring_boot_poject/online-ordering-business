package com.qf.web.dao.impl;

import com.qf.web.dao.BaseRedisDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * @author 51993
 */
@Repository
public class BaseRedisDaoImpl implements BaseRedisDao {

    @Autowired
    RedisTemplate<String,Object> redisTemplate;

    @Autowired
    ValueOperations<String,Object> valueOperations;

    @Override
    public Boolean delete(String key) {
        return redisTemplate.delete(key);
    }

    @Override
    public Object getForValue(String key) {
        return valueOperations.get(key);
    }

    @Override
    public void setForValue(String key, Object value, Duration timeout) {
        valueOperations.set(key,value,timeout);
    }

    @Override
    public void setForValue(String key, Object value, long timeout, TimeUnit unit) {
        valueOperations.set(key,value,timeout,unit);
    }


}
