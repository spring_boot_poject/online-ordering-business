package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.SpecsTemplateRequestParams;
import com.qf.web.common.qo.TemplateGoodsSpecsRequestParams;
import com.qf.web.entity.GoodsSpecsPo;
import com.qf.web.entity.SpecsPo;
import com.qf.web.entity.SpecsTemplatePo;
import com.qf.web.entity.TemplateGoodsSpecsPo;
import com.qf.web.common.vo.SpecsTemplateVo;
import com.qf.web.mapper.GoodsSpecsMapper;
import com.qf.web.mapper.SpecsMapper;
import com.qf.web.mapper.SpecsTemplateMapper;
import com.qf.web.service.SpecsTemplateService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class SpecsTemplateServiceImpl implements SpecsTemplateService {
    private static final Integer NUMBER_ZERO=0;
    @Resource
    SpecsTemplateMapper specsTemplateMapper;

    @Resource
    GoodsSpecsMapper goodsSpecsMapper;

    @Resource
    SpecsMapper specsMapper;

    @Override
    public List<SpecsTemplateVo> showCommonSpecsTemplateList() {
        List<SpecsTemplatePo> list = specsTemplateMapper.selectSpecsTemplateByShopId(0);
        if (list.size()==0){
            throw new ServiceException(ResultCode.DATA_IS_WRONG);
        }
        ArrayList<SpecsTemplateVo> specsTemplateVos = new ArrayList<>();
        for (SpecsTemplatePo specsTemplatePos:list) {
            SpecsTemplateVo specsTemplateVo = new SpecsTemplateVo();
            BeanUtils.copyProperties(specsTemplatePos,specsTemplateVo);
            boolean b = specsTemplateVos.add(specsTemplateVo);
            if (!b){
                throw new ServiceException(ResultCode.DATA_IS_WRONG);
            }
        }
        return specsTemplateVos;
    }

    @Override
    public List<SpecsTemplateVo> showShopSpecsTemplateList(Integer shopId) {
        List<SpecsTemplatePo> list = specsTemplateMapper.selectSpecsTemplateByShopId(shopId);
        if (list.size()==NUMBER_ZERO){
            throw new ServiceException(ResultCode.DATA_IS_WRONG);
        }
        ArrayList<SpecsTemplateVo> specsTemplateVos = new ArrayList<>();
        for (SpecsTemplatePo specsTemplatePos:list) {
            SpecsTemplateVo specsTemplateVo = new SpecsTemplateVo();
            BeanUtils.copyProperties(specsTemplatePos,specsTemplateVo);
            boolean b = specsTemplateVos.add(specsTemplateVo);
            if (!b){
                throw new ServiceException(ResultCode.DATA_IS_WRONG);
            }
        }
        return specsTemplateVos;
    }

    @Override
    public Integer insertTemplate(SpecsTemplateRequestParams specsTemplateRequestParams) {


        //获取模板id
        Integer specsTemplateId = specsTemplateRequestParams.getSpecsTemplateId();
        //根据模板id查找对应的规格信息（id）
        List<GoodsSpecsPo> goodsSpecsPos = goodsSpecsMapper.selectSpecsByTemplateId(specsTemplateId);
        //存储模板规格id和规格id的对象的集合
        List<TemplateGoodsSpecsPo> templateGoodsSpecsPos = new ArrayList<>();
        
        SpecsTemplatePo specsTemplatePo = new SpecsTemplatePo();
        BeanUtils.copyProperties(specsTemplateRequestParams,specsTemplatePo);
        //插入模板返回id
        Integer integer = specsTemplateMapper.insertTemplate(specsTemplatePo);
        if (integer==NUMBER_ZERO || integer==null){
            throw new ServiceException(ResultCode.DATA_IS_WRONG);
        }
        //获取插入模板数据的id
        Integer newSpecsTemplateId = specsTemplatePo.getSpecsTemplateId();
        //将新的模板id及规格放到集合里面去
        for (GoodsSpecsPo goodsSpecsPo:goodsSpecsPos) {
            templateGoodsSpecsPos.add(new TemplateGoodsSpecsPo(newSpecsTemplateId,goodsSpecsPo.getGoodsSpecsId()));
        }
        //插入将模板id及规格id中间表中去
        Integer integer1 = goodsSpecsMapper.insertGoodSpecsTemplateRelation(templateGoodsSpecsPos);
        if (integer1==NUMBER_ZERO || integer==null){
            throw new ServiceException(ResultCode.ERROR);
        }
        return newSpecsTemplateId;
    }

    @Override
    public Integer insertTemplateCustom(SpecsTemplateRequestParams specsTemplateRequestParams){
        SpecsTemplatePo specsTemplatePo = new SpecsTemplatePo();
        BeanUtils.copyProperties(specsTemplateRequestParams,specsTemplatePo);

        //存储模板规格id和规格id的对象的集合
        List<TemplateGoodsSpecsPo> templateGoodsSpecsPosList = new ArrayList<>();

        //插入模板数据
        Integer integer = specsTemplateMapper.insertTemplate(specsTemplatePo);

        if (integer==NUMBER_ZERO || integer==null){
            throw new ServiceException(ResultCode.ERROR);
        }
        List<GoodsSpecsPo> goodsSpecsPosList = specsTemplatePo.getGoodsSpecsPos();
        //用来存储id不为0的goodsSpecsPo
        ArrayList<GoodsSpecsPo> newGoodsPeecsPoList = new ArrayList<>();
        for (GoodsSpecsPo goodsSpecsPo:goodsSpecsPosList) {
            if (goodsSpecsPo.getGoodsSpecsId()!=null){
                //添加新的模板id和已有的规格到集合中
                templateGoodsSpecsPosList.add(new TemplateGoodsSpecsPo(specsTemplatePo.getSpecsTemplateId(),goodsSpecsPo.getGoodsSpecsId()));
            }else {
                //添加规格并返回id
                Integer integer1 = goodsSpecsMapper.insertGoodsSpecs(goodsSpecsPo);
                if (integer1==null || integer==NUMBER_ZERO){
                    throw new ServiceException(ResultCode.ERROR);
                }
                //添加新的模板id和新的规格到集合中
                templateGoodsSpecsPosList.add(new TemplateGoodsSpecsPo(specsTemplatePo.getSpecsTemplateId(),goodsSpecsPo.getGoodsSpecsId()));
                //获取新的规格详细信息
                List<SpecsPo> specsPos = goodsSpecsPo.getSpecsPos();
                for (SpecsPo specsPo:specsPos) {
                    specsPo.setGoodsSpecsId(goodsSpecsPo.getGoodsSpecsId());
                    Integer integer2 = specsMapper.inserspecs(specsPo);
                    if (integer2==null || integer==NUMBER_ZERO){
                        throw new ServiceException(ResultCode.ERROR);
                    }
                }
            }
        }
        //添加中间表数据（）
        Integer integer1 = goodsSpecsMapper.insertGoodSpecsTemplateRelation(templateGoodsSpecsPosList);
        if (integer1==NUMBER_ZERO || integer==null){
            throw new ServiceException(ResultCode.ERROR);
        }
        return specsTemplatePo.getSpecsTemplateId();
    }

    @Override
    public SpecsTemplateVo selectTSpecsTemplate(Integer templateId) {
        SpecsTemplatePo specsTemplatePo = specsTemplateMapper.selectSpecsTemplateByTemolateId(templateId);

        SpecsTemplateVo specsTemplateVo = new SpecsTemplateVo();

        BeanUtils.copyProperties(specsTemplatePo,specsTemplateVo);

        return specsTemplateVo;
    }

    @Override
    public Integer addGoodsSpecsTemplateRelation(List<TemplateGoodsSpecsRequestParams> templateGoodsSpecsRequestParamsList) {
        ArrayList<TemplateGoodsSpecsPo> templateGoodsSpecsPosList = new ArrayList<>();
        for (TemplateGoodsSpecsRequestParams templateGoodsSpecsRequestParams:templateGoodsSpecsRequestParamsList) {
            TemplateGoodsSpecsPo templateGoodsSpecsPo = new TemplateGoodsSpecsPo();
            BeanUtils.copyProperties(templateGoodsSpecsRequestParams,templateGoodsSpecsPo);
            templateGoodsSpecsPosList.add(templateGoodsSpecsPo);
        }
        Integer integer = goodsSpecsMapper.insertGoodSpecsTemplateRelation(templateGoodsSpecsPosList);
        if (integer==NUMBER_ZERO){
            throw new ServiceException(ResultCode.ERROR);
        }
        return integer;
    }

    @Override
    public Integer updateTemplate(SpecsTemplateRequestParams specsTemplateRequestParams) {
        SpecsTemplatePo specsTemplatePo = new SpecsTemplatePo();
        BeanUtils.copyProperties(specsTemplateRequestParams,specsTemplatePo);
        Integer integer = specsTemplateMapper.updateSpecsTemplate(specsTemplatePo);
        if (integer==NUMBER_ZERO){
            throw new ServiceException(ResultCode.ERROR);
        }
        return integer;
    }

    @Override
    public Integer deleteTemplate(List<Integer> templateIds) {
        //删除模板信息
        Integer integer = specsTemplateMapper.deleteTemplateById(templateIds);
        if (integer==NUMBER_ZERO){
            throw new ServiceException(ResultCode.ERROR);
        }
    /*    for (Integer templateId:templateIds) {
            //查找规格模板对应的规格信息
            List<SpecsTemplatePo> specsTemplatePos = specsTemplateMapper.selectSpecsTemplateAndGoodsSpecs(templateId);
            if (specsTemplatePos.size()==0){
                throw new ServiceException(ResultCode.ERROR);
            }
            for (SpecsTemplatePo specsTemplatePo:specsTemplatePos) {
                //查询出对应的规格
                List<GoodsSpecsPo> goodsSpecsPosLlit = specsTemplatePo.getGoodsSpecsPos();
                if (goodsSpecsPosLlit.size()==0){
                    throw new ServiceException(ResultCode.ERROR);
                }
                //删除对应的规格信息
                Integer integer1 = goodsSpecsMapper.deleteGoodsSpecsById(goodsSpecsPosLlit);
                if (integer1==0){
                    throw new ServiceException(ResultCode.ERROR);
                }
               //删除对应的规格详细信息
                Integer integer2 = specsMapper.deleteSpecsByGoodsSpecsIds(goodsSpecsPosLlit);
                if (integer==0){
                    throw new ServiceException(ResultCode.ERROR);
                }
            }

        }*/
        //删除关联表信息
        specsTemplateMapper.deleteTemplateRelationByTemplateId(templateIds);
        GoodsSpecsPo goodsSpecsPo = new GoodsSpecsPo();
        return integer;
    }

    @Override
    public Integer addUserTemplate(SpecsTemplateRequestParams specsTemplateRequestParams) {
        SpecsTemplatePo specsTemplatePo = new SpecsTemplatePo();
        BeanUtils.copyProperties(specsTemplateRequestParams,specsTemplatePo);
        Integer integer = specsTemplateMapper.insertTemplate(specsTemplatePo);
        if (integer==NUMBER_ZERO){
            throw new ServiceException(ResultCode.ERROR);
        }
        return specsTemplatePo.getSpecsTemplateId();
    }

}
