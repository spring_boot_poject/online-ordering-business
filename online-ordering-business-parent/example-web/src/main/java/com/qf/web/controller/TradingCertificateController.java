package com.qf.web.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.TradingCertificateRequestParams;
import com.qf.web.common.utils.ConstantUtils;
import com.qf.web.common.vo.TradingCertificateVo;
import com.qf.web.service.TradingCertificateService;
import com.qf.web.service.UploadService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author banxiaowen
 */
@RestController
@RequestMapping("/trading")
public class TradingCertificateController {
    @Resource
    private TradingCertificateService certificateService;
    @Resource
    private UploadService uploadService;

    @PostMapping("/register/trading")
    public ResponseResult<TradingCertificateVo> registerTrading(@RequestBody TradingCertificateRequestParams tradingCertificateRequestParams){
        TradingCertificateVo tradingCertificateVo = certificateService.registerTradingCertificate(tradingCertificateRequestParams);
        return ResponseResult.success(tradingCertificateVo);
    }

    @PostMapping("trading/upload")
    public ResponseResult<ResultCode> registerTradingImg(@RequestBody MultipartFile multipartFile, TradingCertificateRequestParams tradingCertificateRequestParams){
        String imgUrl = uploadService.uploadImage(multipartFile, ConstantUtils.TRADING_IMAGE_BUCKET_NAME);
        tradingCertificateRequestParams.setTradingImageUrl(imgUrl);
        Integer rSet = certificateService.registerTradingImg(tradingCertificateRequestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
