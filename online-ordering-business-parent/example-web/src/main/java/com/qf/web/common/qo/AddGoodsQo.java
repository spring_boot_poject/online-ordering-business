package com.qf.web.common.qo;

import lombok.Data;

//添加商品
@Data
public class AddGoodsQo {
    /**
     * 商品名
     */
    private String goodsName;

    /**
     * 店铺id
     */
    private Integer shopId;

    /**
     * 商品简介
     */
    private String goodsIntroduce;

    /**
     * 商品类别id
     */
    private Integer goodsTypeId;

    /**
     * 商品价格
     */
    private Integer goodsPrice;

    /**
     * 规格模板id
     */
    private Integer templateId;

    /**
     * 包装费
     */
    private Integer goodsPackingFee;

    /**
     * 图片url
     */
    private String goodsPictureImg;

}
