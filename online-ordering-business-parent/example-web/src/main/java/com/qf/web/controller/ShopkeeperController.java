package com.qf.web.controller;

import com.qf.common.base.exception.ControllerException;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.ShopRequestParams;
import com.qf.web.common.qo.ShopkeeperQo;
import com.qf.web.common.vo.ShopkeeperVo;
import com.qf.web.common.qo.ShopkeeperRequestParams;
import com.qf.web.common.utils.ConstantUtils;
import com.qf.web.common.utils.ShiroUtils;
import com.qf.web.service.MsgService;
import com.qf.web.service.ShopkeeperService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;

/**
 * @author 谢积威
 * @author banxiaowen
 */
@RestController
@RequestMapping("/banker")
@Slf4j
public class ShopkeeperController {

    @Autowired
    ShopkeeperService shopkeeperService;

    @Autowired
    MsgService msgService;

    /**
     * 登录
     * @param tel   登录手机号
     * @param credential    登录凭证，密码/验证码
     * @return
     */
    @PostMapping("/login")
    public ResponseResult<Serializable> codeLogin(String tel, String credential){
        UsernamePasswordToken token = new UsernamePasswordToken(tel, credential);
        try{
            SecurityUtils.getSubject().login(token);
        }catch (UnknownAccountException uae){
            //给前端提示用户不存在，让用户去注册
            throw new ControllerException(ResultCode.ACCOUNT_NOT_EXIST);
        }catch (LockedAccountException lae){
            //账户被锁定
            throw new ControllerException(ResultCode.USER_ACCOUNT_LOCKED);
        }catch (IncorrectCredentialsException ice){
            throw new ControllerException(ResultCode.ACCOUNT_LOGIN_ERROR);
        }catch (Exception e){
            throw new ControllerException(ResultCode.SYSTEM_ERROR);
        }
        //返回sessionID给客户端
        return ResponseResult.success(ShiroUtils.getSession().getId());
    }


    /**
     * 设置支付密码/修改支付密码
     * @param requestParams
     * @return
     */
    @PostMapping("/pay/update")
    public ResponseResult<ResultCode> updatePayPassword(@RequestBody ShopkeeperRequestParams requestParams){
        shopkeeperService.updateAccountPayPwd(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 根据前端传来的支付密码校验输入的密码是否正确
     * @param requestParams
     * @return
     */
    @GetMapping("/pay/check")
    public ResponseResult<ResultCode> checkPayPwd(@RequestBody ShopkeeperRequestParams requestParams){
        shopkeeperService.checkPayPwd(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 登录发送短信验证码
     * @param phone
     * @return
     */
    @GetMapping("/login/msg")
    public ResponseResult<ResultCode> sendMsg(String phone){
        shopkeeperService.sendMsg(phone, ConstantUtils.PHONE_LOGIN_KEY_PREFIX);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 修改手机号前需要通过短信验证码校验原手机号
     * @return
     */
    @GetMapping("/update/msg")
    public ResponseResult<ResultCode> updateMsg(){
        shopkeeperService.sendMsg(ShiroUtils.getShopkeeper().getShopkeeperPhone(),ConstantUtils.PHONE_UPDATE_KEY_PREFIX);
        return ResponseResult.success(ResultCode.SUCCESS);
    }


    /**
     * 退出登录
     * @return
     */
    @GetMapping("/logout")
    public ResponseResult<ResultCode> logout(){
        SecurityUtils.getSubject().logout();
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 获取当前登录用户的信息
     * @return
     */
    @GetMapping("/info")
    public ResponseResult<ShopkeeperVo> getInfo(){
        //根据已登录信息去数据库查询最新数据
        ShopkeeperVo shopkeeperVo = shopkeeperService.selectShopkeeperById(ShiroUtils.getShopkeeper().getShopkeeperId());
        return ResponseResult.success(shopkeeperVo);
    }

    /**
     * 修改登录密码
     * @return
     */
    @PostMapping("/pwd/update")
    public ResponseResult<ResultCode> updatePwd(@RequestBody ShopkeeperRequestParams requestParams){
        shopkeeperService.updateAccountPwd(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 校验登录密码
     * @param requestParams
     * @return
     */
    @GetMapping("/pwd/check")
    public ResponseResult<ResultCode> checkPwd(@RequestBody ShopkeeperRequestParams requestParams){
        shopkeeperService.checkAccountPwd(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 商户换绑手机号
     * @param requestParams
     * @return
     */
    @PostMapping("/phone/update")
    public ResponseResult<ResultCode> updatePhone(@RequestBody ShopkeeperRequestParams requestParams){
        shopkeeperService.updatePhone(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 忘记支付密码时，通过账号绑定的手机号发送短信验证码进行校验
     * @return
     */
    @GetMapping("/msg/pay")
    public ResponseResult<ResultCode> sendPayMsg(@RequestBody ShopkeeperRequestParams requestParams){
        shopkeeperService.sendForgetMsg(requestParams.getShopkeeperPhone(),ConstantUtils.PAY_FORGET_KEY_PREFIX);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 校验忘记支付密码的验证码是否正确
     * @param requestParams 接收前端传递的参数信息
     * @return
     */
    @PostMapping("/msg/pay/verify")
    public ResponseResult<ResultCode> verifyPayCode(@RequestBody ShopkeeperRequestParams requestParams){
        shopkeeperService.verifyMsg(requestParams,ConstantUtils.PAY_FORGET_KEY_PREFIX);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 忘记登录密码时通过短信验证码找回
     * @param phone
     * @return
     */
    @GetMapping("/msg/account")
    public ResponseResult<ResultCode> sendAccountForgetMsg(String phone){
        shopkeeperService.sendMsg(phone,ConstantUtils.ACCOUNT_FORGET_KEY_PREFIX);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 校验找回密码
     * @param phone
     * @param code
     * @return
     */
    @PostMapping("/msg/account/verify")
    public ResponseResult<ResultCode> verifyAccountMsg(String phone,String code){
        shopkeeperService.verifyAccountMsg(phone,code);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 更新商户信息
     * @param requestParams
     * @return
     */
    @PostMapping("/profile/update")
    public ResponseResult<ResultCode> updateProfile(@RequestBody ShopkeeperRequestParams requestParams){
        shopkeeperService.updateShopkeeper(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    @PostMapping("/update/shopkeeper/wallet")
    public ResponseResult<ResultCode> updateShopkeeperWallet(@RequestBody ShopkeeperQo shopkeeperQo){
        Integer rSet = shopkeeperService.updateShopkeeperWallet(shopkeeperQo);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

}
