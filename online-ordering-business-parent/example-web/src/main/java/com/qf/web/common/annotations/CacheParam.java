package com.qf.web.common.annotations;

import java.lang.annotation.*;

/**
 * 用于在参数列表中标识QO对象
 * @author 谢积威
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CacheParam {
    /**
     * 标识的属性名
     */
    String value() default "";
}
