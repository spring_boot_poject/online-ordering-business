package com.qf.web.common.qo;

import com.qf.web.entity.FlowingWater;
import lombok.Data;

import java.util.Date;

@Data
public class ShopkeeperQo extends FlowingWater {
    /**
     * 商家id
     */
    private Integer shopkeeperId;

    /**
     * 商家昵称
     */
    private String shopkeeperName;

    /**
     * 商家电话
     */
    private String shopkeeperPhone;

    /**
     * 商家登录密码
     */
    private String shopkeeperPassword;

    /**
     * 商家支付密码
     */
    private String shopkeeperPayPassword;

    /**
     * 商家头像
     */
    private String shopkeeperImg;

    /**
     * 商户注册时间
     */
    private Date registerTime;

    private Integer money;

}
