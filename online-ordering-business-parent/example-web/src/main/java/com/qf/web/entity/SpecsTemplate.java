package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "specs_template")
public class SpecsTemplate {
    /**
     * 商品规格模板 id
     */
    @TableId(value = "specs_template_id", type = IdType.INPUT)
    private Integer specsTemplateId;

    /**
     * 规格名称
     */
    @TableField(value = "specs_template_name")
    private String specsTemplateName;

    /**
     * 店铺id，index
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 伪删除
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_SPECS_TEMPLATE_ID = "specs_template_id";

    public static final String COL_SPECS_TEMPLATE_NAME = "specs_template_name";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_IS_DELETE = "is_delete";
}