package com.qf.web.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @ClassName FullMinusUse
 * @Description 优惠卷使用记录查询
 * @Author zhenyang
 * @Date 2022/3/31 0:31
 **/
@Data
@ToString
public class FullMinusUseVo {

    @TableId(value = "full_minus_id", type = IdType.INPUT)
    private Integer fullMinusId;


    /**
     * 满减金额
     */
    @TableField(value = "full_minus_money")
    private Double fullMinusMoney;



    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;


    /**
     * 订单id
     */
    @TableField(value = "order_id")
    private Integer orderId;


    /**
     * 订单生产时间
     */
    @TableField(value = "order_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date orderTime;















}
