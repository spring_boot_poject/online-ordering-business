package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.Score;

public interface ScoreMapper extends BaseMapper<Score> {
    //1根据评论id商家进行回复
    Integer  updateReplyMessageByScoreId(Score score);
}