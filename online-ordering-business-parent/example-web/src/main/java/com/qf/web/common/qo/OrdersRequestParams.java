package com.qf.web.common.qo;

import com.qf.web.entity.Orders;
import lombok.Data;

@Data
public class OrdersRequestParams extends Orders {
    private String time;
    /**
     * 范围查询的起始时间
     */
    private String startTimeDate;
    /**
     * 范围查询的末尾时间
     */
    private String endTimeDate;
}
