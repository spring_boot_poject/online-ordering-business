package com.qf.web.common.qo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qf.web.entity.FlowingWater;
import lombok.Data;

import java.util.Date;


@Data
public class ShopRequestParams extends FlowingWater {

    /**
     * 店铺id
     */
    private Integer shopId;

    /**
     * 店铺名
     */
    private String shopName;

    /**
     * 商家id
     */
    private Integer shopkeeperId;

    /**
     * 店铺简介
     */
    private String shopIntroduce;

    /**
     * 店铺地址
     */
    private String shopAddrees;

    /**
     * 店铺联系电话
     */
    private String shopPhone;

    /**
     * 店铺头像
     */
    private String shopImg;

    /**
     * 账户
     */
    private Integer shopAccount;

    /**
     * 起送金额
     */
    private Integer shopStartMoney;

    /**
     * 开始营业时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date shopStartTime;

    /**
     * 结束营业时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date shopEndTime;

    /**
     * 店铺状态。1代表营业，0代表休息
     */
    private Integer shopWorkState;

    /**
     * 伪删除
     */
    private Integer isDelete;

    /**
     * 店铺主营品类ID
     */
    private Integer categoryId;

    /**
     * 店铺类型ID
     */
    private Integer shopTypeId;

    /**
     * 法人身份信息ID
     */
    private Integer legalPersonId;

    /**
     * 店铺状态，1正常，0注销，2审核，3违规
     */
    private Integer status;

    /**
     * 配送方式
     */
    private String distributionMode;

    /**
     * 提现的金钱
     */
    private Integer money;

}
