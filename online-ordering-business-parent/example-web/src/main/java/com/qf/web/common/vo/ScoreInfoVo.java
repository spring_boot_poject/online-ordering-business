package com.qf.web.common.vo;
import com.qf.web.entity.ScoreImg;
import lombok.Data;
import java.util.Date;
import java.util.List;

@Data
public class ScoreInfoVo {
    //评分id
    private Integer scoreId;
    //订单id
    private Integer orderId;
    //商家评分
    private Integer score;
    //味道评分
    private Integer scoreTaste;
    //包装评分
    private Integer scorePack;
    //配送服务
    private Integer scoreDelivery;
    //店铺名称
    private String shopName;
    //评价内容
    private String scoreMessage;
    //评价图片
    private List<ScoreImg> scoreImgList;
    //评价时间
    private Date scoreTime;
    //商家回复评分
    private String replyMessage;
}
