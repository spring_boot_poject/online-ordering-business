package com.qf.web.common.utils;

import com.qf.web.common.vo.ShopkeeperVo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.session.Session;

/**
 * @author 51993
 */

public class ShiroUtils {

    public static final Integer ITERATIONS = 10;

    public static String encryption(String password,String salt){
        //加密的只能在构造方法时传参，因为它是在创建对象时就计算出了加密后的密码
        Md5Hash md5Hash = new Md5Hash(password,salt,ITERATIONS);
        return md5Hash.toHex();
    }

    public static ShopkeeperVo getShopkeeper(){
        ShopkeeperVo shopkeeper = (ShopkeeperVo) SecurityUtils.getSubject().getPrincipal();
        return shopkeeper;
    }

    public static Session getSession(){
        return SecurityUtils.getSubject().getSession();
    }


}
