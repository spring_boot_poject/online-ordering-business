package com.qf.web.config;


import com.qf.web.common.vo.ShopkeeperVo;
import com.qf.web.entity.Shopkeeper;
import com.qf.web.mapper.ShopkeeperMapper;
import org.apache.shiro.authc.*;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

/**
 * 手机号加密码验证
 * @author 51993
 */
public class UserPwdRealm extends AuthenticatingRealm {

    @Autowired
    ShopkeeperMapper shopkeeperMapper;

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {

        //获取前端传过来的手机号
        String tel = (String) authenticationToken.getPrincipal();

        Shopkeeper shopkeeper = shopkeeperMapper.selectShokeeperByTel(tel);

        if (ObjectUtils.isEmpty(shopkeeper)){
            throw new UnknownAccountException("用户不存在");
        }

        ShopkeeperVo shopkeeperVo = new ShopkeeperVo();
        BeanUtils.copyProperties(shopkeeper,shopkeeperVo);

        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(shopkeeperVo, shopkeeper.getShopkeeperPassword(), null, tel);

        return simpleAuthenticationInfo;
    }
}
