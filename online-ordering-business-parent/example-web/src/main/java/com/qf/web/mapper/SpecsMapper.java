package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.GoodsSpecsPo;
import com.qf.web.entity.SpecsPo;
import com.qf.web.entity.Specs;

import java.util.List;

public interface SpecsMapper extends BaseMapper<Specs> {

    /**
     * 插入规格详细信息
     *
     * @param specsPo
     * @return
     */
    Integer inserspecs(SpecsPo specsPo);

    /**
     * 伪删除规格详细信息
     *
     * @param specsId
     * @return
     */
    Integer deleteSpecsBySpecsId(Integer specsId);


    /**
     * 修改规格详细信息
     *
     * @param specsPo
     * @return
     */
    Integer updateSpecsById(SpecsPo specsPo);

    /**
     * 根据规格id获取对应的所有规格详细信息
     * @return
     * @param goodsSpecsId
     */
    List<SpecsPo> selectSpecsListByGoodsSpecsId(Integer goodsSpecsId);

    /**
     * 根据商品规格id伪删除规格详细信息
     * @param goodsSpecsPos
     * @return
     */
    Integer deleteSpecsByGoodsSpecsIds(List<GoodsSpecsPo> goodsSpecsPos);
}