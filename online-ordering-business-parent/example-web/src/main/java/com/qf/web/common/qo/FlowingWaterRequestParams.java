package com.qf.web.common.qo;

import com.qf.web.entity.FlowingWater;
import lombok.Data;

@Data
public class FlowingWaterRequestParams extends FlowingWater {
    private String start;

    private String end;
}
