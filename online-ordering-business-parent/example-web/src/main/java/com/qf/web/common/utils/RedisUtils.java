package com.qf.web.common.utils;

import lombok.Getter;

@Getter
public enum RedisUtils {
    STATUS("add:goodsInfo");

    protected String retMsg;

    RedisUtils(String retMsg) {
        this.retMsg = retMsg;
    }
}
