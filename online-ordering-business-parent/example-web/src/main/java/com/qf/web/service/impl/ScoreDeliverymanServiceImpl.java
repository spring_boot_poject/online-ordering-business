package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.mapper.ScoreDeliverymanMapper;
import com.qf.web.service.ScoreDeliverymanService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

@Service
public class ScoreDeliverymanServiceImpl implements ScoreDeliverymanService {
    @Resource
    ScoreDeliverymanMapper scoreDeliverymanMapper;
    //11根据评价id删除指定的评价
    @Override
    public Integer deleteScoreDeliverymanByScoreId(int ScoreDeliverymanId) {
        Integer integer = scoreDeliverymanMapper.updateScoreDeliverymanByScoreId(ScoreDeliverymanId);
        if (!ObjectUtils.isEmpty(integer)){
            return integer;
        }else {
            throw new ServiceException(ResultCode.ERROR);
        }

    }

}
