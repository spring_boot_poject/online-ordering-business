package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.Shop;
import com.qf.web.entity.ShopType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShopTypeMapper extends BaseMapper<ShopType> {

}