package com.qf.web.service;

import com.qf.web.common.qo.TradingCertificateRequestParams;
import com.qf.web.common.vo.TradingCertificateVo;

public interface TradingCertificateService {
    /**
     * 注册营业执照的信息
     * @param tradingCertificateRequestParams
     * @return
     */
    TradingCertificateVo registerTradingCertificate(TradingCertificateRequestParams tradingCertificateRequestParams);

    /**
     * 上传营业执照
     * @param tradingCertificateRequestParams
     * @return
     */
    Integer registerTradingImg(TradingCertificateRequestParams tradingCertificateRequestParams);
}
