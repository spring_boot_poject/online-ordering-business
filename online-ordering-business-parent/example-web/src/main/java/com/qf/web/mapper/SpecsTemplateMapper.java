package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.SpecsTemplatePo;
import com.qf.web.entity.SpecsTemplate;

import java.util.List;

public interface SpecsTemplateMapper extends BaseMapper<SpecsTemplate> {

    public List<SpecsTemplatePo> selectCommonSpecsTemplateByTemolateId(Integer templateId);

    public List<SpecsTemplatePo> selectSpecsTemplateAndGoodsSpecs(Integer temelateId);

    public List<SpecsTemplatePo> selectSpecsTemplateByShopId(Integer shopId);

    public SpecsTemplatePo selectSpecsTemplateByTemolateId(Integer templateId);

    Integer insertTemplate(SpecsTemplatePo specsTemplatePo);

    //修改规格模板信息（名）
    Integer updateSpecsTemplate(SpecsTemplatePo specsTemplatePo);

    //根据规格模板id伪删除规格模板信息
    Integer deleteTemplateById(List<Integer> templateIdList);
    //根据规格模板id伪删除规格模板中间表信息
    Integer deleteTemplateRelationByTemplateId(List<Integer> templateIdList);
}