package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.ScoreQo;
import com.qf.web.entity.Score;
import com.qf.web.mapper.ScoreMapper;
import com.qf.web.service.ScoreService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

@Service
public class ScoreServiceImpl implements ScoreService {
    @Resource
    ScoreMapper scoreMapper;
    //1根据评论id商家进行回复
    @Override
    public Integer updateReplyMessageByScoreId( ScoreQo scoreQo) {

        if (!ObjectUtils.isEmpty(scoreQo)){
            Score score = new Score();
            //qo转po
            BeanUtils.copyProperties(scoreQo,score);
            Integer integer = scoreMapper.updateReplyMessageByScoreId(score);
            //返回影响行数
            return integer;
        }else {
            throw new ServiceException(ResultCode.ERROR);
        }
    }
}
