package com.qf.web.entity;

import lombok.Data;

@Data
public class GoodsPicturePo {

    /**
     * 商品图片id
     */
    private Integer goodsPictureId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 图片url
     */
    private String goodsPictureImg;
}
