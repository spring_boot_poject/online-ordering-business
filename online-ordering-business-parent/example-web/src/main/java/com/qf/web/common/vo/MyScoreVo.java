package com.qf.web.common.vo;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;
//我的评价实体类
@Data
public class MyScoreVo {
    //评价id（评价配送员的）
    private Integer scoreDeliverymanId;
    //订单id
    private Integer orderId;
    //配送评分
    private Integer deliverymanScore;
    //配送员
    private String deliverymanName;
    //评价内容
    private String scoreDeliverymanMessage;
    //评价时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date scoreDeliverymanTime;

}
