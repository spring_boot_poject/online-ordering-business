package com.qf.web.common.vo;

import lombok.Data;

@Data
public class SpecsVo {

    private Integer specsId;
    private Integer goodsSpecsId;
    private String specsInfo;

}
