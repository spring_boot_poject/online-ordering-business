package com.qf.web.controller;

import com.qf.common.base.exception.ControllerException;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;

import com.qf.web.common.qo.OrdersRequestParams;
import com.qf.web.common.vo.ShopOrdersVo;
import com.qf.validation.validator.StateValidator;
import com.qf.web.common.vo.OrderBeanListVo;
import com.qf.web.service.OrderService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Author:zhaozhenyang
 * @author banxiaowen
 */
@RestController
@RequestMapping("/order")
@Validated
public class OrderController {

    @Resource
    OrderService orderService;




    /**
     * 查看一个店铺所有的订单信息
     * @param orderId
     * @return
     */
    @GetMapping("/order")
    public ResponseResult selectOrderById(@NotNull Integer orderId){
        ResponseResult result = orderService.selectOrderBean(orderId);

        return  result;
    }

    /**
     * 查看一个店铺所有的订单信息
     * @param shopId
     * @return
     */
    @GetMapping("/all/order")
    public ResponseResult selectAllOrder(@NotNull Integer shopId){
        ResponseResult result = orderService.selectOrderBeanList(shopId);

        return  result;
    }

    /**
     * 这里要加上前端的一个校验，检索信息 0，1，2，3，4 超出就会产生错误进行拦截
     * 待处理，配送中，待审核，商家未接单，商家取消订单
     * @param state
     * @return
     */
    @GetMapping("/order/state")
    public ResponseResult selectOrderByState(@NotNull  Integer shopId,@StateValidator({0,1,2,3,4}) Integer state){

        ResponseResult<OrderBeanListVo> result = null;
        try {
            result = orderService.selectOrderByState(shopId,state);
        } catch (Exception e) {

            throw new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }


    /**
     * 判断追踪订单的退款的状态
     * 要加上校验技术
     * 交易成功（正常未发生，订单完成） 支付拒绝退款（拒绝退款） 支付退款成功（统一退款） 顾客撤销退款（撤销退款）
     * @param shopId
     * @param refund  退款状态 0 正常订单  1 顾客退款  2 商家拒绝退款  3 商家同意退款  4 顾客取消退款
     * @return
     */
    @GetMapping("/order/refund")
    public ResponseResult selectOrderByRefund(@NotNull  Integer shopId,@StateValidator({0,1,2,3,4})Integer refund){

        ResponseResult<OrderBeanListVo> result = null;
        try {
            result = orderService.selectOrderByRefund(shopId, refund);
        } catch (Exception e) {
            throw new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }


    /**
     * 成功获得付款的订单的查询
     * @param shopId
     * @return
     */
    @GetMapping("/order/success")
    public ResponseResult selectOrderSuccess(@NotNull Integer shopId){

        ResponseResult<OrderBeanListVo> result = null;
        try {
            result = orderService.selectOrderSuccess(shopId);
        } catch (Exception e) {

            throw new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }


    /**
     * 商家未接受或者取消的订单查询
     * @param shopId
     * @return
     */
    @GetMapping("/order/ignore")
    public ResponseResult selectOrderIgnore(@NotNull Integer shopId){

        ResponseResult<OrderBeanListVo> result = null;
        try {
            result = orderService.selectOrderIgnore(shopId);
        } catch (Exception e) {

            throw new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     * 查看配送完成的历史订单
     * @param shopId
     * @return
     */
    @GetMapping("/history/order")
    public ResponseResult selectHistoryOrder(@NotNull Integer shopId){

        ResponseResult<OrderBeanListVo> result = null;
        try {
             result = orderService.selectHistoryOrder(shopId);
        } catch (Exception e) {
            throw new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }




    /**
     * 自提模式下查看一个店铺所有的订单信息
     * @param shopId
     * @return
     */
    @GetMapping("/all/order/take")
    public ResponseResult selectAllOrderTake(@NotNull Integer shopId){

        ResponseResult result = null;
        try {
            result = orderService.selectOrderBeanListTake(shopId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return  result;
    }

    /**
     * 自提模式的这里要加上前端的一个校验，检索信息 0，1，2，3，4 超出就会产生错误进行拦截
     * 待处理，配送中，待审核，商家未接单，商家取消订单
     * @param state
     * @return
     */
    @GetMapping("/order/state/take")
    public ResponseResult selectOrderByStateTake(@NotNull  Integer shopId,@StateValidator({0,1,2,3,4}) Integer state){

        ResponseResult<OrderBeanListVo> result = null;
        try {
            result = orderService.selectOrderByStateTake(shopId,state);
        } catch (Exception e) {

            throw new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }


    /**
     * 自提模式的判断追踪订单的退款的状态
     * 要加上校验技术
     * 交易成功（正常未发生，订单完成） 支付拒绝退款（拒绝退款） 支付退款成功（统一退款） 顾客撤销退款（撤销退款）
     * @param shopId
     * @param refund  退款状态 0 正常订单  1 顾客退款  2 商家拒绝退款  3 商家同意退款  4 顾客取消退款
     * @return
     */
    @GetMapping("/order/refund/take")
    public ResponseResult selectOrderByRefundTake(@NotNull  Integer shopId,@StateValidator({0,1,2,3,4}) Integer refund){

        ResponseResult<OrderBeanListVo> result = null;
        try {
            result = orderService.selectOrderByRefundTake(shopId, refund);
        } catch (Exception e) {
            throw new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }


    /**
     * 自提模式下成功获得付款的订单的查询
     * @param shopId
     * @return
     */
    @GetMapping("/order/success/take")
    public ResponseResult selectOrderSuccessTake(@NotNull Integer shopId){

        ResponseResult<OrderBeanListVo> result = null;
        try {
            result = orderService.selectOrderSuccessTake(shopId);
        } catch (Exception e) {

            throw new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }


    /**
     * 自提模式下商家未接受或者取消的订单查询
     * @param shopId
     * @return
     */
    @GetMapping("/order/ignore/take")
    public ResponseResult selectOrderIgnoreTake(@NotNull Integer shopId){

        ResponseResult<OrderBeanListVo> result = null;
        try {
            result = orderService.selectOrderIgnoreTake(shopId);
        } catch (Exception e) {

            throw new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     * 查看自提完成的历史订单
     * @param shopId
     * @return
     */
    @GetMapping("/history/order/take")
    public ResponseResult selectHistoryOrderTake(@NotNull Integer shopId){

        ResponseResult<OrderBeanListVo> result = null;
        try {
            result = orderService.selectHistoryOrderTake(shopId);
        } catch (Exception e) {
            throw new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     *商家取消订单
     * @param orderId
     * @return
     */
    @GetMapping("/order/cancel")
    public ResponseResult updateOrderCancel(@NotNull Integer orderId){

        ResponseResult result=null;
        try {
             result = orderService.updateOrderCancel(orderId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     * 商家接受订单
     * @param orderId
     * @return
     */
    @GetMapping("/order/accept")
    public ResponseResult updateOrderAccept(@NotNull Integer orderId){

        ResponseResult result=null;
        try {
           result = orderService.updateOrderAccept(orderId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }


    /**
     * 商家同意退款
     * @param orderId
     * @return
     */
    @GetMapping("/order/agree")
    public ResponseResult updateOrderAgree(@NotNull Integer orderId){

        ResponseResult result=null;
        try {
            result = orderService.updateOrderAgree(orderId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     * 商家取消退款
     * @param orderId
     * @return
     */
    @GetMapping("/order/disagree")
    public ResponseResult updateOrderDisagree(@NotNull Integer orderId){

        ResponseResult result=null;
        try {
            result = orderService.updateOrderDisagree(orderId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    @GetMapping("/order/select/water")
    public ResponseResult<List<ShopOrdersVo>> selectShopOrders(@RequestBody OrdersRequestParams ordersRequestParams){
        List<ShopOrdersVo> shopOrdersVos = orderService.selectShopOrders(ordersRequestParams);
        return ResponseResult.success(shopOrdersVos);
    }

    @GetMapping("/order/select/day/water")
    public ResponseResult<List<ShopOrdersVo>> selectDayOrders(@RequestBody OrdersRequestParams ordersRequestParams){
        List<ShopOrdersVo> shopOrdersVos = orderService.selectDayOrders(ordersRequestParams);
        return ResponseResult.success(shopOrdersVos);
    }

    @GetMapping("/order/select/income/advance")
    public ResponseResult<ShopOrdersVo> selectIncomeAdvance(@RequestBody OrdersRequestParams ordersRequestParams){
        ShopOrdersVo shopOrdersVo = orderService.selectIncomeAdvance(ordersRequestParams);
        return ResponseResult.success(shopOrdersVo);
    }

    @GetMapping("/order/select/range/water")
    public ResponseResult<List<ShopOrdersVo>> selectRangeOrders(@RequestBody OrdersRequestParams ordersRequestParams){
        List<ShopOrdersVo> shopOrdersVos = orderService.selectRangeOrders(ordersRequestParams);
        return ResponseResult.success(shopOrdersVos);
    }

    @GetMapping("/order/select/month/water")
    public ResponseResult<List<ShopOrdersVo>> selectMonthOrders(@RequestBody OrdersRequestParams ordersRequestParams){
        List<ShopOrdersVo> shopOrdersVos = orderService.selectMonthOrders(ordersRequestParams);
        return ResponseResult.success(shopOrdersVos);
    }
}
