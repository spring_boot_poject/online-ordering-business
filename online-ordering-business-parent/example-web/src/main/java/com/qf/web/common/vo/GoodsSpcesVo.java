package com.qf.web.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.web.entity.SpecsPo;
import lombok.Data;

import java.util.List;

@Data
public class GoodsSpcesVo {
    /**
     * 商品规格id
     */
    @TableId(value = "goods_specs_id", type = IdType.INPUT)
    private Integer goodsSpecsId;

    /**
     * 规格名称
     */
    @TableField(value = "goods_specs_name")
    private String goodsSpecsName;

    private List<SpecsPo> specsPos;


    @TableField(value = "shop_id")
    private Integer shopId;

}
