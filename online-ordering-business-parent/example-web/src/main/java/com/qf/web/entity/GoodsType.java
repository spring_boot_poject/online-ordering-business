package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "goods_type")
public class GoodsType {
    /**
     * 商品类别id
     */
    @TableId(value = "goods_type_id", type = IdType.INPUT)
    private Integer goodsTypeId;

    /**
     * 商品类别名称
     */
    @TableField(value = "goods_type_name")
    private String goodsTypeName;

    /**
     * 伪删除
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_GOODS_TYPE_ID = "goods_type_id";

    public static final String COL_GOODS_TYPE_NAME = "goods_type_name";

    public static final String COL_IS_DELETE = "is_delete";
}