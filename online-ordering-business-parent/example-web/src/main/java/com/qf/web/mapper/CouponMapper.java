package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.ActiveOrder;
import com.qf.web.entity.Coupon;
import com.qf.web.entity.CouponInfo;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

public interface CouponMapper extends BaseMapper<Coupon> {
    /**
     * 查询所有代金券
     *
     * @return
     */
    default List<Coupon> selectAllCoupon(Integer shopId) {
        QueryWrapper<Coupon> qw = new QueryWrapper<>();
        qw.eq(Coupon.COL_SHOP_ID,shopId);
        List<Coupon> coupons = this.selectList(qw);
        return coupons;
    }

    /**
     * 根据状态和创建时间查询代金卷
     *
     * @param coupon
     * @return
     */
    List<Coupon> selectCouponListByStatusAndReleaseDate(Coupon coupon);

    /**
     * 根据店铺id查询代金券使用记录
     *
     * @param shopId
     * @return
     */
    List<CouponInfo> selectCouponInfoByShopId(Integer shopId);

    /**
     * 根据店铺id查询活动订单
     *
     * @param shopId
     * @param orderDate
     * @return
     */
    ActiveOrder selectActiveOrdersCountByShopIdAndOrderDate(@Param("shopId") Integer shopId, @Param("orderDate") String orderDate);

    /**
     * 根据店铺id查询活动订单
     *
     * @param shopId
     * @param orderDate
     * @return
     */
    ActiveOrder selectOrderInfoByShopIdAndOrderTime(@Param("shopId")Integer shopId, @Param("orderDate")String orderDate);

    /**
     * 查询最近一周、一个月的活动订单信息
     * @param shopId
     * @param beforeTime
     * @param nowTime
     * @return
     */
    ActiveOrder selectActiveOrderInfoByShopIdAndNearTime(@Param("shopId") Integer shopId,@Param("beforeTime") String beforeTime,@Param("nowTime") String nowTime);

    ActiveOrder selectOrderInfoByShopIdAndNearTime(@Param("shopId") Integer shopId,@Param("beforeTime") String beforeTime,@Param("nowTime") String nowTime);

    /**
     * 修改代金券状态
     * @param couponId
     * @param statusNum
     * @return
     */
    Integer updateStatus(@Param("couponId")Integer couponId,@Param("statusNum") Integer statusNum);

    /**
     * 修改代金券发放状态
     * @param couponId
     * @param isDelete
     * @return
     */
    Integer updateGiveOutStatus(@Param("couponId") Integer couponId, @Param("isDelete") Integer isDelete);
}