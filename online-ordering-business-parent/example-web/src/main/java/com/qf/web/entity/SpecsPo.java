package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class SpecsPo {
    /**
     * 规格详细id
     */
    @TableId(value = "specs_id", type = IdType.INPUT)
    private Integer specsId;

    /**
     * 商品规格id
     */
    @TableField(value = "goods_specs_id")
    private Integer goodsSpecsId;

    /**
     * 规格详细信息
     */
    @TableField(value = "specs_info")
    private String specsInfo;


    public static final String COL_SPECS_ID = "specs_id";

    public static final String COL_GOODS_SPECS_ID = "goods_specs_id";

    public static final String COL_SPECS_INFO = "specs_info";

}
