package com.qf.web.service;

import com.qf.web.common.qo.ShopkeeperQo;
import com.qf.web.common.vo.ShopkeeperVo;
import com.qf.web.common.qo.ShopkeeperRequestParams;
import com.qf.web.entity.Shopkeeper;

/**
 * @author 51993
 */
public interface ShopkeeperService {
    /**
     * 更新商户信息
     * @param requestParams
     * @return
     */
    Integer updateShopkeeper(ShopkeeperRequestParams requestParams);


    /**
     * 校验支付密码是否输入正确
     * @param requestParams
     */
    void checkPayPwd(ShopkeeperRequestParams requestParams);


    /**
     * 根据ID查询指定商户的详细信息
     * @param id
     * @return
     */
    ShopkeeperVo selectShopkeeperById(Integer id);

    /**
     * 校验登录密码
     * @param requestParams
     */
    void checkAccountPwd(ShopkeeperRequestParams requestParams);

    /**
     * 修改商户登录密码
     * @param requestParams
     * @return
     */
    Integer updateAccountPwd(ShopkeeperRequestParams requestParams);

    /**
     * 设置商户支付密码
     * @param requestParams
     * @return
     */
    Integer updateAccountPayPwd(ShopkeeperRequestParams requestParams);

    /**
     * 更改手机号
     * @param requestParams
     * @return
     */
    Integer updatePhone(ShopkeeperRequestParams requestParams);

    /**
     * 忘记支付/登录密码给绑定手机号发短信
     * @param phone
     * @param key
     */
    void sendForgetMsg(String phone,String key);

    /**
     * 校验短信验证码是否正确
     * @param code  验证码
     * @param id    当前登录用户ID
     * @param key   校验类型
     */
    void verifyMsg(ShopkeeperRequestParams requestParams, String key);

    /**
     * 商户注册
     * @param requestParams
     * @return
     */
    Integer register(ShopkeeperRequestParams requestParams);

    /**
     * 根据手机号查询商户信息
     * @param tel
     * @return
     */
    Shopkeeper selectShopkeeperByPhone(String tel);

    /**
     * 账户找回密码发送验证码
     * @param phone
     * @param accountForgetKeyPrefix
     */
    void sendMsg(String phone, String accountForgetKeyPrefix);

    /**
     * 校验验证码是否正确并校验手机号是否存在
     * @param phone
     * @param code
     */
    void verifyAccountMsg(String phone, String code);

    /**
     * 提现操作中商家id和充值金钱来实现商家钱包增加
     * @return
     */
    Integer updateShopkeeperWallet(ShopkeeperQo shopkeeperQo);
}
