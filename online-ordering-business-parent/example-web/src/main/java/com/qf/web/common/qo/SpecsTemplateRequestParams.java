package com.qf.web.common.qo;

import com.qf.web.entity.GoodsSpecsPo;
import lombok.Data;

import java.util.List;

@Data
public class SpecsTemplateRequestParams {
    /**
     * 商品规格 id
     */

    private Integer specsTemplateId;

    /**
     * 规格名称
     */

    private String specsTemplateName;

    /**
     * 店铺id，index
     */

    private Integer shopId;

    private List<GoodsSpecsPo> goodsSpecsPos;


    public static final String COL_SPECS_TEMPLATE_ID = "specs_template_id";

    public static final String COL_SPECS_TEMPLATE_NAME = "specs_template_name";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_IS_DELETE = "is_delete";
}
