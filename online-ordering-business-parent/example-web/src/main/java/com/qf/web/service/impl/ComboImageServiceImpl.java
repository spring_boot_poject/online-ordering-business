package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.ComboImageRequestParams;
import com.qf.web.common.vo.ComboImageVo;
import com.qf.web.entity.ComboImage;
import com.qf.web.mapper.ComboImageMapper;
import com.qf.web.service.ComboImageService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ComboImageServiceImpl implements ComboImageService {

    @Autowired
    ComboImageMapper comboImageMapper;

    @Override
    public ComboImageVo addComboImage(ComboImageRequestParams requestParams) {
        //QO 转 PO
        ComboImage comboImage = new ComboImage();
        BeanUtils.copyProperties(requestParams,comboImage);
        Integer res = comboImageMapper.insertComboImage(comboImage);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        //PO 转 VO
        ComboImageVo imageVo = new ComboImageVo();
        BeanUtils.copyProperties(comboImage,imageVo);
        return imageVo;
    }

    @Override
    public Integer updateComboImage(ComboImageRequestParams requestParams) {
        //QO 转 PO
        ComboImage image = new ComboImage();
        BeanUtils.copyProperties(requestParams,image);
        Integer res = comboImageMapper.updateComboImage(image);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return res;
    }

    @Override
    public Integer removeComboImage(Integer id) {
        Integer res = comboImageMapper.updateComboImageByCombo(id);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return res;
    }

    @Override
    public List<ComboImageVo> getImagesByCombo(Integer id) {
        List<ComboImage> comboImages = comboImageMapper.selectImgByCombo(id);
        //批量PO转VO
        List<ComboImageVo> comboImageVos = comboImages.stream().map(image -> {
            ComboImageVo imageVo = new ComboImageVo();
            BeanUtils.copyProperties(image, imageVo);
            return imageVo;
        }).collect(Collectors.toList());
        return comboImageVos;
    }
}
