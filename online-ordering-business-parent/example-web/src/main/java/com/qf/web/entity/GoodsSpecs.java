package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "goods_specs")
public class GoodsSpecs {
    /**
     * 商品规格id
     */
    @TableId(value = "goods_specs_id", type = IdType.INPUT)
    private Integer goodsSpecsId;

    /**
     * 规格名称
     */
    @TableField(value = "goods_specs_name")
    private String goodsSpecsName;

    @TableField(value = "shop_id")
    private Integer shopId;

    public static final String COL_GOODS_SPECS_ID = "goods_specs_id";

    public static final String COL_GOODS_SPECS_NAME = "goods_specs_name";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_IS_DELETE = "is_delete";
}