package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.Group;

public interface GroupMapper extends BaseMapper<Group> {

}