package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
    * 规则表
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "combo_rule")
public class ComboRule {
    /**
     * 主键
     */
    @TableId(value = "rule_id", type = IdType.INPUT)
    private Integer ruleId;

    /**
     * 店铺ID，记录哪个店铺创建的规则
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 规则内容
     */
    @TableField(value = "rule_context")
    private String ruleContext;

    /**
     * 是否删除，1已删除，0未删除，默认0
     */
    @TableField(value = "is_delete")
    private Integer isDelete;
}