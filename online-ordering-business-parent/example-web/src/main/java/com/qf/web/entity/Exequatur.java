package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "exequatur")
public class Exequatur {
    /**
     * 主键
     */
    @TableId(value = "exequatur_id", type = IdType.INPUT)
    private Integer exequaturId;

    /**
     * 许可证编号
     */
    @TableField(value = "exequatur_number")
    private String exequaturNumber;

    /**
     * 有效期开始时间
     */
    @TableField(value = "exequatur_start_validity")
    private Date exequaturStartValidity;

    /**
     * 有效期结束时间
     */
    @TableField(value = "exequatur_end_validity")
    private Date exequaturEndValidity;

    /**
     * 许可证正面图片url
     */
    @TableField(value = "exequatur_image_url")
    private String exequaturImageUrl;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 是否启用，1启用，0禁用，默认1
     */
    @TableField(value = "`enable`")
    private Integer enable;

    public static final String COL_EXEQUATUR_ID = "exequatur_id";

    public static final String COL_EXEQUATUR_NUMBER = "exequatur_number";

    public static final String COL_EXEQUATUR_START_VALIDITY = "exequatur_start_validity";

    public static final String COL_EXEQUATUR_END_VALIDITY = "exequatur_end_validity";

    public static final String COL_EXEQUATUR_IMAGE_URL = "exequatur_image_url";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_ENABLE = "enable";
}