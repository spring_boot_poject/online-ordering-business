package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "`group`")
public class Group {
    /**
     * 团购套餐id
     */
    @TableId(value = "group_id", type = IdType.INPUT)
    private Integer groupId;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 团购套餐名字
     */
    @TableField(value = "group_name")
    private String groupName;

    /**
     * 团购套餐简介
     */
    @TableField(value = "group_info")
    private String groupInfo;

    /**
     * 团购套餐价格
     */
    @TableField(value = "group_price")
    private Integer groupPrice;

    /**
     * 团购套餐图片
     */
    @TableField(value = "group_img")
    private String groupImg;

    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_GROUP_ID = "group_id";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_GROUP_NAME = "group_name";

    public static final String COL_GROUP_INFO = "group_info";

    public static final String COL_GROUP_PRICE = "group_price";

    public static final String COL_GROUP_IMG = "group_img";

    public static final String COL_IS_DELETE = "is_delete";
}