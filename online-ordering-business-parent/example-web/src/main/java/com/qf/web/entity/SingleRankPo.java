package com.qf.web.entity;

import lombok.Data;

import java.util.List;

/**
 * 商品的单品类型销量排序
 */
@Data
public class SingleRankPo{
    /**
     * 店铺id
     */
    private Integer shopId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品名
     */
    private String goodsName;

    /**
     * 商品简介
     */
    private String goodsIntroduce;

    /**
     * 商品类别id
     */
    private Integer goodsTypeId;

    /**
     * 商品价格
     */
    private Integer goodsPrice;

    /**
     * 规格模板id
     */
    private Integer templateId;

    /**
     * 商品图片的信息
     */
    private List<GoodsPicture> goodsPictureList;

    /**
     * 订单对象里的商品数量
     */
    private Orders orders;


}
