package com.qf.web.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class EnvironmentPictureVo{

    /**
     * 环境图片
     */
    @TableField(value = "environment_picture_img")
    private String environmentPictureImg;
}
