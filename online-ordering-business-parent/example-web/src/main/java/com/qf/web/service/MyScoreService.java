package com.qf.web.service;
import com.qf.web.common.vo.MyScoreVo;
import java.util.List;
public interface MyScoreService {
    //1根据店铺id获取我的所有评价（评价配送员）
    List<MyScoreVo> getMyScoreByShopId(int shopId);
}
