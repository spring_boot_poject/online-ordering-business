package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.FlowingWaterRequestParams;
import com.qf.web.common.qo.ShopRequestParams;
import com.qf.web.common.vo.ShopVo;
import com.qf.web.entity.Shop;
import com.qf.web.mapper.ShopMapper;
import com.qf.web.service.FlowingWaterService;
import com.qf.web.service.ShopService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class ShopServiceImpl implements ShopService {

    @Resource
    private ShopMapper shopMapper;
    @Resource
    private FlowingWaterService flowingWaterService;

    @Override
    public List<ShopVo> selectShopInfo(ShopRequestParams shopRequestParams) {
        List<Shop> shops = shopMapper.selectShopInfo(shopRequestParams.getShopkeeperId());
        if (shops.size()==0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //要存储的返回对象vo集合
        ArrayList<ShopVo> shopVos = new ArrayList<>();
        //遍历实体类（po）对象然后一次加进vo集合里
        for (Shop shop:shops) {
            ShopVo shopVo = new ShopVo();
            //po转vo
            BeanUtils.copyProperties(shop,shopVo);
            boolean flag = shopVos.add(shopVo);
            if (!flag){
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        return shopVos;
    }

    @Override
    public ShopVo selectShopType(ShopRequestParams shopRequestParams) {
        Shop shop = shopMapper.selectShopTypeInfo(shopRequestParams.getShopTypeId());
        if (ObjectUtils.isEmpty(shop)){
            shop = new Shop();
            //qo转po
            BeanUtils.copyProperties(shopRequestParams,shop);
            int i = shopMapper.insert(shop);
            if (i > 0){
                //po转vo
                ShopVo shopVo = new ShopVo();
                BeanUtils.copyProperties(shop,shopVo);
                return shopVo;
            }else {
                throw new ServiceException(ResultCode.ERROR);
            }
        }else {
            throw new ServiceException(ResultCode.ACCOUNT_IS_EXISTENT);
        }
    }

    @Override
    public ShopVo selectShopStatus(ShopRequestParams shopRequestParams) {
        Shop shopStatus = shopMapper.selectShopStatus(shopRequestParams.getShopId());
        //po转vo
        ShopVo shopVo = new ShopVo();
        BeanUtils.copyProperties(shopStatus,shopVo);
        if (ObjectUtils.isEmpty(shopVo)){
            throw new ServiceException(ResultCode.ERROR);
        }
        return shopVo;
    }

    @Override
    public Integer updateShopStatus(ShopRequestParams shopRequestParams) {
        Integer rSet = shopMapper.updateShopStatus(shopRequestParams.getShopWorkState(),shopRequestParams.getShopId());
        if (rSet == 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        return rSet;
    }

    @Override
    public Integer updateShopImg(ShopRequestParams shopRequestParams) {
        Integer rSet = shopMapper.updateShopImg(shopRequestParams.getShopImg(), shopRequestParams.getShopId());
        if (rSet == 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        return rSet;
    }

    @Override
    public ShopVo selectShopAccount(ShopRequestParams shopRequestParams) {
        Shop shopAccount = shopMapper.selectShopAccount(shopRequestParams.getShopId());
        //po转vo
        ShopVo shopVo = new ShopVo();
        BeanUtils.copyProperties(shopAccount,shopVo);
        if (ObjectUtils.isEmpty(shopVo)){
            throw new ServiceException(ResultCode.ERROR);
        }
        return shopVo;
    }

    @Override
    @Transactional(rollbackFor = {RuntimeException.class, Error.class})
    public Integer updateShopAccount(ShopRequestParams shopRequestParams) {
        //提现金额
        Integer rSet = shopMapper.updateShopAccount(shopRequestParams.getMoney(),shopRequestParams.getShopId());
        if (rSet == 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //查看余额
        Shop shopAccount = shopMapper.selectShopAccount(shopRequestParams.getShopId());
        if (shopAccount.getShopAccount() < 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //生成流水
        Integer integer = flowingWaterService.registerFlowingWater(shopRequestParams);
        if (integer == 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        return rSet;
    }
}
