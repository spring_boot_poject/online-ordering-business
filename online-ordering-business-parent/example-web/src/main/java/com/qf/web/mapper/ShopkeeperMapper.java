package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.Shopkeeper;


public interface ShopkeeperMapper extends BaseMapper<Shopkeeper> {
    /**
     * 更新商户信息
     * @param shopkeeper
     * @return
     */
    Integer updateShokeeper(Shopkeeper shopkeeper);

    /**
     * 通过手机号查询商户信息
     * @param tel
     * @return
     */
    Shopkeeper selectShokeeperByTel(String tel);


    /**
     * 商户注册
     * @param shopkeeper
     * @return
     */
    Integer insertShopkeeper(Shopkeeper shopkeeper);

    /**
     * 提现操作中商家id和充值金钱来实现商家钱包增加
     * @return
     */
    Integer updateShopkeeperWallet(Integer money,Integer shopkeeperId);
}