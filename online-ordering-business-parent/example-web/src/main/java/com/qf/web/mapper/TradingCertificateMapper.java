package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.LegalPerson;
import com.qf.web.entity.TradingCertificate;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TradingCertificateMapper extends BaseMapper<TradingCertificate> {

    /**
     * 先查询营业执照是否存在，根据营业执照编号来查询
     * @param
     * @return
     */
    default TradingCertificate selectTrading(String tradingNumber){
        QueryWrapper<TradingCertificate> qw = new QueryWrapper<>();
        qw.select(TradingCertificate.COL_TRADING_CERTIFICATE_ID,
                        TradingCertificate.COL_ENABLE,
                        TradingCertificate.COL_TRADING_IMAGE_URL,
                        TradingCertificate.COL_TRADING_START_VALIDITY,
                        TradingCertificate.COL_TRADING_END_VALIDITY,
                        TradingCertificate.COL_SHOP_ID)
                .eq(TradingCertificate.COL_TRADING_CERTIFICATE_NUMBER,tradingNumber);
        return this.selectOne(qw);
    }

    /**
     * 上传营业执照
     * @param img
     * @param shopId
     * @return
     */
    Integer registerTradingImg(String img,Integer shopId);
}