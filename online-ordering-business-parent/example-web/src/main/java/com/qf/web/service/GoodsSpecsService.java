package com.qf.web.service;

import com.qf.web.common.qo.GoodsSpecsRequestParams;
import com.qf.web.common.vo.GoodsSpcesVo;

import java.util.List;

public interface GoodsSpecsService {
    /**
     * 添加商品规格（属性）以及规格详细信息
     * @param goodsSpecsRequestParams
     * @return
     */
    public Integer addGoodsSpecs(GoodsSpecsRequestParams goodsSpecsRequestParams);

    /**
     * 查询所有的通用规格
     * @return
     */
    public List<GoodsSpcesVo> selectCommonGoodsSpeces();

    /**
     * 查询店铺特有的规格信息
     * @return
     */
    public List<GoodsSpcesVo> selectShopGoodsSpeces(GoodsSpecsRequestParams goodsSpecsRequestParams);

    /**
     * 修改商品规格基本信息
     * @param goodsSpecsRequestParams
     * @return
     */
    Integer updateGoodsSpecs(GoodsSpecsRequestParams goodsSpecsRequestParams);

    /**
     * 伪删除。批量伪删除规格信息
     * @param goodsSpecsRequestParamsList
     * @return
     */
    Integer removeGoodsSpecs(List<GoodsSpecsRequestParams> goodsSpecsRequestParamsList);
}
