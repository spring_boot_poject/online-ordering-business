package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "specs")
public class Specs {
    /**
     * 规格详细id
     */
    @TableId(value = "specs_id", type = IdType.INPUT)
    private Integer specsId;

    /**
     * 商品规格id
     */
    @TableField(value = "goods_specs_id")
    private Integer goodsSpecsId;

    /**
     * 规格详细信息
     */
    @TableField(value = "specs_info")
    private String specsInfo;

    @TableField(value = "is_delete")
    private Integer isDelete;




    public static final String COL_SPECS_ID = "specs_id";

    public static final String COL_GOODS_SPECS_ID = "goods_specs_id";

    public static final String COL_SPECS_INFO = "specs_info";

    public static final String COL_IS_DELETE = "is_delete";
}