package com.qf.web.common.qo;
import lombok.Data;

/**
 * @author 51993
 */
@Data
public class BankRequestParams {

    /**
     * 银行卡id
     */
    private Integer bankId;

    /**
     * 银行卡号
     */
    private String bankNum;

    /**
     * 店铺id
     */
    private Integer shopkeeperId;

}
