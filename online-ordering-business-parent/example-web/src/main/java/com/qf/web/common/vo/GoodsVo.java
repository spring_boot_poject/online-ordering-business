package com.qf.web.common.vo;

import lombok.Data;

@Data
public class GoodsVo {
    /**
     * 商品id
     */
    private Integer goodsId;
    /**
     * 商品名
     */
    private String goodsName;

    /**
     * 商品简介
     */
    private String goodsIntroduce;

    /**
     * 商品类别id
     */
    private Integer goodsTypeId;

    /**
     * 店铺id
     */
    private Integer shopId;

    /**
     * 商品价格
     */
    private Integer goodsPrice;

    /**
     * 规格模板id
     */
    private Integer templateId;

    /**
     * 包装费
     */
    private Integer goodsPackingFee;


}
