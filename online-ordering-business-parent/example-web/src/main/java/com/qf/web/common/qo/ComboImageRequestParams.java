package com.qf.web.common.qo;

import lombok.Data;

@Data
public class ComboImageRequestParams {
    /**
     * 主键
     */
    private Integer imageId;

    /**
     * 图片URL
     */
    private String imageUrl;

    /**
     * 套餐ID
     */
    private Integer comboId;

    /**
     * 是否主图 1 是 0不是
     */
    private Integer mainImage;
}
