package com.qf.web.common.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 51993
 */
@Data
public class ShopkeeperVo implements Serializable {

    /**
     * 商家id
     */
    private Integer shopkeeperId;

    /**
     * 商家昵称
     */
    private String shopkeeperName;

    /**
     * 商家电话
     */
    private String shopkeeperPhone;

    /**
     * 商家头像
     */
    private String shopkeeperImg;

    /**
     * 商家钱包
     */
    private Double shopkeeperWallet;

    /**
     * 商户注册时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date registerTime;


}
