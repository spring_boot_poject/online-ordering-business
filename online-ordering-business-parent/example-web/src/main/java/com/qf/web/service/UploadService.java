package com.qf.web.service;

import org.springframework.web.multipart.MultipartFile;

public interface UploadService {
    /**
     * 上传照片到阿里云oss对象存储服务器，返回图片路径
     * @param multipartFile
     * @return
     */
    String uploadImage(MultipartFile multipartFile, String bucketName);
}
