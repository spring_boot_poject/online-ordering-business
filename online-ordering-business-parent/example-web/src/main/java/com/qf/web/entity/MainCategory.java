package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "main_category")
public class MainCategory {
    @TableId(value = "category_id", type = IdType.INPUT)
    private Integer categoryId;

    /**
     * 品类名称
     */
    @TableField(value = "category_name")
    private String categoryName;

    /**
     * 上级品类ID，默认0，说明是1级品类
     */
    @TableField(value = "parent_category_id")
    private Integer parentCategoryId;

    /**
     * 是否启用，1启用，0禁用，默认1
     */
    @TableField(value = "`enable`")
    private Integer enable;

    /**
     * 隶属店铺类型
     */
    @TableField(value = "shop_type_id")
    private Integer shopTypeId;

    public static final String COL_CATEGORY_ID = "category_id";

    public static final String COL_CATEGORY_NAME = "category_name";

    public static final String COL_PARENT_CATEGORY_ID = "parent_category_id";

    public static final String COL_ENABLE = "enable";

    public static final String COL_SHOP_TYPE_ID = "shop_type_id";
}