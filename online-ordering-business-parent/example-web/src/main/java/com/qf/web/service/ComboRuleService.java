package com.qf.web.service;

import com.qf.web.common.qo.ComboRuleRequestParams;
import com.qf.web.common.vo.ComboRuleVo;

import java.util.List;

public interface ComboRuleService {

    /**
     * 添加套餐和规则的关系
     * @param cid   套餐Id
     * @param rid   规则Id
     * @return
     */
    Integer addComboRuleRelation(Integer cid,Integer rid);

    /**
     * 店铺添加使用规则
     * @param requestParams
     * @return
     */
    Integer addComboRule(ComboRuleRequestParams requestParams);

    /**
     * 根据店铺查询使用规则
     * @param id
     * @return
     */
    List<ComboRuleVo> queryRuleByShop(Integer id);

    /**
     * 更新使用规则
     * @param requestParams
     * @return
     */
    Integer updateRule(ComboRuleRequestParams requestParams);

    /**
     * 删除套餐中的使用规则
     * @param comboId
     * @return
     */
    Integer delComboRule(Integer comboId);

    /**
     * 删除店铺中的使用规则
     * @param requestParams
     * @return
     */
    Integer delRule(ComboRuleRequestParams requestParams);
}
