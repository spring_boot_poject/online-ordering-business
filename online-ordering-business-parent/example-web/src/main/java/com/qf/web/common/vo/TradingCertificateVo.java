package com.qf.web.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
public class TradingCertificateVo{
    /**
     * 主键
     */
    @TableId(value = "trading_certificate_id", type = IdType.INPUT)
    private Integer tradingCertificateId;

    /**
     * 营业执照编号
     */
    @TableField(value = "trading_certificate_number")
    private String tradingCertificateNumber;

    /**
     * 营业执照开始有效期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "trading_start_validity")
    private Date tradingStartValidity;

    /**
     * 营业执照结束有效期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "trading_end_validity")
    private Date tradingEndValidity;

    /**
     * 营业执照正面图片url
     */
    @TableField(value = "trading_image_url")
    private String tradingImageUrl;

    /**
     * 店铺ID
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 是否启用，1启用，0禁用，默认1
     */
    @TableField(value = "`enable`")
    private Integer enable;
}
