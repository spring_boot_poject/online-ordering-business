package com.qf.web.service;


import com.qf.web.common.qo.CouponRequestParams;
import com.qf.web.common.vo.CouponVo;
import com.qf.web.entity.ActiveOrder;
import com.qf.web.entity.CouponInfo;

import java.util.List;

public interface CouponService {

    /**
     * 添加代金券
     * @param couponRequestParams
     * @return
     */
    public Integer addCoupon(CouponRequestParams couponRequestParams);


    /**
     * 查询所有的代金券
     * @return
     */
    public List<CouponVo> selectCouponList(Integer shopId);
    /**
     * 筛选查询代金券
     * @return
     */
    public List<CouponVo> selectSpecificCouponList(CouponRequestParams couponRequestParams);

    /**
     * 查询店铺内的代金券的使用记录
     * @param shopId
     * @return
     */
    public List<CouponInfo> selectCouponInfo(Integer shopId);

    /**
     * 数据分析
     * @param shopId
     * @param dateType
     * @return
     */
    public ActiveOrder selectActiveOrderInfo(Integer shopId,Integer dateType);

    /**
     * 修改发放状态
     * @param couponId
     * @param isDelete
     * @return
     */
    Integer updateGiveOutStatus(Integer couponId,Integer isDelete);
}
