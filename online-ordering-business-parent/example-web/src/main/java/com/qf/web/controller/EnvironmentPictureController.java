package com.qf.web.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.EnvironmentRequestParams;
import com.qf.web.common.utils.ConstantUtils;
import com.qf.web.common.vo.EnvironmentPictureVo;
import com.qf.web.service.EnvironmentPictureService;
import com.qf.web.service.UploadService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author banxiaowen
 */
@RestController
@RequestMapping("/environment")
public class EnvironmentPictureController {
    @Resource
    private EnvironmentPictureService environmentPictureService;
    @Resource
    UploadService uploadService;

    @GetMapping("/environment/info")
    public ResponseResult<List<EnvironmentPictureVo>> selectShopEnvironmentInfo(@RequestBody EnvironmentRequestParams environmentRequestParams){
        List<EnvironmentPictureVo> pictures = environmentPictureService.selectEnvironmentInfo(environmentRequestParams);
        return ResponseResult.success(pictures);
    }

    @PostMapping("/environment/upload")
    public ResponseResult<ResultCode> updateShopEnvironmentPicture(@RequestBody MultipartFile multipartFile,
                                                                   EnvironmentRequestParams environmentRequestParams){
        String imgUrl = uploadService.uploadImage(multipartFile, ConstantUtils.ENVIRONMENT_IMAGE_BUCKET_NAME);
        environmentRequestParams.setEnvironmentPictureImg(imgUrl);
        Integer rSet = environmentPictureService.updateShopEnvironmentPicture(environmentRequestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
