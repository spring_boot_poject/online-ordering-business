package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.common.vo.ShopOrdersVo;
import com.qf.web.entity.Orders;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;
import com.qf.web.common.vo.FullOrderNumVo;
import com.qf.web.common.vo.OrderBeanVo;import com.qf.web.entity.Orders;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrdersMapper extends BaseMapper<Orders> {



    /**
     * 生成一个订单的详细信息
     *
     * @param orderId
     * @return
     */
    OrderBeanVo selectOrderBean(Integer orderId);

    /**
     * 生成一个未完成的详细信息
     *
     * @param shopId
     * @return
     */
    List<OrderBeanVo> selectOrderBeanList(Integer shopId);

    /**
     * 定义状态生成订单的所有的状态
     *
     * @param state
     * @return
     */
    List<OrderBeanVo> selectOrderBystate(@Param(value = "shopId") Integer shopId, @Param(value = "state") Integer state);

    /**
     * 定义订单退款的所有状态
     *
     * @param shopId
     * @param refund 退款状态 0 正常订单  1 顾客退款  2 商家拒绝退款  3 商家同意退款
     * @return
     */
    List<OrderBeanVo> selectOrderByRefund(@Param(value = "shopId") Integer shopId, @Param(value = "refund")Integer refund);

    /**
     * 查询已完成的订单，成功交易的
     * @param shopId
     * @return
     */
    List<OrderBeanVo> selectOrderSuccess(Integer shopId);


    /**
     * 商家取消订单或者为接单
     * @param shopId
     * @return
     */
    List<OrderBeanVo> selectOrderIgnore(Integer shopId);


    /**
     * 查看配送的历史订单数据
     * @param shopId
     * @return
     */
    List<OrderBeanVo> selectHistoryOrder(Integer shopId);


    /**
     * 生成一个订单的详细信息
     *
     * @param shopId
     * @return
     */
    List<OrderBeanVo> selectOrderBeanListTake(Integer shopId);

    /**
     * 定义状态生成订单的所有的状态
     *
     * @param state
     * @return
     */
    List<OrderBeanVo> selectOrderBystateTake(@Param(value = "shopId")Integer shopId, @Param(value = "state") Integer state);

    /**
     * 定义订单退款的所有状态
     *
     * @param shopId
     * @param refund 退款状态 0 正常订单  1 顾客退款  2 商家拒绝退款  3 商家同意退款
     * @return
     */
    List<OrderBeanVo> selectOrderByRefundTake(@Param(value = "shopId") Integer shopId, @Param(value = "refund") Integer refund);

    /**
     * 查询已完成的订单，成功交易的
     * @param shopId
     * @return
     */
    List<OrderBeanVo> selectOrderSuccessTake(Integer shopId);


    /**
     * 商家取消订单或者为接单
     * @param shopId
     * @return
     */
    List<OrderBeanVo> selectOrderIgnoreTake(Integer shopId);


    /**
     * 查看配送的历史订单数据
     * @param shopId
     * @return
     */
    List<OrderBeanVo> selectHistoryOrderTake(Integer shopId);


    /**
     * 商家取消订单的操作
     * @param orderId
     * @return
     */
    int updateOrderCancel(Integer orderId);


    /**
     * 商家接受订单
     * @param orderId
     * @return
     */
    int updateOrderAccept(Integer orderId);

    /**
     * 商家同意退款
     * @param orderId
     * @return
     */
    int updateOrderAgree(Integer orderId);

    /**
     * 商家不同意退款
     * @param orderId
     * @return
     */
    int updateOrderDisagree(Integer orderId);

    /**
     * 查询 今天 昨天 订单总量
     * @param shopId
     * @return
     */
    FullOrderNumVo selectOrderNumDay(@Param(value = "shopId") Integer shopId,@Param(value = "day") Integer day);



    /**
     * 查询 7  30 天订单总量
     * @param shopId
     * @return
     */
    FullOrderNumVo selectSomeOrderNumDay(@Param(value = "shopId") Integer shopId,@Param(value = "day") Integer day);

    /**
     * 根据店铺id查询店铺订单
     * @param shopId
     * @return
     */
    List<ShopOrdersVo> selectShopOrders(Integer shopId);

    /**
     * 根据当天时间查看详细店铺订单流水
     * @param date
     * @return
     */
    List<ShopOrdersVo> selectDayDetailOrders(String date,Integer shopId);

    /**
     * 查看当日的预计收入（预计今日的订单量和订单收入）
     * @param shopId
     * @return
     */
    ShopOrdersVo selectIncomeAdvance(Integer shopId);

    /**
     * 根据店铺id来查询范围内的订单流水
     * @return
     */
    List<ShopOrdersVo> selectRangeOrders(String start,String end, Integer shopId);

    /**
     * 根据店铺id来查询近一个月的订单流水
     * @param shopId
     * @return
     */
    List<ShopOrdersVo> selectMonthOrders(Integer shopId);

}