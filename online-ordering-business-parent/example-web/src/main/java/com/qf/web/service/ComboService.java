package com.qf.web.service;

import com.qf.web.common.qo.ComboRequestParams;
import com.qf.web.common.vo.ComboImageVo;
import com.qf.web.common.vo.ComboVo;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

public interface ComboService {

    /**
     * 添加套餐
     * @param requestParams
     * @return
     */
    Integer addCombo(ComboRequestParams requestParams);


    /**
     * 添加套餐图片
     * @param multipartFile
     * @return
     */
    ComboImageVo addComboImage(MultipartFile multipartFile);

    /**
     * 查询店铺中的套餐
     * @param id    店铺ID
     * @return
     */
    List<ComboVo> queryAllComboByShop(Integer id);

    /**
     * 店家获取本店的套餐信息
     * @return
     */
    List<ComboVo> getComboByShop(Integer id);

    /**
     * 更新店铺中套餐的信息
     * @param requestParams
     * @return
     */
    Integer updateComboDetails(ComboRequestParams requestParams);

    /**
     * 获取指定商品的详情信息
     * @param id
     * @return
     */
    ComboVo getComboDetails(Integer id);

    /**
     * 删除指定套餐
     * @param comboId
     * @return
     */
    Integer delCombo(Integer comboId);

    /**
     * 设置活动是否是否开启优惠
     * @param requestParams
     * @return
     */
    Integer updateComboEnable(ComboRequestParams requestParams);

    /**
     * 根据日期条件查询套餐
     * @param requestParams
     * @return
     */
    List<ComboVo> searchCombo(ComboRequestParams requestParams);
}
