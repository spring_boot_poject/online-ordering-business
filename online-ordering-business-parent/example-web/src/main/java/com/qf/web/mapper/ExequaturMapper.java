package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.Exequatur;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ExequaturMapper extends BaseMapper<Exequatur> {
}