package com.qf.web.common.qo;

import lombok.Data;

import java.util.Date;

/**
 * 用于接收前端注册店铺时传递的参数
 *
 * @author xjw
 */
@Data
public class RegisterShopRequestParams {

    /**
     * 店铺名
     */
    private String shopName;

    /**
     * 商家id
     */
    private Integer shopkeeperId;

    /**
     * 店铺简介
     */
    private String shopIntroduce;

    /**
     * 店铺地址
     */
    private String shopAddrees;

    /**
     * 店铺联系电话
     */
    private Integer shopPhone;

    /**
     * 店铺头像
     */
    private String shopImg;

    /**
     * 店铺主营品类ID
     */
    private Integer categoryId;


    /**
     * 店铺类型ID
     */
    private Integer shopTypeId;


    /**
     * 许可证编号
     */
    private String exequaturNumber;

    /**
     * 有效期开始时间
     */
    private Date exequaturStartValidity;

    /**
     * 有效期结束时间
     */
    private Date exequaturEndValidity;

    /**
     * 许可证正面图片url
     */
    private String exequaturImageUrl;

    /**
     * 店铺id
     */
    private Integer shopId;

    /**
     * 法人证件ID
     */
    private Integer legalPersonId;

    /**
     * 性别，1男，0女
     */
    private Integer gender;


    /**
     * 证件类型ID
     */
    private Integer certificateTypeId;

    /**
     * 证件号码
     */
    private String idNumber;

    /**
     * 有效期开始时间
     */
    private Date personStartValidity;

    /**
     * 有效期结束时间
     */
    private Date personEndValidity;


    /**
     * 证件反面url
     */
    private String reverseImageUrl;

    /**
     * 证件正面url
     */
    private String obverseImageUrl;

    /**
     * 营业执照编号
     */
    private String tradingCertificateNumber;

    /**
     * 营业执照开始有效期
     */
    private Date tradingStartValidity;

    /**
     * 营业执照结束有效期
     */
    private Date tradingEndValidity;

    /**
     * 营业执照正面图片url
     */
    private String tradingImageUrl;


}
