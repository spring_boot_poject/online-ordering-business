package com.qf.web.common.qo;

import lombok.Data;

import java.util.Date;

/**
 * @author 51993
 */
@Data
public class ExequaturRequestParams {

    /**
     * 主键
     */
    private Integer exequaturId;

    /**
     * 许可证编号
     */
    private String exequaturNumber;

    /**
     * 有效期开始时间
     */
    private Date exequaturStartValidity;

    /**
     * 有效期结束时间
     */
    private Date exequaturEndValidity;

    /**
     * 许可证正面图片url
     */
    private String exequaturImageUrl;

    /**
     * 店铺id
     */
    private Integer shopId;

}
