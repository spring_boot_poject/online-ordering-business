package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.ScoreImg;

public interface ScoreImgMapper extends BaseMapper<ScoreImg> {
}