package com.qf.web.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.qf.web.entity.GoodsPicture;
import lombok.Data;

import java.util.List;

/**
 * 商品的详细信息
 */

@Data
public class GoodsBeanVo {

    /**
     * 商品id
     */
    @TableId(value = "goods_id", type = IdType.INPUT)
    private Integer goodsId;

    /**
     * 商品名
     */
    @TableField(value = "goods_name")
    private String goodsName;

    /**
     * 商品简介
     */
    @TableField(value = "goods_introduce")
    private String goodsIntroduce;

    /**
     * 商品类别id
     */
    @TableField(value = "goods_type_id")
    private Integer goodsTypeId;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 商品价格
     */
    @TableField(value = "goods_price")
    private Integer goodsPrice;

    /**
     * 规格模板id
     */
    @TableField(value = "template_id")
    private Integer templateId;

    /**
     * 包装费
     */
    @TableField(value = "goods_packing_fee")
    private Integer goodsPackingFee;

    /**
     * 商品的数量
     */
    private Integer goodsNum;

    //图片的信息
    List<GoodsPicture> goodsPictures;
}
