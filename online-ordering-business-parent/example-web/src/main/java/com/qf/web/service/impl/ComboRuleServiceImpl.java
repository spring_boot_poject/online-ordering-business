package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.ComboRuleRequestParams;
import com.qf.web.common.vo.ComboRuleVo;
import com.qf.web.entity.ComboRule;
import com.qf.web.mapper.ComboRuleMapper;
import com.qf.web.service.ComboRuleService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ComboRuleServiceImpl implements ComboRuleService {

    @Autowired
    ComboRuleMapper comboRuleMapper;

    @Override
    public Integer addComboRuleRelation(Integer cid, Integer rid) {

        Integer res = comboRuleMapper.insertComboRuleRelation(cid, rid);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }

        return res;
    }

    @Override
    public Integer addComboRule(ComboRuleRequestParams requestParams) {
        //QO 转 PO
        ComboRule comboRule = new ComboRule();
        BeanUtils.copyProperties(requestParams,comboRule);
        int res = comboRuleMapper.insert(comboRule);
        if (res < 0){
            //添加失败
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return res;
    }

    @Override
    public List<ComboRuleVo> queryRuleByShop(Integer id) {
        List<ComboRule> comboRules = comboRuleMapper.selectByShop(id);
        //批量PO 转 VO对象
        List<ComboRuleVo> comboRuleVoList = comboRules.stream().map(comboRule -> {
            ComboRuleVo comboRuleVo = new ComboRuleVo();
            BeanUtils.copyProperties(comboRule, comboRuleVo);
            return comboRuleVo;
        }).collect(Collectors.toList());
        return comboRuleVoList;
    }

    @Override
    public Integer updateRule(ComboRuleRequestParams requestParams) {
        //先将原来设置为删除状态，然后插入新的
        Integer res = comboRuleMapper.updateRuleToDel(requestParams.getRuleId());
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        //QO 转 VO
        ComboRule comboRule = new ComboRule();
        BeanUtils.copyProperties(requestParams,comboRule);
        //把修改后的使用规则当新添加的规则
        comboRule.setRuleId(null);
        res = comboRuleMapper.insert(comboRule);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return res;
    }

    @Override
    public Integer delComboRule(Integer comboId) {
        Integer res = comboRuleMapper.deleteComboRuleRelation(comboId);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return res;
    }

    @Override
    public Integer delRule(ComboRuleRequestParams requestParams) {
        Integer res = comboRuleMapper.updateRuleToDel(requestParams.getRuleId());
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return res;
    }
}
