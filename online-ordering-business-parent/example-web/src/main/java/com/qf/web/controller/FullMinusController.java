package com.qf.web.controller;

import com.qf.common.base.exception.ControllerException;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.FullMinusRequestParams;
import com.qf.web.service.FullMinusService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * @ClassName FullMinusController
 * @Description FullMinus控制层
 * @Author zhenyang
 * @Date 2022/3/30 20:44
 **/
@RestController
@RequestMapping("/full")
public class FullMinusController {

    @Resource
    FullMinusService fullMinusService;

    /**
     * 添加优惠卷
     * @param fullMinusRequestParams
     * @return
     */
    @PostMapping("/add/full")
    public ResponseResult insertFull(@Valid  @RequestBody FullMinusRequestParams fullMinusRequestParams){

        ResponseResult result=null;
        try {
             result = fullMinusService.insertFull(fullMinusRequestParams);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     * 查询优惠使用情况
     * @param shopId
     * @return
     */
    @GetMapping("/order/full")
    public ResponseResult selectOrderFull(@NotNull  Integer shopId){
        ResponseResult result=null;
        try {
            result = fullMinusService.selectOrderFull(shopId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     * 查询今天的优惠使用信息
     * @param shopId
     * @return
     */
    @GetMapping("/order/now")
    public ResponseResult selectFullOrderNow(@NotNull Integer shopId){
        ResponseResult result=null;
        try {
            result = fullMinusService.selectFullOrderNow(shopId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     * 查询昨天的优惠信息
     * @param shopId
     * @return
     */
    @GetMapping("/order/yetday")
    public ResponseResult selectFullOrderYETday(@NotNull Integer shopId){
        ResponseResult result=null;
        try {
            result = fullMinusService.selectFullOrderYETday(shopId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     * 查询七天内的优惠信息
     * @param shopId
     * @return
     */
    @GetMapping("/order/seven")
    public ResponseResult selectFullOrderSevenDay(@NotNull Integer shopId){
        ResponseResult result=null;
        try {
            result = fullMinusService.selectFullOrderSevenDay(shopId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     * 查询30天内的优惠信息
     * @param shopId
     * @return
     */
    @GetMapping("/order/thirty")
    public ResponseResult selectFullOrderThirtyDay(@NotNull Integer shopId){
        ResponseResult result=null;
        try {
            result = fullMinusService.selectFullOrderThirtyDay(shopId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }


    /**
     * 查询所有的优惠情况
     * @param shopId
     * @return
     */
    @GetMapping("/all/full")
    public ResponseResult selectAllFull(@NotNull Integer shopId){
        ResponseResult result=null;
        try {
            result = fullMinusService.selectAllFull(shopId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }

    /**
     * 删除一个优惠活动
     * @param fullMinusId
     * @return
     */
    @DeleteMapping("/delete/full")
    public ResponseResult deleteFull(@NotNull Integer fullMinusId){
        ResponseResult result=null;
        try {
            result = fullMinusService.deleteFull(fullMinusId);
        } catch (Exception e) {
            throw  new ControllerException(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return result;
    }


}
