package com.qf.web.common.vo;

import lombok.Data;

@Data
public class ScoreCountVo {
    private Integer badScoreCount;
    private Integer goodScoreCount;

}
