package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.EnvironmentPicture;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface EnvironmentPictureMapper extends BaseMapper<EnvironmentPicture> {
    /**
     * 根据店铺id查询店铺环境图
     * @param shopId
     * @return
     */
    List<EnvironmentPicture> selectShopEnvironmentInfo(Integer shopId);

    /**
     * 根据店铺环境图id修改店铺环境图
     * @param img
     * @param id
     * @return
     */
    Integer updateShopEnvironmentPicture(String img,Integer id);
}