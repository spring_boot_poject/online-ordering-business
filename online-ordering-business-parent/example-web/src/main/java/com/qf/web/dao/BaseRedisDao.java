package com.qf.web.dao;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

/**
 * 封装Redis常用操作
 * @author 谢积威
 */
public interface BaseRedisDao {


    /**
     * redis删除key
     * @param key
     * @return
     */
    Boolean delete(String key);

    /**
     * String 根据key获取值
     * @param key
     * @return
     */
    Object getForValue(String key);


    /**
     * @param key
     * @param value
     * @param timeout
     */
    void setForValue(String key, Object value, Duration timeout);

    /**
     *
     * @param key
     * @param value
     * @param timeout
     * @param unit
     */
    void setForValue(String key, Object value, long timeout, TimeUnit unit);
}
