package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.common.vo.FlowingWaterVo;import com.qf.web.entity.FlowingWater;
import org.apache.ibatis.annotations.Mapper;import java.util.List;

@Mapper
public interface FlowingWaterMapper extends BaseMapper<FlowingWater> {
    /**
     * 根据店铺id查询一个月内提现流水
     *
     * @param shopId
     * @return
     */
    List<FlowingWaterVo> selectFlowingWater(Integer shopId);

    /**
     * 根据店铺id查询自定义时间段内内提现流水
     *
     * @param shopId
     * @param start
     * @param end
     * @return
     */
    List<FlowingWaterVo> selectDateFlowingWater(Integer shopId, String start, String end);

    /**
     * 根据商家id查询一个月内充值流水
     *
     * @param shopkeeperId
     * @return
     */
    List<FlowingWaterVo> selectUpFlowingWater(Integer shopkeeperId);

    /**
     * 根据商家id查询自定义时间段内内充值流水
     *
     * @param shopkeeperId
     * @param start
     * @param end
     * @return
     */
    List<FlowingWaterVo> selectDateUpFlowingWater(Integer shopkeeperId, String start, String end);
}