package com.qf.web.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.util.Date;

@Data
public class CouponInfo {

    @TableField("order_id")
    private Long orderId;

    @TableField("coupon_price")
    private double couponPrice;

    @TableField("order_time")
    private Date orderTime;
}
