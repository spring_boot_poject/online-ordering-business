package com.qf.web.controller;


import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.vo.BankVo;
import com.qf.web.common.qo.BankRequestParams;
import com.qf.web.service.BankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author 谢积威
 */
@RestController
@RequestMapping("/bank")
public class BankController {

    @Autowired
    BankService bankService;

    /**
     * 绑定银行卡
     * @return
     */
    @PutMapping("/add")
    public ResponseResult<ResultCode> addBank(BankRequestParams requestParams){
        bankService.addBank(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 解绑银行卡
     * @param requestParams
     * @return
     */
    @PostMapping("/update")
    public ResponseResult<ResultCode> unbindBank(@RequestBody BankRequestParams requestParams){
        bankService.unbind(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 查看店铺绑定的银行卡
     * @param requestParams
     * @return
     */
    @GetMapping("/get")
    public ResponseResult<List<BankVo>> getBanks(@RequestBody BankRequestParams requestParams){
        List<BankVo> bankVoList = bankService.selectBanks(requestParams.getShopkeeperId());
        return ResponseResult.success(bankVoList);
    }

}
