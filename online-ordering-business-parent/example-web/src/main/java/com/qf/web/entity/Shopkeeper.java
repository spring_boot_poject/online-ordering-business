package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author 51993
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "shopkeeper")
public class Shopkeeper implements Serializable {
    /**
     * 商家id
     */
    @TableId(value = "shopkeeper_id", type = IdType.INPUT)
    private Integer shopkeeperId;

    /**
     * 商家昵称
     */
    @TableField(value = "shopkeeper_name")
    private String shopkeeperName;

    /**
     * 商家电话
     */
    @TableField(value = "shopkeeper_phone")
    private String shopkeeperPhone;

    /**
     * 商家登录密码
     */
    @TableField(value = "shopkeeper_password")
    private String shopkeeperPassword;

    /**
     * 商家支付密码
     */
    @TableField(value = "shopkeeper_pay_password")
    private String shopkeeperPayPassword;

    /**
     * 商家头像
     */
    @TableField(value = "shopkeeper_img")
    private String shopkeeperImg;

    /**
     * 商家钱包
     */
    @TableField(value = "shopkeeper_wallet")
    private Double shopkeeperWallet;

    /**
     * 伪删除
     */
    @TableField(value = "is_delete")
    private Integer isDelete;


    /**
     * 商户注册时间
     */
    @TableField(value = "register_time")
    private Date registerTime;

    public static final String COL_SHOPKEEPER_ID = "shopkeeper_id";

    public static final String COL_SHOPKEEPER_NAME = "shopkeeper_name";

    public static final String COL_SHOPKEEPER_PHONE = "shopkeeper_phone";

    public static final String COL_SHOPKEEPER_PASSWORD = "shopkeeper_password";

    public static final String COL_SHOPKEEPER_PAY_PASSWORD = "shopkeeper_pay_password";

    public static final String COL_SHOPKEEPER_IMG = "shopkeeper_img";

    public static final String COL_SHOPKEEPER_WALLET = "shopkeeper_wallet";

    public static final String COL_IS_DELETE = "is_delete";

}