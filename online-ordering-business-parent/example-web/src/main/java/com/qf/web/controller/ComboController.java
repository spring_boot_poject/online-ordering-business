package com.qf.web.controller;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.annotations.CacheParam;
import com.qf.web.common.annotations.EnableRedisCache;
import com.qf.web.common.annotations.UpdateRedisCache;
import com.qf.web.common.qo.ComboRequestParams;
import com.qf.web.common.utils.ConstantUtils;
import com.qf.web.common.vo.ComboImageVo;
import com.qf.web.common.vo.ComboVo;
import com.qf.web.service.ComboService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

/**
 * @author 谢积威
 */
@RestController
@RequestMapping("/combo")
public class ComboController {

    @Autowired
    ComboService comboService;

    /**
     * 添加套餐
     * @param requestParams
     * @return
     */
    @PostMapping("/add")
    public ResponseResult<ResultCode> addCombo(@RequestBody ComboRequestParams requestParams){
        comboService.addCombo(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 前端在添加套餐时上传图片，然后返回一个套餐图片对象，其中包含URL给前端回显，图片ID用于提交保存套餐时携带ID数组让套餐和图片关联
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public ResponseResult<ComboImageVo> uploadImage(MultipartFile file){
        ComboImageVo imageVo = comboService.addComboImage(file);
        return ResponseResult.success(imageVo);
    }

    /**
     * 通过店铺ID查询店铺中的套餐信息(提供管理员使用，暂时留着，可能用不到)
     * @param id    店铺ID
     * @return
     */
    @GetMapping("/get/{id}")
    public ResponseResult<List<ComboVo>> getComboListById(@PathVariable("id") Integer id){
        List<ComboVo> comboVoList = comboService.queryAllComboByShop(id);
        return ResponseResult.success(comboVoList);
    }

    /**
     * 店铺查看本店中的所有套餐信息
     * @param requestParams
     * @return
     */
    @GetMapping("/shop")
    public ResponseResult<List<ComboVo>> getComboListByShop(@RequestBody ComboRequestParams requestParams){
        List<ComboVo> comboVoList = comboService.getComboByShop(requestParams.getShopId());
        return ResponseResult.success(comboVoList);
    }

    /**
     * 更新店铺中套餐信息
     * @param requestParams
     * @return
     */
    @PostMapping("/update")
    @UpdateRedisCache(keyPrefix = ConstantUtils.COMBO_CACHE_KEY_PREFIX)
    public ResponseResult<ResultCode> updateCombo(@RequestBody @CacheParam("comboId") ComboRequestParams requestParams){
        comboService.updateComboDetails(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 获取指定套餐的商品详情信息
     * @param requestParams
     * @return
     */
    @EnableRedisCache(timeout = 3000,keyPrefix = ConstantUtils.COMBO_CACHE_KEY_PREFIX)
    @GetMapping("/details")
    public ResponseResult<ComboVo> getDetails(@RequestBody @CacheParam("comboId") ComboRequestParams requestParams){
        ComboVo comboDetails = comboService.getComboDetails(requestParams.getComboId());
        return ResponseResult.success(comboDetails);
    }

    /**
     * 删除套餐
     * @param requestParams
     * @return
     */
    @DeleteMapping("/del")
    @UpdateRedisCache(keyPrefix = ConstantUtils.COMBO_CACHE_KEY_PREFIX)
    public ResponseResult<ResultCode> delCombo(@RequestBody @CacheParam("comboId") ComboRequestParams requestParams){
        comboService.delCombo(requestParams.getComboId());
        return ResponseResult.success(ResultCode.SUCCESS);
    }


    /**
     * 根据日期条件筛选套餐
     * @return
     */
    @GetMapping("/search")
    public ResponseResult<List<ComboVo>> searchCombo(@RequestBody ComboRequestParams requestParams){
        List<ComboVo> comboVoList = comboService.searchCombo(requestParams);
        return ResponseResult.success(comboVoList);
    }

    /**
     * 修改套餐的优惠状态
     * @param requestParams
     * @return
     */
    @PostMapping("/enable")
    @UpdateRedisCache(keyPrefix = ConstantUtils.COMBO_CACHE_KEY_PREFIX)
    public ResponseResult<ResultCode> updateEnable(@RequestBody @CacheParam("comboId") ComboRequestParams requestParams){
        comboService.updateComboEnable(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

}
