package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.AddGoodsQo;
import com.qf.web.common.qo.GoodsQo;
import com.qf.web.common.utils.RedisUtils;
import com.qf.web.common.vo.GetGoodsInfoVo;
import com.qf.web.common.vo.ShopGoodsVo;
import com.qf.web.entity.*;
import com.qf.web.mapper.GoodsMapper;
import com.qf.web.mapper.GoodsPictureMapper;
import com.qf.web.service.GoodsService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private GoodsPictureMapper goodsPictureMapper;

    @Resource
    private ValueOperations<String,Object> valueOperations;

    @Resource
    private RedisTemplate redisTemplate;



    /**
     * 添加商品信息（默认商品图片为主图）
     * @param addGoodsQo
     * @return
     */
    @Override
    public Integer addGoodsInfo(AddGoodsQo addGoodsQo) {
        //使用到两个表的对象
        Goods goods = new Goods();
        GoodsPicture goodsPicture = new GoodsPicture();
        //将Qo 对象转换为 Po 对象
        BeanUtils.copyProperties(addGoodsQo, goods);
        BeanUtils.copyProperties(addGoodsQo, goodsPicture);
        //调用方法进行添加商品数据
        int count1 = goodsMapper.insertGoods(goods);
        //获取返回的商品自增的id
        int goodsId = goods.getGoodsId();
        if (count1 > 0){
            //对商品图片的属性进行赋值值操作
            goodsPicture.setGoodsId(goodsId);
            int count2 = goodsPictureMapper.insertPicture(goodsPicture);
            //获取返回的商品图片自增的id
            int goodsPictureId = goodsPicture.getGoodsPictureId();
            if (count2 > 0){
                return goodsPictureId;
            }
        }
        throw new ServiceException(ResultCode.ADD_INFO_ERROR);
    }

    /**
     * 添加商品信息（默认商品图片为副图）
     * @param addGoodsQo
     * @return
     */
    @Override
    public Integer addGoodsInfoAuxiliary(AddGoodsQo addGoodsQo) {
        //使用到两个表的对象
        Goods goods = new Goods();
        GoodsPicture goodsPicture = new GoodsPicture();
        //将Qo 对象转换为 Po 对象
        BeanUtils.copyProperties(addGoodsQo, goods);
        BeanUtils.copyProperties(addGoodsQo, goodsPicture);
        //调用方法进行添加商品数据
        int count1 = goodsMapper.insertGoods(goods);
        //获取返回的商品自增的id
        int goodsId = goods.getGoodsId();
        if (count1 > 0){
            //对商品图片的属性进行赋值值操作
            goodsPicture.setGoodsId(goodsId);
            int count2 = goodsPictureMapper.insertAuxiliaryPicture(goodsPicture);
            //获取返回的商品图片自增的id
            int goodsPictureId = goodsPicture.getGoodsPictureId();
            if (count2 > 0){
                return goodsPictureId;
            }
        }
        throw new ServiceException(ResultCode.ADD_INFO_ERROR);
    }

    /**
     * 查询在售状态商品信息
     * @param GoodsId
     * @return
     */
    @Override
    public List<GetGoodsInfoVo> selectGoodsInfoByGoodsId(int GoodsId) {
        //调用方法
        List<GetGoodsInfoVo> list = goodsMapper.selectGoodsInfoByGoodsId(GoodsId);
        if (!ObjectUtils.isEmpty(list)){
            return list;
        }
            throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 查询已下架状态商品信息
     * @param shopId
     * @return
     */
    @Override
    public List<GetGoodsInfoVo> selectGoodsSoldOutInfoByGoodsInfo(int shopId) {
        //调用方法
        List<GetGoodsInfoVo> list = goodsMapper.selectGoodsSoldOutInfoByGoodsInfo(shopId);
        if (!ObjectUtils.isEmpty(list)){
            return list;
        }
        throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 根据商品id修改商品信息
     * @param goodsQo
     * @return
     */
    @Override
    public Integer updateGoodsInfo(GoodsQo goodsQo) {
        //将Po 对象转换为Vo 对象
        Goods goods = new Goods();
        GoodsPicture goodsPicture = new GoodsPicture();
        //将商品图片Qo 对象转换为Po 对象
        BeanUtils.copyProperties(goodsQo, goods);
        BeanUtils.copyProperties(goodsQo, goodsPicture);
        //调用对应方法
        int count1 = goodsMapper.updateGoodsInfo(goods);
        if (count1 > 0){
            //图片不能为空
            int count2 = goodsPictureMapper.updatePicture(goodsPicture);
            if (count2 > 0){
                return count2;
            }
        }
        throw new ServiceException(ResultCode.UPDATE_INFO_ERROR);
    }

    /**
     * 根据对应的商品id 删除对应的商品信息（伪删除）
     * @param goodsId
     * @return
     */
    @Override
    public Integer deleteGoodsInfoByGoodsInfo(Integer goodsId) {
        int count1 = goodsMapper.deleteGoodsInfoByGoodsInfo(goodsId);
        if (count1 > 0){
            int count2 = goodsPictureMapper.deletePictureByGoodsId(goodsId);
            if (count2 > 0){
                return count2;
            }
        }
        throw new ServiceException(ResultCode.DELETE_INFO_ERROR);

    }

    /**
     * 恢复指定已被伪删除的商品信息
     * @param goodsId
     * @return
     */
    @Override
    public Integer regainGoodsInfoByGoodsInfo(Integer goodsId) {
        int count = goodsMapper.regainGoodsInfoByGoodsInfo(goodsId);
        if (count > 0){
            int count2 = goodsPictureMapper.regainPictureByGoodsId(goodsId);
            if (count2 > 0){
                return count2;
            }
        }
        throw new ServiceException(ResultCode.REGAIN_INFO_ERROR);
    }

    /**
     * 使用 redis 存储对象
     * @param goodsQo
     * @return
     */
    @Override
    public String addGoodsRedis(GoodsQo goodsQo) {
        //使用到两个表的对象
        Goods goods = new Goods();
        GoodsPicture goodsPicture = new GoodsPicture();
        //将Qo 对象转换为 Po 对象
        BeanUtils.copyProperties(goodsQo, goods);
        BeanUtils.copyProperties(goodsQo, goodsPicture);
        valueOperations.setIfAbsent(RedisUtils.STATUS.getRetMsg(), goodsQo,300, TimeUnit.MINUTES);
        //判断是否正确
        if (!ObjectUtils.isEmpty(valueOperations)){
            //调用方法进行添加商品数据
            int count1 = goodsMapper.insertGoods(goods);
            //获取返回的商品自增的id
            int goodsId = goods.getGoodsId();
            if (count1 > 0){
                //对商品图片的属性进行赋值值操作
                goodsPicture.setGoodsId(goodsId);
                int count2 = goodsPictureMapper.insertPicture(goodsPicture);
                //获取返回的商品图片自增的id
                int goodsPictureId = goodsPicture.getGoodsPictureId();
                if (count2 > 0){
                    return RedisUtils.STATUS.getRetMsg();
                }
            }
        }
        throw new ServiceException(ResultCode.ADD_INFO_ERROR);
    }

    /**
     * 给即将过期的商品重新添加时间,使用键存在则进行修改值
     * @param addGoodsQo
     * @return
     */
    @Override
    public String updateGoodsTimeRedis(AddGoodsQo addGoodsQo) {
        //将商品Qo 对象转换为Po 对象
        AddGoodsPo addGoodsPo = new AddGoodsPo();
        //将商品图片Qo 对象转换为Po 对象
        BeanUtils.copyProperties(addGoodsQo,addGoodsPo);
        //调用方法
        valueOperations.setIfPresent(RedisUtils.STATUS.getRetMsg(), addGoodsPo,300, TimeUnit.MINUTES);
        //判断是否为空
        if (!ObjectUtils.isEmpty(valueOperations)){
            return RedisUtils.STATUS.getRetMsg();
        }
        throw new ServiceException(ResultCode.ADD_INFO_ERROR);
    }

    /**
     * 获得指定 key 的 value信息，并返回剩余时间
     * @param redisKey
     * @return
     */
    @Override
    public Object getGoodsInfoByKey(String redisKey) {
        return valueOperations.get(redisKey);
    }


    /**
     * 返回指定限时商品剩余存活时间
     * @param redisKey
     * @return
     */
    @Override
    public Long getRedisKeyLifeTimes (String redisKey) {
        Long expire = redisTemplate.getExpire(redisKey,TimeUnit.SECONDS);
        if (expire > 0){
            return expire;
        }
        throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 根据店铺id获取指定店铺所有商品信息
     * @param shopId
     * @return
     */
    @Override
    public List<ShopGoodsVo> getAllGoodsInfoByShopId(int shopId) {
        List<ShopGoodsVo> list = goodsMapper.getAllGoodsInfoByShopId(shopId);
        //判断是否为空
        if (!ObjectUtils.isEmpty(list)){
            //返回 Vo 对象
            return list;
        }
        //否则抛出查询异常
        throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 根据店铺id获取指定店铺所有商品类型为单品的商品信息
     * @param shopId
     * @return
     */
    @Override
    public List<ShopGoodsVo> getAllSingleGoodsInfoByShopId(int shopId) {
        List<ShopGoodsVo> list = goodsMapper.getAllSingleGoodsInfoByShopId(shopId);
        if (!ObjectUtils.isEmpty(list)){
            return list;
        }
        throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 根据店铺id获取指定店铺所有商品类型为套餐的商品信息
     * @param shopId
     * @return
     */
    @Override
    public List<ShopGoodsVo> getAllSetMealGoodsInfoByShopId(int shopId) {
        List<ShopGoodsVo> list = goodsMapper.getAllSetMealGoodsInfoByShopId(shopId);
        if (!ObjectUtils.isEmpty(list)){
            return list;
        }
        throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 根据指定的商铺id 展示该店铺的所有商品。并根据商品价格升序（从低到高）
     * @param shopId
     * @return
     */
    @Override
    public List<ShopGoodsVo> getShopGoodsByPriceAsc(int shopId) {
        List<ShopGoodsVo> list = goodsMapper.getShopGoodsByPriceAsc(shopId);
        if (!ObjectUtils.isEmpty(list)){
            //返回Vo 对象
            return list;
        }
        //抛出查询异常
        throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 根据指定的商铺id 展示该店铺的所有商品。并根据商品价格降序（从高到低）
     * @param shopId
     * @return
     */
    @Override
    public List<ShopGoodsVo> getShopGoodsByPriceDesc(int shopId) {
        List<ShopGoodsVo> list = goodsMapper.getShopGoodsByPriceDesc(shopId);
        if (!ObjectUtils.isEmpty(list)){
            //返回Vo 对象
            return list;
        }
        //抛出查询异常
        throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 根据店铺id获取指定店铺所有商品信息,根据商品打包费升序
     * @param shopId
     * @return
     */
    @Override
    public List<ShopGoodsVo> getShopGoodsByPackMoneyAsc(int shopId) {
        List<ShopGoodsVo> list = goodsMapper.getShopGoodsByPackMoneyAsc(shopId);
        if (!ObjectUtils.isEmpty(list)){
            //返回Vo 对象
            return list;
        }
        //抛出查询异常
        throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 根据店铺id获取指定店铺所有商品信息,根据商品打包费降序
     * @param shopId
     * @return
     */
    @Override
    public List<ShopGoodsVo> getShopGoodsByPackMoneyDesc(int shopId) {
        List<ShopGoodsVo> list = goodsMapper.getShopGoodsByPackMoneyDesc(shopId);
        if (!ObjectUtils.isEmpty(list)){
            return list;
        }
        throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 根据商品图片id 将主图变为副图
     * @param goodsPictureId
     * @return
     */
    @Override
    public int updateMasterPicture(int goodsPictureId) {
        int count = goodsPictureMapper.updateMasterPicture(goodsPictureId);
        if (count > 0){
            return count;
        }
        throw new ServiceException(ResultCode.UPDATE_INFO_ERROR);
    }

    /**
     * 根据商品图片id 将主图变为副图
     * @param goodsPictureId
     * @return
     */
    @Override
    public int updateAuxiliaryPicture(int goodsPictureId) {
        int count = goodsPictureMapper.updateAuxiliaryPicture(goodsPictureId);
        if (count > 0){
            return count;
        }
        throw new ServiceException(ResultCode.UPDATE_INFO_ERROR);
    }

    /**
     * 根据商品的名字模糊查询对应的商品信息
     * @param goodsName
     * @return
     */
    @Override
    public List<ShopGoodsVo> getShopGoodsByLikeName(String goodsName) {
        List<ShopGoodsVo> list = goodsMapper.getShopGoodsByLikeName(goodsName);
        if (!ObjectUtils.isEmpty(list)){
            return list;
        }
        throw new ServiceException(ResultCode.SELECT_INFO_ERROR);
    }

    /**
     * 根据指定的商铺id 一键将下架的商品上架
     * @param shopId
     * @return
     */
    @Override
    public int updateAllGoodsPull(int shopId) {
        int count = goodsMapper.updateAllGoodsPull(shopId);
        if (count > 0){
            return count;
        }
        throw new ServiceException(ResultCode.UPDATE_INFO_ERROR);
    }

    /**
     * 根据指定的商铺id 一键将上架的商品下架
     * @param shopId
     * @return
     */
    @Override
    public int updateAllGoodsSoldOut(int shopId) {
        int count = goodsMapper.updateAllGoodsSoldOut(shopId);
        if (count > 0){
            return count;
        }
        throw new ServiceException(ResultCode.UPDATE_INFO_ERROR);
    }

    /**
     * 根据指定的商品id，单独已下架的商品进行上架
     * @param goodsId
     * @return
     */
    @Override
    public int updateSingleGoodsPull(int goodsId) {
        int count = goodsMapper.updateSingleGoodsPull(goodsId);
        if (count > 0){
            return count;
        }
        throw new ServiceException(ResultCode.UPDATE_INFO_ERROR);
    }

    /**
     * 根据指定的商品id，单独将已上架的商品进行下架
     * @param goodsId
     * @return
     */
    @Override
    public int updateSingleGoodsSoldOut(int goodsId) {
        int count = goodsMapper.updateSingleGoodsSoldOut(goodsId);
        if (count > 0){
            return count;
        }
        throw new ServiceException(ResultCode.UPDATE_INFO_ERROR);
    }


}
