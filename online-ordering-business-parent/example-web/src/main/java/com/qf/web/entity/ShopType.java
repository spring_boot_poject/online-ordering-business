package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "shop_type")
public class ShopType {
    /**
     * 主键
     */
    @TableId(value = "shop_type_id", type = IdType.INPUT)
    private Integer shopTypeId;

    /**
     * 类型名称
     */
    @TableField(value = "shop_type_name")
    private String shopTypeName;

    public static final String COL_SHOP_TYPE_ID = "shop_type_id";

    public static final String COL_SHOP_TYPE_NAME = "shop_type_name";
}