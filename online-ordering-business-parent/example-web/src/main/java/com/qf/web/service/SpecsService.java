package com.qf.web.service;

import com.qf.web.common.qo.SpecsRequestParams;
import com.qf.web.common.vo.SpecsVo;

import java.util.List;

public interface SpecsService {
    /**
     * 添加规格详细信息
     * @param specsRequestParams
     * @return
     */
    Integer addSpercs(SpecsRequestParams specsRequestParams);

    /**
     * 伪删除规格详细信息
     * @param specsId
     * @return
     */
    Integer removeSpecs(Integer specsId);

    /**
     * 修改规格详细信息
     * @param specsRequestParams
     * @return
     */
    Integer updateSpecs(SpecsRequestParams specsRequestParams);

    /**
     * 根据规格id获取对应的规格详细信息
     * @param goodsSpecsId
     * @return
     */
    List<SpecsVo> selectSpecsList(Integer goodsSpecsId);
}
