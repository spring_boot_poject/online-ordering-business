package com.qf.web.entity;

import com.qf.web.entity.GoodsPicture;
import lombok.Data;

import java.util.List;

/**
 * 显示前端商品对象
 */
@Data
public class GetGoodsInfoPo {
    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品名
     */
    private String goodsName;

    /**
     * 商品简介
     */
    private String goodsIntroduce;

    /**
     * 商品类别id
     */
    private Integer goodsTypeId;

    /**
     * 店铺id
     */
    private Integer shopId;

    /**
     * 商品价格
     */
    private Integer goodsPrice;

    /**
     * 规格模板id
     */
    private Integer templateId;

    /**
     * 包装费
     */
    private Integer goodsPackingFee;

    /**
     * 图片对象信息
     */
    private List<GoodsPicture> goodsPictureList;

}
