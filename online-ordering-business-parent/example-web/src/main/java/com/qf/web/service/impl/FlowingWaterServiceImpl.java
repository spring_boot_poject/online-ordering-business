package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.FlowingWaterRequestParams;
import com.qf.web.common.qo.ShopRequestParams;
import com.qf.web.common.qo.ShopkeeperQo;
import com.qf.web.common.vo.FlowingWaterVo;
import com.qf.web.common.vo.ShopOrdersVo;
import com.qf.web.entity.FlowingWater;
import com.qf.web.mapper.FlowingWaterMapper;
import com.qf.web.service.FlowingWaterService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class FlowingWaterServiceImpl implements FlowingWaterService {

    @Resource
    private FlowingWaterMapper flowingWaterMapper;

    @Override
    public Integer registerFlowingWater(ShopRequestParams shopRequestParams) {
        //qo转po
        FlowingWater water = new FlowingWater();
        BeanUtils.copyProperties(shopRequestParams,water);
        int rSet = flowingWaterMapper.insert(water);
        if (rSet <= 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        return rSet;
    }

    @Override
    public List<FlowingWaterVo> selectFlowingWater(FlowingWaterRequestParams flowingWaterRequestParams) {
        List<FlowingWaterVo> flowingWaterVoList = flowingWaterMapper.selectFlowingWater(flowingWaterRequestParams.getShopId());
        if (flowingWaterVoList.size()==0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //要存储的返回对象vo集合
        ArrayList<FlowingWaterVo> flowingWaterVos = new ArrayList<>();
        //遍历实体类（po）对象然后依次加进vo集合里
        for (FlowingWaterVo flowingWaterVo:flowingWaterVoList) {
            FlowingWaterVo waterVo = new FlowingWaterVo();
            //po转vo
            BeanUtils.copyProperties(flowingWaterVo,waterVo);
            boolean flag = flowingWaterVos.add(flowingWaterVo);
            if (!flag){
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        return flowingWaterVos;
    }

    @Override
    public List<FlowingWaterVo> selectDateFlowingWater(FlowingWaterRequestParams flowingWaterRequestParams) {
        List<FlowingWaterVo> flowingWaterVoList = flowingWaterMapper.selectDateFlowingWater(flowingWaterRequestParams.getShopId(),
                flowingWaterRequestParams.getStart(),
                flowingWaterRequestParams.getEnd());
        if (flowingWaterVoList.size()==0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //要存储的返回对象vo集合
        ArrayList<FlowingWaterVo> flowingWaterVos = new ArrayList<>();
        //遍历实体类（po）对象然后依次加进vo集合里
        for (FlowingWaterVo flowingWaterVo:flowingWaterVoList) {
            FlowingWaterVo waterVo = new FlowingWaterVo();
            //po转vo
            BeanUtils.copyProperties(flowingWaterVo,waterVo);
            boolean flag = flowingWaterVos.add(flowingWaterVo);
            if (!flag){
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        return flowingWaterVos;
    }

    @Override
    public Integer registerUpFlowingWater(ShopkeeperQo shopkeeperQo) {
        //qo转po
        FlowingWater water = new FlowingWater();
        BeanUtils.copyProperties(shopkeeperQo,water);
        int rSet = flowingWaterMapper.insert(water);
        if (rSet <= 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        return rSet;
    }

    @Override
    public List<FlowingWaterVo> selectUpFlowingWater(FlowingWaterRequestParams flowingWaterRequestParams) {
        List<FlowingWaterVo> flowingWaterVoList = flowingWaterMapper.selectUpFlowingWater(flowingWaterRequestParams.getShopkeeperId());
        if (flowingWaterVoList.size()==0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //要存储的返回对象vo集合
        ArrayList<FlowingWaterVo> flowingWaterVos = new ArrayList<>();
        //遍历实体类（po）对象然后依次加进vo集合里
        for (FlowingWaterVo flowingWaterVo:flowingWaterVoList) {
            FlowingWaterVo waterVo = new FlowingWaterVo();
            //po转vo
            BeanUtils.copyProperties(flowingWaterVo,waterVo);
            boolean flag = flowingWaterVos.add(flowingWaterVo);
            if (!flag){
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        return flowingWaterVos;
    }

    @Override
    public List<FlowingWaterVo> selectDateUpFlowingWater(FlowingWaterRequestParams flowingWaterRequestParams) {
        List<FlowingWaterVo> flowingWaterVoList = flowingWaterMapper.selectDateUpFlowingWater(flowingWaterRequestParams.getShopkeeperId(),
                flowingWaterRequestParams.getStart(),
                flowingWaterRequestParams.getEnd());
        if (flowingWaterVoList.size()==0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //要存储的返回对象vo集合
        ArrayList<FlowingWaterVo> flowingWaterVos = new ArrayList<>();
        //遍历实体类（po）对象然后依次加进vo集合里
        for (FlowingWaterVo flowingWaterVo:flowingWaterVoList) {
            FlowingWaterVo waterVo = new FlowingWaterVo();
            //po转vo
            BeanUtils.copyProperties(flowingWaterVo,waterVo);
            boolean flag = flowingWaterVos.add(flowingWaterVo);
            if (!flag){
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        return flowingWaterVos;
    }
}
