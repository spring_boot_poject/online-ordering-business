package com.qf.web.common.po;

import lombok.Data;

/**
 * 查询分页对象
 */
@Data
public class SelectGoodsPage {
    /**
     * 店铺id
     */
    private Integer shopId;

    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品名
     */
    private String goodsName;

    /**
     * 商品简介
     */
    private String goodsIntroduce;

    /**
     * 商品类别id
     */
    private Integer goodsTypeId;

    /**
     * 商品价格
     */
    private Integer goodsPrice;

    /**
     * 规格模板id
     */
    private Integer templateId;

    /**
     * 包装费
     */
    private Integer goodsPackingFee;
}
