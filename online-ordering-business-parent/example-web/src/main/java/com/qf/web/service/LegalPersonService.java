package com.qf.web.service;

import com.qf.web.common.qo.LegalPersonRequestParams;
import com.qf.web.common.vo.LegalPersonVo;

public interface LegalPersonService {
    /**
     * 注册法人信息
     * @param legalPersonRequestParams
     * @return
     */
    LegalPersonVo registerLegalPerson(LegalPersonRequestParams legalPersonRequestParams);

    /**
     * 上传法人身份证
     * @param legalPersonRequestParams
     * @return
     */
    Integer registerLegalImgStart(LegalPersonRequestParams legalPersonRequestParams);
    Integer registerLegalImgEnd(LegalPersonRequestParams legalPersonRequestParams);
}
