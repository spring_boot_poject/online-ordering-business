package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.LegalPerson;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LegalPersonMapper extends BaseMapper<LegalPerson> {
    /**
     * 先查询法人信息是否存在，根据身份证（唯一）来查询
     * @param idNumber
     * @return
     */
    default LegalPerson selectLegalPerson(String idNumber){
        QueryWrapper<LegalPerson> qw = new QueryWrapper<>();
        qw.select(LegalPerson.COL_LEGAL_PERSON_ID,
                LegalPerson.COL_GENDER,
                LegalPerson.COL_CERTIFICATE_TYPE,
                LegalPerson.COL_PERSON_START_VALIDITY,
                LegalPerson.COL_PERSON_END_VALIDITY,
                LegalPerson.COL_ENABLE,
                LegalPerson.COL_REVERSE_IMAGE_URL,
                LegalPerson.COL_OBVERSE_IMAGE_URL)
                .eq(LegalPerson.COL_ID_NUMBER,idNumber);
        return this.selectOne(qw);
    }

    /**
     * 上传法人身份信息
     * @param
     * @param
     * @return
     */
    Integer registerLegalImgStart(String riu,Integer legalPersonId);
    Integer registerLegalImgEnd(String oiu,Integer legalPersonId);
}