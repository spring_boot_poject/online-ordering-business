package com.qf.web.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class CouponVo{
    /**
     * 代金券Id
     */
    @TableId(value = "coupon_id", type = IdType.INPUT)
    private Integer couponId;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 优惠金额
     */
    @TableField(value = "coupon_price")
    private BigDecimal couponPrice;

    /**
     * 达标金额
     */
    @TableField(value = "tandards_price")
    private BigDecimal tandardsPrice;

    /**
     * 发放数量
     */
    @TableField(value = "released_quantity")
    private Integer releasedQuantity;

    /**
     * 已发放数量
     */
    @TableField(value = "coupon_receive_count")
    private Integer couponReceiveCount;

    /**
     * 0店铺新用户，1店铺所有用户
     */
    @TableField(value = "user_type")
    private Integer userType;

    /**
     * 发放开始日期
     */
    @TableField(value = "release_start_date")
    private Date releaseStartDate;

    /**
     * 发放结束日期
     */
    @TableField(value = "release_end_date")
    private Date releaseEndDate;

    /**
     * 可用开始时间
     */
    @TableField(value = "usable_start_date")
    private Date usableStartDate;

    /**
     * 到期时间
     */
    @TableField(value = "usable_end_date")
    private Date usableEndDate;

    /**
     * 0移动端，1桌面端
     */
    @TableField(value = "use_way")
    private Integer useWay;

    /**
     * 0与满减卷共享，1不共享
     */
    @TableField(value = "share_full_minus")
    private Integer shareFullMinus;

    /**
     * 优惠卷状态0，未开始，1进行中，2已结束
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 0删除，1未删除
     */
    @TableField(value = "is_delete")
    private Integer isDelete;
}
