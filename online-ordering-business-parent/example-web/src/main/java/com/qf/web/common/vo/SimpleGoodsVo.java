package com.qf.web.common.vo;

import lombok.Data;

@Data
public class SimpleGoodsVo {
    //商品名称
    private String goodsName;
    //商品数量
    private Integer goodsNumber;
    //商品价格
    private Double goodsPrice;

}
