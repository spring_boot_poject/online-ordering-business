package com.qf.web.common.qo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.qf.web.entity.ShopType;

public class ShopTypeRequestParams {
    /**
     * 类型名称
     */
    @TableField(value = "shop_type_name")
    private String shopTypeName;
}
