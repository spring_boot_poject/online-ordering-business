package com.qf.web.common.qo;

import lombok.Data;

/**
 * @author 51993
 */
@Data
public class MsgTemplateParam {
    private String code;
}
