package com.qf.web.service;

import com.qf.web.common.qo.EnvironmentRequestParams;
import com.qf.web.common.vo.EnvironmentPictureVo;

import java.util.List;

public interface EnvironmentPictureService {
    /**
     * 根据店铺id查询店铺环境图
     * @param environmentRequestParams
     * @return
     */
    List<EnvironmentPictureVo> selectEnvironmentInfo(EnvironmentRequestParams environmentRequestParams);

    /**
     * 根据店铺环境id修改店铺环境图
     * @param environmentRequestParams
     * @return
     */
    Integer updateShopEnvironmentPicture(EnvironmentRequestParams environmentRequestParams);
}
