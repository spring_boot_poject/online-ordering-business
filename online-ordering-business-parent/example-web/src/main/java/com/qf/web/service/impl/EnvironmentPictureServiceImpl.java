package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.EnvironmentRequestParams;
import com.qf.web.common.vo.EnvironmentPictureVo;
import com.qf.web.common.vo.SpecsTemplateVo;
import com.qf.web.entity.EnvironmentPicture;
import com.qf.web.mapper.EnvironmentPictureMapper;
import com.qf.web.service.EnvironmentPictureService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class EnvironmentPictureServiceImpl implements EnvironmentPictureService {
    @Resource
    private EnvironmentPictureMapper environmentPictureMapper;

    @Override
    public List<EnvironmentPictureVo> selectEnvironmentInfo(EnvironmentRequestParams environmentRequestParams) {
        List<EnvironmentPicture> pictures = environmentPictureMapper.selectShopEnvironmentInfo(environmentRequestParams.getShopId());
        if (pictures.size()==0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //要存储的返回对象vo集合
        ArrayList<EnvironmentPictureVo> pictureVos = new ArrayList<>();
        //遍历实体类（po）对象然后一次加进vo集合里
        for (EnvironmentPicture environmentPicture:pictures) {
            EnvironmentPictureVo pictureVo = new EnvironmentPictureVo();
            //po转vo
            BeanUtils.copyProperties(environmentPicture,pictureVo);
            boolean flag = pictureVos.add(pictureVo);
            if (!flag){
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        return pictureVos;
    }

    @Override
    public Integer updateShopEnvironmentPicture(EnvironmentRequestParams environmentRequestParams) {
        Integer rSet = environmentPictureMapper.updateShopEnvironmentPicture(environmentRequestParams.getEnvironmentPictureImg(),
                environmentRequestParams.getEnvironmentPictureId());
        if (rSet == 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        return rSet;
    }
}
