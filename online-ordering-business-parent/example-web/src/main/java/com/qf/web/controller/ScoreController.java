package com.qf.web.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.web.common.qo.ScoreQo;
import com.qf.web.service.ScoreService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/reply")
public class ScoreController {
    @Resource
    ScoreService scoreService;

    //1根据评论id商家进行回复
    @PostMapping("/reply/message")
    public ResponseResult<Integer> updateReplyMessageByScoreId(ScoreQo scoreQo){
        Integer integer = scoreService.updateReplyMessageByScoreId(scoreQo);
        return ResponseResult.success(integer);
    }
}
