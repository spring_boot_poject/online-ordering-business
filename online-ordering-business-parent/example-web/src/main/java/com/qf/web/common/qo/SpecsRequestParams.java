package com.qf.web.common.qo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class SpecsRequestParams {

    private Integer specsId;
    private Integer goodsSpecsId;
    private String specsInfo;

}
