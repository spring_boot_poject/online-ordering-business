package com.qf.web.common.vo;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class ShopOrdersVo {
    private String time;
    private Integer total;
    private BigDecimal prices;
    private BigDecimal pri;
    /**
     * 实际到手价格
     */
    private BigDecimal really;
    private Integer orderId;
    private BigDecimal price;
    /**
     * 范围查询的起始时间
     */
    private String startTimeDate;
    /**
     * 范围查询的末尾时间
     */
    private String endTimeDate;
}
