package com.qf.web.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.web.common.annotations.CacheParam;
import com.qf.web.common.annotations.EnableRedisCache;
import com.qf.web.common.qo.AddGoodsQo;
import com.qf.web.common.qo.GoodsQo;
import com.qf.web.common.utils.ConstantUtils;
import com.qf.web.common.vo.GetGoodsInfoVo;
import com.qf.web.common.vo.ShopGoodsVo;
import com.qf.web.entity.GoodsPicture;
import com.qf.web.service.GoodsService;
import com.qf.web.service.UploadService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * 作者：huang da long
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {
    //调用上传模块的service 层进行上传文件
    @Resource
    private UploadService uploadService;

    @Resource
    private GoodsService goodsService;

    /**
     * 添加商品类型为默认单品的商品信息并返回图片的id(默认商品图片为主图)
     * @param multipartFile
     * @param addGoodsQo
     * @return
     */
    @PostMapping("/add/goods")
    public ResponseResult<Integer> addGoodsInfo(@RequestBody MultipartFile multipartFile, @CacheParam AddGoodsQo addGoodsQo){
        //返回图片路径
        String imgUrl = uploadService.uploadImage(multipartFile, ConstantUtils.GOODS_IMAGE_BUCKET_NAME);
        //将图片url赋值给商品图片QO对象
        addGoodsQo.setGoodsPictureImg(imgUrl);
        //添加成功返回商品图片id
        int goodsPictureId = goodsService.addGoodsInfo(addGoodsQo);
        //返回成功存储的图片的id
        return ResponseResult.success(goodsPictureId);
    }


    /**
     * 添加商品类型为默认单品的商品信息并返回图片的id（默认商品图片为副图）
     * @param multipartFile
     * @param addGoodsQo
     * @return
     */
    @PostMapping("/add/goods/auxiliary")
    public ResponseResult<Integer> addGoodsInfoAuxiliary(@RequestBody MultipartFile multipartFile, @CacheParam AddGoodsQo addGoodsQo){
        //返回图片路径
        String imgUrl = uploadService.uploadImage(multipartFile, ConstantUtils.GOODS_IMAGE_BUCKET_NAME);
        //将图片url赋值给商品图片QO对象
        addGoodsQo.setGoodsPictureImg(imgUrl);
        //添加成功返回商品图片id
        int goodsPictureId = goodsService.addGoodsInfoAuxiliary(addGoodsQo);
        //返回成功存储的图片的id
        return ResponseResult.success(goodsPictureId);
    }




    /**
     * 通过商品id 获取指定商品信息
     * @param goodsQo
     * @return
     */
    @EnableRedisCache(keyPrefix = ConstantUtils.GOODS_CACHE_KEY_PREFIX)
    @GetMapping("/get/goods")
    public ResponseResult<List> getGoodsInfo(@CacheParam("/goods") GoodsQo goodsQo){
        List<GetGoodsInfoVo> list = goodsService.selectGoodsInfoByGoodsId(goodsQo.getGoodsId());
        return ResponseResult.success(list);
    }

    /**
     * 通过店铺id 获取所有已下架商品信息
     * @param goodsQo
     * @return
     */
    @GetMapping("/get/goods/sold/out")
    public ResponseResult<List> selectGoodsSoldOutInfoByGoodsInfo(GoodsQo goodsQo){
        List<GetGoodsInfoVo> list = goodsService.selectGoodsSoldOutInfoByGoodsInfo(goodsQo.getShopId());
        return ResponseResult.success(list);
    }

    /**
     * 根据店铺id获取指定店铺所有商品信息（可售状态，包含所有类型）
     * @param goodsQo
     * @return
     */
    @GetMapping("/get/all/goods")
    public ResponseResult<List> getAllGoodsInfoByShopId(GoodsQo goodsQo){
        List<ShopGoodsVo> list = goodsService.getAllGoodsInfoByShopId(goodsQo.getShopId());
        return ResponseResult.success(list);
    }


    /**
     * 根据店铺id获取指定店铺所有商品类型为单品的商品信息
     * @param goodsQo
     * @return
     */
    @GetMapping("/get/all/single/goods")
    public ResponseResult<List> getAllSingleGoodsInfoByShopId(GoodsQo goodsQo){
        List<ShopGoodsVo> list = goodsService.getAllSingleGoodsInfoByShopId(goodsQo.getShopId());
        return ResponseResult.success(list);
    }

    /**
     * 根据店铺id获取指定店铺所有商品类型为套餐的商品信息
     * @param goodsQo
     * @return
     */
    @GetMapping("/get/all/set/meal/goods")
    public ResponseResult<List> getAllSetMealGoodsInfoByShopId(GoodsQo goodsQo){
        List<ShopGoodsVo> list = goodsService.getAllSetMealGoodsInfoByShopId(goodsQo.getShopId());
        return ResponseResult.success(list);
    }


    /**
     * 通过指定的商品id 修改指定的商品信息
     * @param multipartFile
     * @param goodsQo
     * @return
     */
    @PutMapping("/update/goods")
    public ResponseResult<Integer> updateGoodsInfo(@RequestBody MultipartFile multipartFile, GoodsQo goodsQo){
        //返回图片路径
        String imgUrl = uploadService.uploadImage(multipartFile,ConstantUtils.GOODS_IMAGE_BUCKET_NAME);
        //将图片url赋值给商品图片QO对象
        goodsQo.setGoodsPictureImg(imgUrl);
        //添加成功返回商品图片id
        Integer count = goodsService.updateGoodsInfo(goodsQo);
        //响应影响行数
        return ResponseResult.success(count);
    }

    /**
     * 根据商品图片id 将主图变为副图
     * @param goodsPicture
     * @return
     */
    @PutMapping("/update/master/picture")
    public ResponseResult<Integer> updateMasterPicture(GoodsPicture goodsPicture){
        int count = goodsService.updateMasterPicture(goodsPicture.getGoodsPictureId());
        return ResponseResult.success(count);
    }

    /**
     * 根据商品图片id 将主图变为副图
     * @param goodsPicture
     * @return
     */
    @PutMapping("/update/auxiliary/picture")
    public ResponseResult<Integer> updateAuxiliaryPicture(GoodsPicture goodsPicture){
        int count = goodsService.updateAuxiliaryPicture(goodsPicture.getGoodsPictureId());
        return ResponseResult.success(count);
    }



    /**
     * 通过指定的商品id 伪删除指定的商品信息
     * @param goodsQo
     * @return
     */
    @DeleteMapping("/delete/goods")
    public ResponseResult<Integer> deleteGoodsInfoByGoodsInfo(GoodsQo goodsQo){
        Integer count = goodsService.deleteGoodsInfoByGoodsInfo(goodsQo.getGoodsId());
        return ResponseResult.success(count);
    }

    /**
     *  通过指定的商品id，恢复已被伪删除的商品信息
     * @param goodsQo
     * @return
     */
    @PutMapping("/regain/goods")
    public ResponseResult<Integer> regainGoodsInfoByGoodsInfo(GoodsQo goodsQo){
        Integer count = goodsService.regainGoodsInfoByGoodsInfo(goodsQo.getGoodsId());
        return ResponseResult.success(count);
    }




    /**
     * 根据商品姓名模糊查询对应的商品信息
     * @param goodsQo
     * @return
     */
    @GetMapping("/get/goods/like/name")
    public ResponseResult<List> getShopGoodsByLikeName(GoodsQo goodsQo){
        List<ShopGoodsVo> list = goodsService.getShopGoodsByLikeName(goodsQo.getGoodsName());
        return ResponseResult.success(list);
    }



    /**
     * 根据指定的商铺id 展示该店铺的所有商品。并根据商品价格降序（从低到高）
     * @param goodsQo
     * @return
     */
    @GetMapping("/get/redis/goods/price/asc")
    public ResponseResult<List> getShopGoodsByPriceAsc(GoodsQo goodsQo){
        List<ShopGoodsVo> list = goodsService.getShopGoodsByPriceAsc(goodsQo.getShopId());
        return ResponseResult.success(list);
    }

    /**
     * 根据指定的商铺id 展示该店铺的所有商品。并根据商品价格降序（从低到高）
     * @param goodsQo
     * @return
     */
    @GetMapping("/get/redis/goods/price/desc")
    public ResponseResult<List> getShopGoodsByPriceDesc(GoodsQo goodsQo){
        List<ShopGoodsVo> list = goodsService.getShopGoodsByPriceDesc(goodsQo.getShopId());
        return ResponseResult.success(list);
    }

    /**
     * 根据指定的商铺id，获取指定店铺的所有商品信息。根据打包费进行升序回显。
     * @param goodsQo
     * @return
     */
    @GetMapping("/get/redis/goods/pack/money/asc")
    public ResponseResult<List> getShopGoodsByPackMoneyAsc(GoodsQo goodsQo){
        List<ShopGoodsVo> list = goodsService.getShopGoodsByPackMoneyAsc(goodsQo.getShopId());
        return ResponseResult.success(list);
    }

    /**
     * 根据指定的商铺id，获取指定店铺的所有商品信息。根据打包费进行降序回显
     * @param goodsQo
     * @return
     */
    @GetMapping("/get/redis/goods/pack/money/desc")
    public ResponseResult<List> getShopGoodsByPackMoneyDesc(GoodsQo goodsQo){
        List<ShopGoodsVo> list = goodsService.getShopGoodsByPackMoneyDesc(goodsQo.getShopId());
        return ResponseResult.success(list);
    }


    /**
     * 根据指定的商铺id 一键将下架的商品上架
     * @param goodsQo
     * @return
     */
    @PutMapping("/update/all/goods/pull")
    public ResponseResult<Integer> updateAllGoodsPull(GoodsQo goodsQo){
        int count = goodsService.updateAllGoodsPull(goodsQo.getShopId());
        return ResponseResult.success(count);
    }

    /**
     * 根据指定的商铺id 一键将上架的商品下架
     * @param goodsQo
     * @return
     */
    @PutMapping("/update/all/goods/sold/out")
    public ResponseResult<Integer> updateAllGoodsSoldOut(GoodsQo goodsQo){
        int count = goodsService.updateAllGoodsSoldOut(goodsQo.getShopId());
        return ResponseResult.success(count);
    }


    /**
     * 根据指定的商品id，单独已下架的商品进行上架
     * @param goodsQo
     * @return
     */
    @PutMapping("/update/single/goods/pull")
    public ResponseResult<Integer> updateSingleGoodsPull(GoodsQo goodsQo){
        int count = goodsService.updateSingleGoodsPull(goodsQo.getGoodsId());
        return ResponseResult.success(count);
    }

    /**
     * 根据指定的商品id，单独将已上架的商品进行下架
     * @param goodsQo
     * @return
     */
    @PutMapping("/update/single/goods/sold/out")
    public ResponseResult<Integer> updateSingleGoodsSoldOut(GoodsQo goodsQo){
        int count = goodsService.updateSingleGoodsSoldOut(goodsQo.getGoodsId());
        return ResponseResult.success(count);
    }


    /**
     * 添加有期限的商品信息,返回key值。使用键存在则不进行修改值(Redis)
     * @param multipartFile
     * @param goodsQo
     * @return
     */
    @PostMapping("/add/redis/goods/")
    public ResponseResult<String> addGoodsRedis(@RequestBody MultipartFile multipartFile,GoodsQo goodsQo){
        //返回图片路径
        String imgUrl = uploadService.uploadImage(multipartFile,ConstantUtils.GOODS_IMAGE_BUCKET_NAME);
        //将图片url赋值给商品图片QO对象
        goodsQo.setGoodsPictureImg(imgUrl);
        String redisKey = goodsService.addGoodsRedis(goodsQo);
        return ResponseResult.success(redisKey);
    }



    /**
     * 获取指定key 的剩余生存时间
     * @param redisKey
     * @return
     */
    @EnableRedisCache(keyPrefix = ConstantUtils.GOODS_CACHE_KEY_PREFIX)
    @GetMapping("get/redis/key/life")
    public ResponseResult<Long> getRedisKeyLifeTimes(String redisKey){
        Long redisKeyLifeTimes = goodsService.getRedisKeyLifeTimes(redisKey);
        return ResponseResult.success(redisKeyLifeTimes);
    }



    /**
     * 给即将过期的商品重新添加时间,使用键存在则进行修改值(Redis)
     * @param multipartFile
     * @param addGoodsQo
     * @return
     */
    @PutMapping("/update/redis/goods/time")
    public ResponseResult<String> updateGoodsTimeRedis(@RequestBody MultipartFile multipartFile,@CacheParam AddGoodsQo addGoodsQo){
        //返回图片路径
        String imgUrl = uploadService.uploadImage(multipartFile,ConstantUtils.GOODS_IMAGE_BUCKET_NAME);
        //将图片url赋值给商品图片QO对象
        addGoodsQo.setGoodsPictureImg(imgUrl);
        String key = goodsService.updateGoodsTimeRedis(addGoodsQo);
        return ResponseResult.success(key);
    }


    /**
     * 通过 Key 返回指定商品的信息(Redis)
     * @param redisKey
     * @return
     */
    @EnableRedisCache(keyPrefix = ConstantUtils.GOODS_CACHE_KEY_PREFIX)
    @GetMapping("/get/redis/goods/info")
    public ResponseResult<Object> getGoodsInfoByKey(String redisKey){
        Object ob= goodsService.getGoodsInfoByKey(redisKey);
        return ResponseResult.success(ob);
    }


}
