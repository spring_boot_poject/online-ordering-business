package com.qf.web.controller;
import com.qf.common.base.result.ResponseResult;
import com.qf.web.common.vo.MyScoreVo;
import com.qf.web.service.MyScoreService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/my/score")
public class MyScoreController {
    @Resource
    MyScoreService myScoreService;

    //1根据商店id查看所有评价（评价派送员）
    @GetMapping("/get/my/score")
    public ResponseResult<List<MyScoreVo>> getMyScoreByShopId(int shopId){
        List<MyScoreVo> myScoreList= myScoreService.getMyScoreByShopId(shopId);
        return ResponseResult.success(myScoreList);
    }

}
