package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "environment_picture")
public class EnvironmentPicture {
    /**
     * 环境图片表的id
     */
    @TableId(value = "environment_picture_id", type = IdType.INPUT)
    private Integer environmentPictureId;

    /**
     * 环境图片
     */
    @TableField(value = "environment_picture_img")
    private String environmentPictureImg;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 当前图片的状态值，1启用，0代表不启用
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_ENVIRONMENT_PICTURE_ID = "environment_picture_id";

    public static final String COL_ENVIRONMENT_PICTURE_IMG = "environment_picture_img";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_IS_DELETE = "is_delete";
}