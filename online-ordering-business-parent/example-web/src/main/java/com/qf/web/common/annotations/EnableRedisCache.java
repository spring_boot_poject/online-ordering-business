package com.qf.web.common.annotations;
import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * @author 谢积威
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface EnableRedisCache{

    /**
     * key的前缀
     */
    String keyPrefix() default "";


    /**
     * 缓存过期时间
     */
    long timeout() default 1000;

    /**
     * 时间单位
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;
}
