package com.qf.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.CouponRequestParams;
import com.qf.web.common.vo.CouponVo;
import com.qf.web.entity.ActiveOrder;
import com.qf.web.entity.Coupon;
import com.qf.web.entity.CouponInfo;
import com.qf.web.mapper.CouponMapper;
import com.qf.web.service.CouponService;
import org.omg.CORBA.PUBLIC_MEMBER;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public class CouponServiceImpl implements CouponService {


    public static final Integer NUMBER_ZERO=0;

    public static final int DATE_TIME_LASTDAY = 0;
    public static final int DATE_TIME_TODAY = 1;
    public static final int DATE_TIME_NEARWEEK = 2;
    public static final int DATE_TIME_NEARMONTH = 3;

    public static final Integer  STATUS_BEFORE=0;
    public static final Integer  STATUS_BEING=1;
    public static final Integer  STATUS_END=2;


    public static final Integer LAST_DAY_NUM=- 1;
    public static final Integer LAST_WEEK_NUM=-6;
    public static final Integer LAST_MONTH_NUM=-1;

    @Resource
    CouponMapper couponMapper;


    @Override
    public Integer addCoupon(CouponRequestParams couponRequestParams){
        Coupon coupon = new Coupon();
        Date date = new Date();
        //q转p
        BeanUtils.copyProperties(couponRequestParams,coupon);
        //判断当前时间是否在发放时间之前
        if (date.before(couponRequestParams.getReleaseStartDate())){
            //设置状态值0
                coupon.setStatus(STATUS_BEFORE);
        }else if (date.after(couponRequestParams.getReleaseStartDate()) && date.before(couponRequestParams.getReleaseEndDate())){
            //设置状态值1
            coupon.setStatus(STATUS_BEING);
        }else {
            //设置状态值2
            coupon.setStatus(STATUS_END);
        }
        //插入
        int insert = couponMapper.insert(coupon);
        if(insert==NUMBER_ZERO){
            throw new ServiceException(ResultCode.DATA_IS_WRONG);
        }
        return insert;
    }

    @Override
    public List<CouponVo> selectCouponList(Integer shopId) {

        ArrayList<CouponVo> couponVosList = new ArrayList<>();
        //查询所有代金券
        List<Coupon> couponsList = couponMapper.selectAllCoupon(shopId);
        //判断是否存在
        if (!ObjectUtils.isEmpty(couponsList)){
            //po转换qo
            for (Coupon coupon:couponsList) {
                CouponVo couponVo = new CouponVo();
                BeanUtils.copyProperties(coupon,couponVo);
                couponVosList.add(couponVo);
            }
        }else {
            //抛出数据错误异常
            throw new ServiceException(ResultCode.DATA_IS_WRONG);
        }
        //返回
        return couponVosList;
    }

    @Override
    public List<CouponVo> selectSpecificCouponList(CouponRequestParams couponRequestParams) {
        Coupon coupon1 = new Coupon();
        //qo转换po
        BeanUtils.copyProperties(couponRequestParams,coupon1);
        //根据特点条件查询代金券
        List<Coupon> couponList = couponMapper.selectCouponListByStatusAndReleaseDate(coupon1);
        //判断是否元素
        if (!ObjectUtils.isEmpty(couponList)){
            ArrayList<CouponVo> couponVosList = new ArrayList<>();
            //po转qo
            for (Coupon coupon:couponList) {
                CouponVo couponVo = new CouponVo();
                BeanUtils.copyProperties(coupon,couponVo);
                couponVosList.add(couponVo);
            }
            //返回
            return couponVosList;

        }else {
            throw new ServiceException(ResultCode.DATA_IS_WRONG);
        }

    }

    @Override
    public List<CouponInfo> selectCouponInfo(Integer shopId) {
        //根据店铺id查询代金券信息
        List<CouponInfo> couponInfos = couponMapper.selectCouponInfoByShopId(shopId);
        return couponInfos;
    }

    /**
     * 数据分析
     * @param shopId
     * @param dateType
     * @return
     */
    @Override
    public ActiveOrder selectActiveOrderInfo(Integer shopId, Integer dateType) {
        SimpleDateFormat format=null;
        String day=null;
        ActiveOrder activeOrder=null;
        ActiveOrder activeOrder1=null;
        SimpleDateFormat format1=null;
        String nowTime=null;
        String before=null;

        switch (dateType){
            //0昨天
            case DATE_TIME_LASTDAY:

                format = new SimpleDateFormat("yyyy-MM-dd");
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(new Date());
                calendar.add(Calendar.DATE, - LAST_DAY_NUM);
                Date d = calendar.getTime();
                day = format.format(d);

                 activeOrder = couponMapper.selectOrderInfoByShopIdAndOrderTime(shopId, day);
                if (ObjectUtils.isEmpty(activeOrder)){
                    throw new ServiceException(ResultCode.DATA_IS_WRONG);
                }
                 activeOrder1 = couponMapper.selectActiveOrdersCountByShopIdAndOrderDate(shopId, day);
                if (ObjectUtils.isEmpty(activeOrder1)){
                    throw new ServiceException(ResultCode.DATA_IS_WRONG);
                }
                activeOrder.setActiveOrderCount(activeOrder1.getActiveOrderCount());
                activeOrder.setActiveOrderInComePirce(activeOrder1.getActiveOrderInComePirce());
                break;

            //1今天
            case DATE_TIME_TODAY:

                Date date = new Date();
                format = new SimpleDateFormat("yyyy-MM-dd");
                day = format.format(date);

                 activeOrder = couponMapper.selectOrderInfoByShopIdAndOrderTime(shopId, day);
                if (ObjectUtils.isEmpty(activeOrder)){
                    throw new ServiceException(ResultCode.DATA_IS_WRONG);
                }
                 activeOrder1 = couponMapper.selectActiveOrdersCountByShopIdAndOrderDate(shopId, day);
                if (ObjectUtils.isEmpty(activeOrder1)){
                    throw new ServiceException(ResultCode.DATA_IS_WRONG);
                }
                activeOrder.setActiveOrderCount(activeOrder1.getActiveOrderCount());
                activeOrder.setActiveOrderInComePirce(activeOrder1.getActiveOrderInComePirce());

                break;
            //2七天前
            case DATE_TIME_NEARWEEK:
                format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                calendar = Calendar.getInstance();
                calendar.add(Calendar.DAY_OF_MONTH, LAST_WEEK_NUM);
                before = format.format(calendar.getTime())+" 00:00:00";

                date = new Date();
                format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                nowTime = format1.format(date);


                activeOrder = couponMapper.selectActiveOrderInfoByShopIdAndNearTime(shopId, before,nowTime);
                activeOrder1 = couponMapper.selectOrderInfoByShopIdAndNearTime(shopId, before,nowTime);
                activeOrder.setOrderCount(activeOrder1.getOrderCount());
                activeOrder.setOrderInComePrice(activeOrder1.getOrderInComePrice());
               break;
               //3最近一个月
            case DATE_TIME_NEARMONTH:

                format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                calendar= Calendar.getInstance();
                calendar.add(Calendar.MONTH, LAST_MONTH_NUM);
                before = format.format(calendar.getTime())+" 00:00:00";

                date = new Date();
                format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                nowTime = format1.format(date);


                activeOrder = couponMapper.selectActiveOrderInfoByShopIdAndNearTime(shopId, before,nowTime);
                activeOrder1 = couponMapper.selectOrderInfoByShopIdAndNearTime(shopId, before,nowTime);
                activeOrder.setOrderCount(activeOrder1.getOrderCount());

                activeOrder.setOrderInComePrice(activeOrder1.getOrderInComePrice());
                break;
            default:
                throw new ServiceException(ResultCode.DATA_IS_WRONG);

        }
        return activeOrder;
    }

    /**
     * 修改发放状态
     * @param couponId
     * @param isDelete
     * @return
     */
    @Override
    public Integer updateGiveOutStatus(Integer couponId, Integer isDelete) {
        Integer integer = couponMapper.updateGiveOutStatus(couponId, isDelete);
        if (integer==NUMBER_ZERO){
            throw new ServiceException(ResultCode.DATA_IS_WRONG);
        }
        return integer;
    }
}
