package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.Shop;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ShopMapper extends BaseMapper<Shop> {
    /**
     * 查询商家店铺信息
     *
     * @param id
     * @return
     */
    List<Shop> selectShopInfo(Integer id);

    /**
     * 选择店铺类型
     */
    default Shop selectShopTypeInfo(Integer shopTypeId) {
        QueryWrapper<Shop> qw = new QueryWrapper<>();
        qw.select(Shop.COL_SHOP_TYPE_ID)
                .eq(Shop.COL_SHOP_TYPE_ID, shopTypeId);
        return this.selectOne(qw);
    }

    /**
     * 获取店铺状态（营业状态 4.待营业，5.营业中，6.休息中）
     *
     * @return shopId
     */
    Shop selectShopStatus(Integer shopId);

    /**
     * 修改店铺状态
     *
     * @return shopId
     */
    Integer updateShopStatus(@Param("state") Integer status, @Param("shopId") Integer shopId);

    /**
     * 根据店铺id修改店铺的头像
     * @param shopId
     * @param img
     * @return
     */
    Integer updateShopImg(String img,Integer shopId);

    /**
     * 根据店铺id查看店铺账户余额
     * @param shopId
     * @return
     */
    Shop selectShopAccount(Integer shopId);

    /**
     * 提现操作中店铺id和提现金钱来实现店铺余额减少
     * @return
     */
    Integer updateShopAccount(Integer money,Integer shopId);




}