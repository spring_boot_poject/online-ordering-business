package com.qf.web.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.ShopManageRequestParams;
import com.qf.web.common.qo.ShopRequestParams;
import com.qf.web.common.vo.ShopManageVo;
import com.qf.web.service.ShopManageService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/shop/manage")
public class ShopManageController {

    @Resource
    private ShopManageService shopManageService;

    @GetMapping("/select/shop/manage/info")
    public ResponseResult<ShopManageVo> selectAllShopManageInfo(@RequestBody ShopManageRequestParams shopManageRequestParams){
        ShopManageVo shopManageVo = shopManageService.selectAllShopManageInfo(shopManageRequestParams);
        return ResponseResult.success(shopManageVo);
    }

    @PostMapping("/update/shop/manage/info")
    public ResponseResult<ResultCode> updateShopManageInfo(@RequestBody ShopRequestParams shopRequestParams){
        Integer integer = shopManageService.updateShopManageInfo(shopRequestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
