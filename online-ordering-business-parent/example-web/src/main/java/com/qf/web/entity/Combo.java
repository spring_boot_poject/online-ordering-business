package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 套餐表
 * @author 51993
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "combo")
public class Combo {
    /**
     * 主键
     */
    @TableId(value = "combo_id", type = IdType.INPUT)
    private Integer comboId;

    /**
     * 套餐名
     */
    @TableField(value = "combo_name")
    private String comboName;

    /**
     * 套餐单人价格
     */
    @TableField(value = "combo_single_price")
    private BigDecimal comboSinglePrice;

    /**
     * 套餐多人价格
     */
    @TableField(value = "combo_multi_price")
    private BigDecimal comboMultiPrice;

    /**
     * 套餐类型，1单人团，0多人团
     */
    @TableField(value = "combo_type")
    private Integer comboType;

    /**
     * 套餐多人团人数
     */
    @TableField(value = "combo_number")
    private Integer comboNumber;

    /**
     * 套餐开始日期
     */
    @TableField(value = "combo_start_date")
    private Date comboStartDate;

    /**
     * 套餐结束日期
     */
    @TableField(value = "combo_end_date")
    private Date comboEndDate;

    /**
     * 套餐排除开始时间
     */
    @TableField(value = "combo_exclude_start_date")
    private Date comboExcludeStartDate;

    /**
     * 套餐排除结束时间
     */
    @TableField(value = "combo_exclude_end_date")
    private Date comboExcludeEndDate;

    /**
     * 使用星期
     */
    @TableField(value = "combo_use_week")
    private String comboUseWeek;

    /**
     * 使用时间类型，1全天，0自定义
     */
    @TableField(value = "use_time_type")
    private Integer useTimeType;

    /**
     * 是否需要预约，1需要，0不需要
     */
    @TableField(value = "is_order")
    private Integer isOrder;

    /**
     * 是否启用1启用0禁用，默认1
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    /**
     * 是否启用优惠，1启用，0禁用，默认1
     */
    @TableField(value = "`enable`")
    private Integer enable;

    /**
     * 店铺ID
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 使用时间
     */
    @TableField(value = "use_time")
    private String useTime;
}