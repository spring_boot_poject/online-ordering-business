package com.qf.web.config;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.qf.web.common.utils.ShiroUtils;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.mgt.SessionsSecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.web.config.DefaultShiroFilterChainDefinition;
import org.apache.shiro.spring.web.config.ShiroFilterChainDefinition;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.crazycake.shiro.RedisCacheManager;
import org.crazycake.shiro.RedisSessionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.ArrayList;

/**
 * @author 51993
 */
@Configuration
public class ShiroConfig {

    @Autowired
    RedisSessionDAO redisSessionDAO;

    @Autowired
    RedisCacheManager redisCacheManager;

    @Bean
    public UserCodeRealm userCodeRealm(){
        return new UserCodeRealm();
    }

    @Bean
    public UserPwdRealm userPwdRealm(HashedCredentialsMatcher hashedCredentialsMatcher){
        UserPwdRealm userRealm = new UserPwdRealm();
        userRealm.setCredentialsMatcher(hashedCredentialsMatcher);
        return userRealm;
    }

    @Bean
    public SessionsSecurityManager securityManager(UserCodeRealm userCodeRealm,UserPwdRealm userPwdRealm, CustomDefaultWebSessionManager defaultWebSessionManager){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        ArrayList<Realm> realms = new ArrayList<>();
        realms.add(userPwdRealm);
        realms.add(userCodeRealm);
        securityManager.setRealms(realms);
        securityManager.setSessionManager(defaultWebSessionManager);
        securityManager.setCacheManager(redisCacheManager);
        return securityManager;
    }

    @Bean
    public ShiroFilterChainDefinition filterChainDefinition(){
        DefaultShiroFilterChainDefinition shiroFilterChainDefinition = new DefaultShiroFilterChainDefinition();
        //不需要认证的
        shiroFilterChainDefinition.addPathDefinition("/register", "anon");
        shiroFilterChainDefinition.addPathDefinition("/banker/no/login","anon");
        shiroFilterChainDefinition.addPathDefinition("/shop/**","anon");
        shiroFilterChainDefinition.addPathDefinition("/legal/**","anon");
        shiroFilterChainDefinition.addPathDefinition("/shopType/**","anon");
        shiroFilterChainDefinition.addPathDefinition("/trading/**","anon");
        shiroFilterChainDefinition.addPathDefinition("/combo/**","anon");
        shiroFilterChainDefinition.addPathDefinition("/rule/**","anon");
        shiroFilterChainDefinition.addPathDefinition("/banker/login","anon");
        shiroFilterChainDefinition.addPathDefinition("/banker/login/msg","anon");
        shiroFilterChainDefinition.addPathDefinition("/goods/**","anon");
        // 其他所有都需要认证
        shiroFilterChainDefinition.addPathDefinition("/**", "authc");
        return shiroFilterChainDefinition;
    }

    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher(){
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName(Md5Hash.ALGORITHM_NAME);
        hashedCredentialsMatcher.setHashIterations(ShiroUtils.ITERATIONS);
        return hashedCredentialsMatcher;
    }

    @Bean
    public CustomDefaultWebSessionManager defaultWebSessionManager(){
        CustomDefaultWebSessionManager sessionManager = new CustomDefaultWebSessionManager();
        sessionManager.setSessionDAO(redisSessionDAO);
        sessionManager.setCacheManager(redisCacheManager);
        return sessionManager;
    }

}
