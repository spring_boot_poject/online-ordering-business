package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
    * 存储套餐图片
    */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "combo_image")
public class ComboImage {
    /**
     * 主键
     */
    @TableId(value = "image_id", type = IdType.INPUT)
    private Integer imageId;

    /**
     * 图片URL
     */
    @TableField(value = "image_url")
    private String imageUrl;

    /**
     * 套餐ID
     */
    @TableField(value = "combo_id")
    private Integer comboId;

    /**
     * 是否删除，默认0，未删除
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    /**
     * 是否主图 1是 0不是
     */
    @TableField(value = "main_image")
    private Integer mainImage;
}