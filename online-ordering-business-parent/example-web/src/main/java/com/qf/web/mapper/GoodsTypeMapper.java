package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.GoodsType;

public interface GoodsTypeMapper extends BaseMapper<GoodsType> {
}