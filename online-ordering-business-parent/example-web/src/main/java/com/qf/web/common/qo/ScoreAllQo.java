package com.qf.web.common.qo;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.qf.web.entity.ScoreImg;
import lombok.Data;

import java.util.Date;
import java.util.List;
@Data
public class ScoreAllQo {
    //评价id
    private Integer scoreId;
    //用户名
    private String userName;
    //评价时间
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date scoreTime;
    //商家评分
    private Integer score;
    //味道评分
    private Integer scoreTaste;
    //包装评分
    private Integer scorePack;
    //评价内容
    private String scoreMessage;
    //评价图片
    private List<ScoreImg> scoreImgList;
}
