package com.qf.web.common.qo;



import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class LegalPersonRequestParams {

    /**
     * 主键
     */
    private Integer legalPersonId;

    /**
     * 性别，1男，0女
     */
    private Integer gender;

    /**
     * 证件类型ID
     */
    private String certificateType;

    /**
     * 证件号码
     */
    private String idNumber;

    /**
     * 有效期开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date personStartValidity;

    /**
     * 有效期结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date personEndValidity;

    /**
     * 证件反面url
     */
    private String reverseImageUrl;

    /**
     * 证件正面url
     */
    private String obverseImageUrl;

}
