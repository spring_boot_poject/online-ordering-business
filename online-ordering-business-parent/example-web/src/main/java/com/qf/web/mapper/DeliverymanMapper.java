package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.Deliveryman;

public interface DeliverymanMapper extends BaseMapper<Deliveryman> {
}