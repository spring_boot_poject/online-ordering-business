package com.qf.web.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;

import java.util.Date;

/**
 * @ClassName FullMinusVo
 * @Description 优惠的详细信息查询返回
 * @Author zhenyang
 * @Date 2022/3/31 19:05
 **/
@Data
@ToString
public class FullMinusVo {
    @TableId(value = "full_minus_id", type = IdType.INPUT)
    private Integer fullMinusId;

    /**
     * 满减门槛
     */
    @TableField(value = "full_minus_threshold")
    private Double fullMinusThreshold;

    /**
     * 满减金额
     */
    @TableField(value = "full_minus_money")
    private Double fullMinusMoney;

    /**
     * 创建时间
     */
    @TableField(value = "full_minus_create")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date fullMinusCreate;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 满减标题
     */
    @TableField(value = "full_minus_title")
    private String fullMinusTitle;

    /**
     * 满减状态。1代表开始满减，0代表关闭满减
     */
    @TableField(value = "is_delete")
    private Integer isDelete;
}
