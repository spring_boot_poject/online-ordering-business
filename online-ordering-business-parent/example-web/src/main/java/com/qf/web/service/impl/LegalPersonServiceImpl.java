package com.qf.web.common.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.LegalPersonRequestParams;
import com.qf.web.common.vo.LegalPersonVo;
import com.qf.web.entity.LegalPerson;
import com.qf.web.mapper.LegalPersonMapper;
import com.qf.web.service.LegalPersonService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

@Service
public class LegalPersonServiceImpl implements LegalPersonService {
    @Resource
    private LegalPersonMapper legalPersonMapper;

    @Override
    public LegalPersonVo registerLegalPerson(LegalPersonRequestParams legalPersonRequestParams) {
        LegalPerson legalPerson = legalPersonMapper.selectLegalPerson(legalPersonRequestParams.getIdNumber());
        if (ObjectUtils.isEmpty(legalPerson)){
            legalPerson = new LegalPerson();
            //qo转po
            BeanUtils.copyProperties(legalPersonRequestParams,legalPerson);
            int i = legalPersonMapper.insert(legalPerson);
            if (i > 0){
                //po转vo
                LegalPersonVo legalPersonVo = new LegalPersonVo();
                BeanUtils.copyProperties(legalPerson,legalPersonVo);
                return legalPersonVo;
            }else {
                throw new ServiceException(ResultCode.ERROR);
            }
        }else {
            throw new ServiceException(ResultCode.ACCOUNT_IS_EXISTENT);
        }
    }

    @Override
    public Integer registerLegalImgStart(LegalPersonRequestParams legalPersonRequestParams) {
        Integer rSet = legalPersonMapper.registerLegalImgStart(legalPersonRequestParams.getReverseImageUrl(),
                legalPersonRequestParams.getLegalPersonId());
        if (rSet == 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        return rSet;
    }

    @Override
    public Integer registerLegalImgEnd(LegalPersonRequestParams legalPersonRequestParams) {
        Integer rSet = legalPersonMapper.registerLegalImgEnd(legalPersonRequestParams.getObverseImageUrl(),
                legalPersonRequestParams.getLegalPersonId());
        if (rSet == 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        return rSet;
    }

}
