package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.ComboRule;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 51993
 */
public interface ComboRuleMapper extends BaseMapper<ComboRule> {

    /**
     * 插入套餐和规则的关联关系
     * @param cid
     * @param rid
     * @return
     */
    Integer insertComboRuleRelation(@Param("cid") Integer cid, @Param("rid") Integer rid);

    /**
     * 根据店铺ID查询店铺的使用规则
     * @param id
     * @return
     */
    List<ComboRule> selectByShop(Integer id);

    /**
     * 修改使用规则内容
     * @param comboRule
     * @return
     */
    Integer updateRule(ComboRule comboRule);

    /**
     * 根据套餐ID删除套餐和使用规则关系表中的记录
     * @param comboId
     * @return
     */
    Integer deleteComboRuleRelation(Integer comboId);

    /**
     * 将使用规则状态设置为删除状态
     * @param ruleId
     * @return
     */
    Integer updateRuleToDel(Integer ruleId);
}