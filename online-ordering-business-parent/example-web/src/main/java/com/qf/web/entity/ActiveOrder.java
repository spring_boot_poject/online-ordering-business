package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class ActiveOrder {

    @TableField("active_order_count")
    private Integer activeOrderCount;

    @TableField("active_order_price")
    private Double activeOrderInComePirce;

    @TableField("order_count")
    private Integer orderCount;

    @TableField("order_price")
    private Double orderInComePrice;
}
