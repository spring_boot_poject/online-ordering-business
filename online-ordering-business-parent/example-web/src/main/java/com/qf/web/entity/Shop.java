package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "shop")
public class Shop {
    /**
     * 店铺id
     */
    @TableId(value = "shop_id", type = IdType.INPUT)
    private Integer shopId;

    /**
     * 店铺名
     */
    @TableField(value = "shop_name")
    private String shopName;

    /**
     * 商家id
     */
    @TableField(value = "shopkeeper_id")
    private Integer shopkeeperId;

    /**
     * 店铺简介
     */
    @TableField(value = "shop_introduce")
    private String shopIntroduce;

    /**
     * 店铺地址
     */
    @TableField(value = "shop_addrees")
    private String shopAddrees;

    /**
     * 店铺联系电话
     */
    @TableField(value = "shop_phone")

    private String shopPhone;

    /**
     * 店铺头像
     */
    @TableField(value = "shop_img")
    private String shopImg;

    /**
<<<<<<< HEAD
     * 营业执照
     */
    @TableField(value = "shop_license")
    private String shopLicense;

    /**
     * 卫生许可证
     */
    @TableField(value = "shop_hygiene")
    private String shopHygiene;

    /**
=======
>>>>>>> origin/master
     * 账户
     */
    @TableField(value = "shop_account")
    private Integer shopAccount;

    /**
     * 起送金额
     */
    @TableField(value = "shop_start_money")
    private Integer shopStartMoney;

    /**
     * 开始营业时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "shop_start_time")
    private Date shopStartTime;

    /**
     * 结束营业时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "shop_end_time")
    private Date shopEndTime;

    /**
     * 店铺状态。1代表营业，0代表休息
     */
    @TableField(value = "shop_work_state")
    private Integer shopWorkState;

    /**
     * 伪删除
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    /**
     * 店铺主营品类ID
     */
    @TableField(value = "category_id")
    private Integer categoryId;

    /**
     * 店铺类型ID
     */
    @TableField(value = "shop_type_id")
    private Integer shopTypeId;

    /**
     * 法人身份信息ID
     */
    @TableField(value = "legal_person_id")
    private Integer legalPersonId;

    /**
     * 店铺状态，1正常，0注销，2审核，3违规
     */
    @TableField(value = "`status`")
    private Integer status;

    @TableField(value = "distribution_mode")
    private String distributionMode;

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_SHOP_NAME = "shop_name";

    public static final String COL_SHOPKEEPER_ID = "shopkeeper_id";

    public static final String COL_SHOP_INTRODUCE = "shop_introduce";

    public static final String COL_SHOP_ADDREES = "shop_addrees";

    public static final String COL_SHOP_PHONE = "shop_phone";

    public static final String COL_SHOP_IMG = "shop_img";

    public static final String COL_SHOP_LICENSE = "shop_license";

    public static final String COL_SHOP_HYGIENE = "shop_hygiene";

    public static final String COL_SHOP_ACCOUNT = "shop_account";

    public static final String COL_SHOP_START_MONEY = "shop_start_money";

    public static final String COL_SHOP_START_TIME = "shop_start_time";

    public static final String COL_SHOP_END_TIME = "shop_end_time";

    public static final String COL_SHOP_WORK_STATE = "shop_work_state";

    public static final String COL_IS_DELETE = "is_delete";

    public static final String COL_CATEGORY_ID = "category_id";

    public static final String COL_SHOP_TYPE_ID = "shop_type_id";

    public static final String COL_LEGAL_PERSON_ID = "legal_person_id";

    public static final String COL_STATUS = "status";

    public static final String COL_DISTRIBUTION_MODE = "distribution_mode";

}