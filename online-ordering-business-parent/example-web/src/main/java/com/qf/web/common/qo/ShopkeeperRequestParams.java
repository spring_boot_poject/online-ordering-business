package com.qf.web.common.qo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author 51993
 */

@Data
public class ShopkeeperRequestParams {
    /**
     * 商家id
     */
    private Integer shopkeeperId;

    /**
     * 商家昵称
     */
    private String shopkeeperName;

    /**
     * 商家电话
     */
    private String shopkeeperPhone;

    /**
     * 商家登录密码
     */
    private String shopkeeperPassword;

    /**
     * 商家支付密码
     */
    private String shopkeeperPayPassword;

    /**
     * 商家头像
     */
    private String shopkeeperImg;

    /**
     * 商户注册时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date registerTime;

    /**
     * 忘记支付密码是时输入的验证码
     */
    private String payCode;


}
