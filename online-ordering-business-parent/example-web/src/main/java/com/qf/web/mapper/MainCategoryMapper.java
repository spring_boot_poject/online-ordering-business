package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.MainCategory;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface MainCategoryMapper extends BaseMapper<MainCategory> {
}