package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.GoodsSpecsRequestParams;
import com.qf.web.common.vo.GoodsSpcesVo;
import com.qf.web.entity.GoodsSpecsPo;
import com.qf.web.entity.SpecsPo;
import com.qf.web.mapper.GoodsSpecsMapper;
import com.qf.web.mapper.SpecsMapper;
import com.qf.web.service.GoodsSpecsService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class GoodsSpecsServiceImpl implements GoodsSpecsService {
    private static final Integer NUMBER_ZERO=0;
    @Resource
    GoodsSpecsMapper goodsSpecsMapper;
    @Resource
    SpecsMapper specsMapper;

    @Override
    public Integer addGoodsSpecs(GoodsSpecsRequestParams goodsSpecsRequestParams) {
        GoodsSpecsPo goodsSpecsPo = new GoodsSpecsPo();
        BeanUtils.copyProperties(goodsSpecsRequestParams,goodsSpecsPo);
        //插入规格并返回id
        Integer integer = goodsSpecsMapper.insertGoodsSpecs(goodsSpecsPo);
        if (integer==NUMBER_ZERO || integer==null){
            throw new ServiceException(ResultCode.ERROR);
        }
        //过去所有的规格详细信息
        List<SpecsPo> specsPoslist = goodsSpecsPo.getSpecsPos();
        if (specsPoslist.size()!=NUMBER_ZERO){
            for (SpecsPo specsPo:specsPoslist) {
                specsPo.setGoodsSpecsId(goodsSpecsPo.getGoodsSpecsId());
                //插入规格详细信息
                Integer inserspecs = specsMapper.inserspecs(specsPo);
                if (inserspecs==NUMBER_ZERO || inserspecs==null){
                    throw new ServiceException(ResultCode.ERROR);
                }
            }
        }
        return goodsSpecsPo.getGoodsSpecsId();
    }

    @Override
    public List<GoodsSpcesVo> selectCommonGoodsSpeces() {
        List<GoodsSpecsPo> goodsSpecsPosList = goodsSpecsMapper.selectCommonGoodsSpecsList(0);
        if (goodsSpecsPosList.size()==NUMBER_ZERO){
            throw new ServiceException(ResultCode.ERROR);
        }
        ArrayList<GoodsSpcesVo> goodsSpcesVosList = new ArrayList<>();

        for (GoodsSpecsPo goodsSpecsPo:goodsSpecsPosList) {
            GoodsSpcesVo goodsSpcesVo = new GoodsSpcesVo();
            BeanUtils.copyProperties(goodsSpecsPo,goodsSpcesVo);
            goodsSpcesVosList.add(goodsSpcesVo);
        }
        return goodsSpcesVosList;
    }

    @Override
    public List<GoodsSpcesVo> selectShopGoodsSpeces(GoodsSpecsRequestParams goodsSpecsRequestParams) {
        Integer shopId = goodsSpecsRequestParams.getShopId();
        List<GoodsSpecsPo> goodsSpecsPosList = goodsSpecsMapper.selectCommonGoodsSpecsList(shopId);
        if (goodsSpecsPosList.size()==NUMBER_ZERO){
            throw new ServiceException(ResultCode.ERROR);
        }
        ArrayList<GoodsSpcesVo> goodsSpcesVosList = new ArrayList<>();

        for (GoodsSpecsPo goodsSpecsPo:goodsSpecsPosList) {
            GoodsSpcesVo goodsSpcesVo = new GoodsSpcesVo();
            BeanUtils.copyProperties(goodsSpecsPo,goodsSpcesVo);
            goodsSpcesVosList.add(goodsSpcesVo);
        }
        return goodsSpcesVosList;
    }

    @Override
    public Integer updateGoodsSpecs(GoodsSpecsRequestParams goodsSpecsRequestParams) {
        GoodsSpecsPo goodsSpecsPo = new GoodsSpecsPo();
        BeanUtils.copyProperties(goodsSpecsRequestParams,goodsSpecsPo);
        Integer integer = goodsSpecsMapper.updateGoodsSpecsById(goodsSpecsPo);
        if (integer==NUMBER_ZERO ||integer==null){
            throw new ServiceException(ResultCode.ERROR);
        }
        return integer;
    }

    @Override
    public Integer removeGoodsSpecs(List<GoodsSpecsRequestParams> goodsSpecsRequestParamsList) {
        if (goodsSpecsRequestParamsList==null){
            throw new ServiceException(ResultCode.ERROR);
        }
        ArrayList<GoodsSpecsPo> goodsSpecsPoList = new ArrayList<>();
        for (GoodsSpecsRequestParams goodsSpecsRequestParams:goodsSpecsRequestParamsList) {
            GoodsSpecsPo goodsSpecsPo = new GoodsSpecsPo();
            BeanUtils.copyProperties(goodsSpecsRequestParams,goodsSpecsPo);
            goodsSpecsPoList.add(goodsSpecsPo);
        }
        Integer integer = goodsSpecsMapper.deleteGoodsSpecsById(goodsSpecsPoList);
        if (integer==NUMBER_ZERO){
            throw new ServiceException(ResultCode.ERROR);
        }
        goodsSpecsMapper.deleteTemplateGoodsSpecsRelation(goodsSpecsPoList);

        Integer integer2 = specsMapper.deleteSpecsByGoodsSpecsIds(goodsSpecsPoList);
        if (integer2==NUMBER_ZERO){
            throw new ServiceException(ResultCode.ERROR);
        }
        return integer;
    }

}
