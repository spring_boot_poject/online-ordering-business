package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.common.qo.ShopRequestParams;
import com.qf.web.common.vo.ShopManageVo;
import com.qf.web.entity.Shop;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShopManageMapper extends BaseMapper<ShopManageVo> {

    /**
     * 根据店铺id来查找店铺管理的全部信息
     * @param shopId
     * @return
     */
    ShopManageVo selectAllShopManageInfo(Integer shopId);

    /**
     * 根据店铺id来修改管理店铺的信息
     * @param shop
     * @return
     */
    Integer updateShopManage(Shop shop);
}
