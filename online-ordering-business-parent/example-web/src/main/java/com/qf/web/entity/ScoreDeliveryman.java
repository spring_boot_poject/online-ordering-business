package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "score_deliveryman")
public class ScoreDeliveryman {
    /**
     * 评价配送员id
     */
    @TableId(value = "score_deliveryman_id", type = IdType.INPUT)
    private Integer scoreDeliverymanId;

    /**
     * 配送员id
     */
    @TableField(value = "deliveryman_id")
    private Integer deliverymanId;

    /**
     * 订单id
     */
    @TableField(value = "order_id")
    private Integer orderId;

    /**
     * 评价内容（评价配送员）
     */
    @TableField(value = "score_deliveryman_message")
    private String scoreDeliverymanMessage;

    /**
     * 评价时间（评价配送员）
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "score_deliveryman_time")
    private Date scoreDeliverymanTime;

    /**
     * 配送评分
     */
    @TableField(value = "deliveryman_score")
    private Double deliverymanScore;

    @TableField(value = "is_delete")
    private String isDelete;

    public static final String COL_SCORE_DELIVERYMAN_ID = "score_deliveryman_id";

    public static final String COL_DELIVERYMAN_ID = "deliveryman_id";

    public static final String COL_ORDER_ID = "order_id";

    public static final String COL_SCORE_DELIVERYMAN_MESSAGE = "score_deliveryman_message";

    public static final String COL_SCORE_DELIVERYMAN_TIME = "score_deliveryman_time";

    public static final String COL_DELIVERYMAN_SCORE = "deliveryman_score";

    public static final String COL_IS_DELETE = "is_delete";
}