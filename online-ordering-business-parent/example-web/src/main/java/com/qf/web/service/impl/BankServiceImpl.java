package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.vo.BankVo;
import com.qf.web.common.qo.BankRequestParams;
import com.qf.web.entity.Bank;
import com.qf.web.mapper.BankMapper;
import com.qf.web.service.BankService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 51993
 */
@Service
public class BankServiceImpl implements BankService {

    @Autowired
    BankMapper bankMapper;

    @Override
    public Integer addBank(BankRequestParams requestParams) {
        Bank bank = new Bank();
        BeanUtils.copyProperties(requestParams,bank);
        Integer res = bankMapper.addBank(bank);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return res;
    }

    @Override
    public Integer unbind(BankRequestParams requestParams) {
        Bank bank = new Bank();
        BeanUtils.copyProperties(requestParams,bank);
        Integer res = bankMapper.updateBank(bank);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return res;
    }

    @Override
    public List<BankVo> selectBanks(Integer id) {
        List<Bank> banks = bankMapper.selectBankList(id);
        List<BankVo> bankVos = banks.stream().map(bank -> {
            BankVo bankVo = new BankVo();
            BeanUtils.copyProperties(bank, bankVo);
            return bankVo;
        }).collect(Collectors.toList());
        return bankVos;
    }
}
