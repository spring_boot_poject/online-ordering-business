package com.qf.web.common.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.ToString;

/**
 * @ClassName FullOrderListVo
 * @Description 订单信息返回值
 * @Author zhenyang
 * @Date 2022/3/31 16:32
 **/
@Data
@ToString
public class FullOrderListVo {
    /**
     * 活动订单数量
     */
    Integer fullOrdersNum;

    /**
     * 活动订单净收入
     */
    Double fullPrice;

    /**
     * 活动补贴支出
     */
    Double fullExpend;

    /**
     *总客单量
     */
    Double fullArpa;

    /**
     * 订单总数量
     */
    Integer OrderNum;

    /**
     * 订单总收入
     */
    Double OrderNumMoney;
}
