package com.qf.web.service;

import com.qf.web.common.qo.AddGoodsQo;
import com.qf.web.common.qo.GoodsQo;
import com.qf.web.common.vo.GetGoodsInfoVo;
import com.qf.web.common.vo.ShopGoodsVo;

import java.util.List;

public interface GoodsService {
    /**
     * 添加商品信息（默认将商品图标作为主图）
     * @return
     */
    Integer addGoodsInfo(AddGoodsQo addGoodsQo);

    /**
     * 添加商品信息（默认将商品图标作为副图）
     * @param addGoodsQo
     * @return
     */
    Integer addGoodsInfoAuxiliary(AddGoodsQo addGoodsQo);

    /**
     * 通过商品id 查询指定可售商品的信息
     */
    List<GetGoodsInfoVo> selectGoodsInfoByGoodsId(int GoodsId);


    /**
     * 通过店铺id 查询所有的已下架商品信息
     * @param shopId
     * @return
     */
    List<GetGoodsInfoVo> selectGoodsSoldOutInfoByGoodsInfo(int shopId);

    /**
     * 根据店铺id获取指定店铺所有商品类型的商品信息
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getAllGoodsInfoByShopId(int shopId);


    /**
     * 根据店铺id获取指定店铺所有商品类型为单品的商品信息
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getAllSingleGoodsInfoByShopId(int shopId);


    /**
     * 根据店铺id获取指定店铺所有商品类型为套餐的商品信息
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getAllSetMealGoodsInfoByShopId(int shopId);


    /**
     * 通过商品id 修改商品信息
     * @param goodsQo
     * @return
     */
    Integer updateGoodsInfo(GoodsQo goodsQo);

    /**
     * 根据对应的商品id 删除对应的商品信息（伪删除）
     * @param goodsId
     * @return
     */
    Integer deleteGoodsInfoByGoodsInfo(Integer goodsId);


    /**
     * 根据对应的商品id 恢复对应的商品信息已被伪删除的商品信息
     * @param goodsId
     * @return
     */
    Integer regainGoodsInfoByGoodsInfo(Integer goodsId);

    /**
     * 将数据存储到 Redis 中
     * @param goodsQo
     * @return
     */
    String addGoodsRedis(GoodsQo goodsQo);

    /**
     * 给即将过期的商品重新添加时间 (Redis)
     */
    String updateGoodsTimeRedis(AddGoodsQo addGoodsQo);

    /**
     * 返回 Redis中指定的 key
     * @param redisKey
     * @return
     */
    Object getGoodsInfoByKey(String redisKey);


    /**
     * 根据指定的商铺id 展示该店铺的所有商品。并根据商品价格降序（从低到高）
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getShopGoodsByPriceAsc(int shopId);


    /**
     * 根据指定的商铺id 展示该店铺的所有商品。并根据商品价格升序（从高到低）
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getShopGoodsByPriceDesc(int shopId);


    /**
     * 返回限时商品剩余存活时间
     * @param redisKey
     * @return
     */
    Long getRedisKeyLifeTimes(String redisKey);

    /**
     * 根据店铺id获取指定店铺所有商品信息,根据商品打包费降序
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getShopGoodsByPackMoneyAsc(int shopId);

    /**
     * 根据店铺id获取指定店铺所有商品信息,根据商品打包费降序
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getShopGoodsByPackMoneyDesc(int shopId);

    /**
     * 根据商品图片id 将主图变为副图
     * @param goodsPictureId
     * @return
     */
    int updateMasterPicture (int goodsPictureId);

    /**
     * 根据商品图片id 将主图变为副图
     * @param goodsPictureId
     * @return
     */
    int updateAuxiliaryPicture (int goodsPictureId);

    /**
     * 根据商品的名字模糊查询对应的商品信息
     * @param goodsName
     * @return
     */
    List<ShopGoodsVo> getShopGoodsByLikeName(String goodsName);


    /**
     * 根据指定的商铺id 一键将下架的商品上架
     * @param shopId
     * @return
     */
    int updateAllGoodsPull(int shopId);


    /**
     * 根据指定的商铺id 一键将上架的商品下架
     * @param shopId
     * @return
     */
    int updateAllGoodsSoldOut(int shopId);


    /**
     * 根据指定的商品id，单独已下架的商品进行上架
     * @param goodsId
     * @return
     */
    int updateSingleGoodsPull(int goodsId);


    /**
     * 根据指定的商品id，单独将已上架的商品进行下架
     * @param goodsId
     * @return
     */
    int updateSingleGoodsSoldOut(int goodsId);


}
