package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.math.BigDecimal;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "coupon")
public class Coupon {
    public static final String COL_USABLE_DATE = "usable_date";
    public static final String COL_RELEASE_DATE = "release_date";
    /**
     * 代金券Id
     */
    @TableId(value = "coupon_id", type = IdType.INPUT)
    private Integer couponId;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 优惠金额
     */
    @TableField(value = "coupon_price")
    private BigDecimal couponPrice;

    /**
     * 达标金额
     */
    @TableField(value = "tandards_price")
    private BigDecimal tandardsPrice;

    /**
     * 发放数量
     */
    @TableField(value = "released_quantity")
    private Integer releasedQuantity;

    /**
     * 已发放数量
     */
    @TableField(value = "coupon_receive_count")
    private Integer couponReceiveCount;

    /**
     * 0店铺新用户，1店铺所有用户
     */
    @TableField(value = "user_type")
    private Integer userType;

    /**
     * 发放开始日期
     */
    @TableField(value = "release_start_date")
    private Date releaseStartDate;

    /**
     * 发放结束日期
     */
    @TableField(value = "release_end_date")
    private Date releaseEndDate;

    /**
     * 可用开始时间
     */
    @TableField(value = "usable_start_date")
    private Date usableStartDate;

    /**
     * 到期时间
     */
    @TableField(value = "usable_end_date")
    private Date usableEndDate;

    /**
     * 0移动端，1桌面端
     */
    @TableField(value = "use_way")
    private Integer useWay;

    /**
     * 0与满减卷共享，1不共享
     */
    @TableField(value = "share_full_minus")
    private Integer shareFullMinus;

    /**
     * 优惠卷状态0，未开始，1进行中，2已结束
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 0删除，1未删除
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_COUPON_ID = "coupon_id";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_COUPON_PRICE = "coupon_price";

    public static final String COL_TANDARDS_PRICE = "tandards_price";

    public static final String COL_RELEASED_QUANTITY = "released_quantity";

    public static final String COL_COUPON_RECEIVE_COUNT = "coupon_receive_count";

    public static final String COL_USER_TYPE = "user_type";

    public static final String COL_RELEASE_START_DATE = "release_start_date";

    public static final String COL_RELEASE_END_DATE = "release_end_date";

    public static final String COL_USABLE_START_DATE = "usable_start_date";

    public static final String COL_USABLE_END_DATE = "usable_end_date";

    public static final String COL_USE_WAY = "use_way";

    public static final String COL_SHARE_FULL_MINUS = "share_full_minus";

    public static final String COL_STATUS = "status";

    public static final String COL_IS_DELETE = "is_delete";
}