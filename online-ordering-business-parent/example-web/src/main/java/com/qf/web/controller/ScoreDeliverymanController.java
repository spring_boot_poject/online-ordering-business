package com.qf.web.controller;
import com.qf.common.base.result.ResponseResult;
import com.qf.web.service.ScoreDeliverymanService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;


@RestController
@RequestMapping("/score/Deliveryman")
public class ScoreDeliverymanController {

    @Resource
    ScoreDeliverymanService scoreDeliverymanService;
    //11根据评价id删除指定的评价
    @GetMapping("/delete/Score/deliveryman")
    public  ResponseResult<Integer> deleteScoreDeliverymanByScoreId (int ScoreDeliverymanId){
        Integer integer = scoreDeliverymanService.deleteScoreDeliverymanByScoreId(ScoreDeliverymanId);
        return ResponseResult.success(integer);
    }
}
