package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.common.qo.FullMinusRequestParams;
import com.qf.web.common.vo.FullAnalysisVo;
import com.qf.web.common.vo.FullMinusUseVo;
import com.qf.web.common.vo.FullMinusVo;
import com.qf.web.entity.FullMinus;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface FullMinusMapper extends BaseMapper<FullMinus> {

    /**
     * 插入优惠信息政策
     * @param fullMinusRequestParams
     * @return
     */
    int  insertFull(FullMinusRequestParams fullMinusRequestParams);

    /**
     * 查询所有的优惠活动
     * @param shopId
     * @return
     */
    List<FullMinusVo> selectAllFull(Integer shopId);

    List<FullMinusUseVo> selectOrderFull(Integer shopId);

    /**
     * 查询今日,昨日活动订单信息
     * @param shopId
     * @return
     */
    FullAnalysisVo selectDayFull(@Param(value="shopId") Integer shopId, @Param(value="day")Integer day);

    /**
     * 查询7天 30 天内 活动订单信息
     * @param shopId
     * @return
     */
    FullAnalysisVo selectSomeDayFull(@Param(value="shopId") Integer shopId, @Param(value="day")Integer day);

    /**
     * 停止优惠活动
     * @param fullMinusId
     * @return
     */
    Integer deleteFull(Integer fullMinusId);
}