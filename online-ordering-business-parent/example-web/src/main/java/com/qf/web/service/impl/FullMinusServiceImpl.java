package com.qf.web.service.impl;

import ch.qos.logback.core.joran.util.beans.BeanUtil;
import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.FullMinusRequestParams;
import com.qf.web.common.vo.*;
import com.qf.web.mapper.FullMinusMapper;
import com.qf.web.mapper.OrdersMapper;
import com.qf.web.service.FullMinusService;
import io.micrometer.core.instrument.binder.BaseUnits;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @ClassName FullMinusServiceImpl
 * @Description FullMinus的业务处理层
 * @Author zhenyang
 * @Date 2022/3/30 20:10
 **/
@Service
public class FullMinusServiceImpl implements FullMinusService {
    public static final Integer NOW=0;
    public static final Integer YESTEADAY=-1;
    public static final Integer SEVEN_DAY=-6;
    public static final Integer THIRTY_DAY=-29;


    @Resource
    FullMinusMapper fullMinusMapper;

    @Resource
    OrdersMapper ordersMapper;
    @Override
    public ResponseResult<? extends Object> insertFull(FullMinusRequestParams fullMinusRequestParams) {
        int i=0;
        try {
             i = fullMinusMapper.insertFull(fullMinusRequestParams);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        if (i == 0) {
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 查看优惠使用情况
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult selectOrderFull(Integer shopId) {
        List<FullMinusUseVo> fullMinusUseVos=null;

        try {
             fullMinusUseVos = fullMinusMapper.selectOrderFull(shopId);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        if (fullMinusUseVos==null){
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,fullMinusUseVos);
    }

    /**
     * 查询当天的活动订单详细
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult selectFullOrderNow(Integer shopId) {
        //设置一个天数的属性
        Integer day=NOW;
        FullOrderListVo fullOrderListVo = new FullOrderListVo();

        try {
            FullAnalysisVo fullAnalysisVo = fullMinusMapper.selectDayFull(shopId, day);
            FullOrderNumVo fullOrderNumVo = ordersMapper.selectOrderNumDay(shopId, day);
            BeanUtils.copyProperties(fullAnalysisVo,fullOrderListVo);
            BeanUtils.copyProperties(fullOrderNumVo,fullOrderListVo);
        } catch (Exception e) {
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,fullOrderListVo);
    }


    /**
     * 查询昨天的的活动订单详细
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult selectFullOrderYETday(Integer shopId) {
        //设置一个天数的属性
        Integer day=YESTEADAY;
        FullOrderListVo fullOrderListVo = new FullOrderListVo();

        try {
            FullAnalysisVo fullAnalysisVo = fullMinusMapper.selectDayFull(shopId, day);
            FullOrderNumVo fullOrderNumVo = ordersMapper.selectOrderNumDay(shopId, day);
            BeanUtils.copyProperties(fullAnalysisVo,fullOrderListVo);
            BeanUtils.copyProperties(fullOrderNumVo,fullOrderListVo);
        } catch (Exception e) {
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }

        return ResponseResult.success(ResultCode.SUCCESS,fullOrderListVo);
    }


    /**
     * 查询七天内的的活动订单详细
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult selectFullOrderSevenDay(Integer shopId) {
        //设置一个天数的属性
        Integer day=YESTEADAY;
        FullOrderListVo fullOrderListVo = new FullOrderListVo();

        try {
            FullAnalysisVo fullAnalysisVo = fullMinusMapper.selectSomeDayFull(shopId, day);
            FullOrderNumVo fullOrderNumVo = ordersMapper.selectSomeOrderNumDay(shopId, day);
            BeanUtils.copyProperties(fullAnalysisVo,fullOrderListVo);
            BeanUtils.copyProperties(fullOrderNumVo,fullOrderListVo);
        } catch (Exception e) {
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }

        return ResponseResult.success(ResultCode.SUCCESS,fullOrderListVo);
    }


    /**
     * 查询30天内的的活动订单详细
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult selectFullOrderThirtyDay(Integer shopId) {
        //设置一个天数的属性
        Integer day=THIRTY_DAY;
        FullOrderListVo fullOrderListVo = new FullOrderListVo();

        try {
            FullAnalysisVo fullAnalysisVo = fullMinusMapper.selectSomeDayFull(shopId, day);
            FullOrderNumVo fullOrderNumVo = ordersMapper.selectSomeOrderNumDay(shopId, day);
            BeanUtils.copyProperties(fullAnalysisVo,fullOrderListVo);
            BeanUtils.copyProperties(fullOrderNumVo,fullOrderListVo);
        } catch (Exception e) {
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }

        return ResponseResult.success(ResultCode.SUCCESS,fullOrderListVo);
    }

    /**
     * 查询所有的优惠活动
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult selectAllFull(Integer shopId) {
        List<FullMinusVo> fullMinusVos=null;
        try {
            fullMinusVos = fullMinusMapper.selectAllFull(shopId);
        } catch (Exception e) {
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS,fullMinusVos);
    }

    /**
     * 删除优惠活动
     * @param fullMinusId
     * @return
     */
    @Override
    public ResponseResult deleteFull(Integer fullMinusId) {
        Integer integer=null;
        try {
         integer = fullMinusMapper.deleteFull(fullMinusId);
        } catch (Exception e) {
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        if (integer==null){
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return  ResponseResult.success(ResultCode.SUCCESS);

    }


}
