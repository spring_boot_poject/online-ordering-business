package com.qf.web.config;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author 51993
 */
@Configuration
public class AliyunMsgConfig {

    @Autowired
    MsgProperites msgProperites;

    @Bean
    public IAcsClient iAcsClient() throws ClientException {
        DefaultProfile profile = DefaultProfile.getProfile(msgProperites.getRegionId(), msgProperites.getAccessKeyId(), msgProperites.getAccessKeySecret());
        IAcsClient client = new DefaultAcsClient(profile);
        return client;
    }


}
