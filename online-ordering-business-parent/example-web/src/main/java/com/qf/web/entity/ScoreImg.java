package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "score_img")
public class ScoreImg {
    /**
     * 评价图片id
     */
    @TableId(value = "score_img_id", type = IdType.INPUT)
    private Integer scoreImgId;

    @TableField(value = "img")
    private String img;

    /**
     * 评价id
     */
    @TableField(value = "score_id")
    private Integer scoreId;

    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_SCORE_IMG_ID = "score_img_id";

    public static final String COL_IMG = "img";

    public static final String COL_SCORE_ID = "score_id";

    public static final String COL_IS_DELETE = "is_delete";
}