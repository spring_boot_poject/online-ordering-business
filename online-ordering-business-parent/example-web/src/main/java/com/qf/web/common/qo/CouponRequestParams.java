package com.qf.web.common.qo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class CouponRequestParams {

    @TableId(value = "coupon_id", type = IdType.INPUT)
    private Integer couponId;

    @NotNull
    @TableField(value = "shop_id")
    private Integer shopId;

    @NotNull
    @TableField(value = "coupon_price")
    private BigDecimal couponPrice;

    @NotNull
    @TableField(value = "tandards_price")
    private BigDecimal tandardsPrice;

    @NotNull
    @TableField(value = "released_quantity")
    private Integer releasedQuantity;

    @NotNull
    @TableField(value = "user_type")
    private Integer userType;

    /**
     * 优惠卷状态0，未开始，1进行中，2已结束
     */
    @TableField(value = "`status`")
    private Integer status;

    /**
     * 发放开始日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "release_start_date")
    private Date releaseStartDate;

    /**
     * 发放结束日期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "release_end_date")
    private Date releaseEndDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "usable_start_date")
    private Date usableStartDate;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "usable_end_date")
    private Date usableEndDate;


    @NotNull
    @TableField(value = "use_way")
    private Integer useWay;

    @NotNull
    @TableField(value = "share_full_minus")
    private Integer shareFullMinus;
}
