package com.qf.web.common.qo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.ToString;
import org.hibernate.validator.constraints.Length;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * @ClassName FullMinusRequestParams
 * @Description 满减返回类
 * @Author zhenyang
 * @Date 2022/3/30 19:18
 **/
@Data
@ToString
@Validated
public class FullMinusRequestParams {

    @TableId(value = "full_minus_id", type = IdType.INPUT)
    private Integer fullMinusId;

    /**
     * 满减门槛
     */
    @TableField(value = "full_minus_threshold")
    @Min(0)
    private Double fullMinusThreshold;

    /**
     * 满减金额
     */
    @TableField(value = "full_minus_money")
    @Min(0)
    private Double fullMinusMoney;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    @NotNull
    private Integer shopId;

    /**
     * 满减标题
     */
    @TableField(value = "full_minus_title")

    @NotBlank
    @Length(min = 1, max = 20, message = "活动名称必须1 到20 位")
    private String fullMinusTitle;


    /**
     * 创建时间
     */
    @TableField(value = "full_minus_create")
    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date fullMinusCreate;
}
