package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "goods")
public class Goods {
    /**
     * 商品id
     */
    @TableId(value = "goods_id", type = IdType.INPUT)
    private Integer goodsId;

    /**
     * 商品名
     */
    @TableField(value = "goods_name")
    private String goodsName;

    /**
     * 商品简介
     */
    @TableField(value = "goods_introduce")
    private String goodsIntroduce;

    /**
     * 商品类别id
     */
    @TableField(value = "goods_type_id")
    private Integer goodsTypeId;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 商品价格
     */
    @TableField(value = "goods_price")
    private Integer goodsPrice;

    /**
     * 规格模板id
     */
    @TableField(value = "template_id")
    private Integer templateId;

    /**
     * 包装费
     */
    @TableField(value = "goods_packing_fee")
    private Integer goodsPackingFee;

    /**
     * 1代表商品上架，0代表商品下架
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_GOODS_ID = "goods_id";

    public static final String COL_GOODS_NAME = "goods_name";

    public static final String COL_GOODS_INTRODUCE = "goods_introduce";

    public static final String COL_GOODS_TYPE_ID = "goods_type_id";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_GOODS_PRICE = "goods_price";

    public static final String COL_TEMPLATE_ID = "template_id";

    public static final String COL_GOODS_PACKING_FEE = "goods_packing_fee";

    public static final String COL_IS_DELETE = "is_delete";
}