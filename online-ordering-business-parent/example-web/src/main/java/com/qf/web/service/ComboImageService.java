package com.qf.web.service;

import com.qf.web.common.qo.ComboImageRequestParams;
import com.qf.web.common.vo.ComboImageVo;

import java.util.List;

public interface ComboImageService {

    /**
     * 添加套餐图片
     * @param requestParams
     * @return
     */
    ComboImageVo addComboImage(ComboImageRequestParams requestParams);

    /**
     * 修改图片信息
     * @param requestParams
     * @return
     */
    Integer updateComboImage(ComboImageRequestParams requestParams);

    /**
     * 删除套餐图片
     * @param id    套餐ID
     * @return
     */
    Integer removeComboImage(Integer id);

    /**
     * 根据套餐ID获取图片
     * @param id
     * @return
     */
    List<ComboImageVo> getImagesByCombo(Integer id);

}
