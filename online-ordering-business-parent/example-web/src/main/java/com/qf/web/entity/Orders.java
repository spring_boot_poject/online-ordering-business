package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "orders")
public class Orders {
    /**
     * 订单id
     */
    @TableId(value = "order_id", type = IdType.INPUT)
    private Integer orderId;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 订单生产时间
     */
    @TableField(value = "order_time")
    private Date orderTime;

    /**
     * 订单配送状态1=自提;0=配送
     */
    @TableField(value = "order_delivery")
    private Integer orderDelivery;

    /**
     * 订单状态0=未接单；1=已接单；2=配送中；3=订单已完成
     */
    @TableField(value = "order_state")
    private Integer orderState;

    /**
     * 0=正常订单;1=顾客发起退款;2=商家拒绝退款;3=商家同意退款;4=顾客取消退款
     */
    @TableField(value = "order_refund")
    private Integer orderRefund;

    /**
     * 订单价格
     */
    @TableField(value = "order_price")
    private Double orderPrice;

    /**
     * 收货地址
     */
    @TableField(value = "user_address")
    private String userAddress;

    /**
     * 收货人姓名
     */
    @TableField(value = "username")
    private String username;

    /**
     * 电话号码
     */
    @TableField(value = "user_phone")
    private String userPhone;

    /**
     * 订单备注
     */
    @TableField(value = "order_message")
    private String orderMessage;

    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_ORDER_ID = "order_id";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_ORDER_TIME = "order_time";

    public static final String COL_ORDER_DELIVERY = "order_delivery";

    public static final String COL_ORDER_STATE = "order_state";

    public static final String COL_ORDER_REFUND = "order_refund";

    public static final String COL_ORDER_PRICE = "order_price";

    public static final String COL_USER_ADDRESS = "user_address";

    public static final String COL_USERNAME = "username";

    public static final String COL_USER_PHONE = "user_phone";

    public static final String COL_ORDER_MESSAGE = "order_message";

    public static final String COL_IS_DELETE = "is_delete";
}