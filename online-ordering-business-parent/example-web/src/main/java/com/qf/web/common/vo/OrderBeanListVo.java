package com.qf.web.common.vo;

import lombok.Data;

import java.util.List;

/**
 * 订单详细集合
 */
@Data
public class OrderBeanListVo {

    //多个订单信息
    List<OrderBeanVo> orderBeanVos;

}
