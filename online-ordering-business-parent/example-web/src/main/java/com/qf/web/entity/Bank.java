package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "bank")
public class Bank {
    /**
     * 银行卡id
     */
    @TableId(value = "bank_id", type = IdType.INPUT)
    private Integer bankId;

    /**
     * 银行卡号
     */
    @TableField(value = "bank_num")
    private String bankNum;

    /**
     * 店铺id
     */
    @TableField(value = "shopkeeper_id")
    private Integer shopkeeperId;

    /**
     * 伪删除
     */
    @TableField(value = "is_delete")
    private Integer isDelete;
}