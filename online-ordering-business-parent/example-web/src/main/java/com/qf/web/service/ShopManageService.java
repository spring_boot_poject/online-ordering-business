package com.qf.web.service;

import com.qf.web.common.qo.ShopManageRequestParams;
import com.qf.web.common.qo.ShopRequestParams;
import com.qf.web.common.vo.ShopManageVo;

public interface ShopManageService {
    ShopManageVo selectAllShopManageInfo(ShopManageRequestParams shopManageRequestParams);

    Integer updateShopManageInfo(ShopRequestParams shopRequestParams);
}
