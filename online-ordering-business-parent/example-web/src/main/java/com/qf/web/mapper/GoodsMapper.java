package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.common.vo.GetGoodsInfoVo;
import com.qf.web.common.vo.ShopGoodsVo;
import com.qf.web.entity.Goods;

import java.util.List;


public interface GoodsMapper extends BaseMapper<Goods> {
    /**
     * 添加商品信息啊（主图和副图通用一个 添加 goods信息）
     * @return
     */
    int insertGoods(Goods goods);

    /**
     * 根据商品姓名返回商品id
     * @param GoodsName
     * @return
     */
    int selectGoodsId(String GoodsName);


    /**
     * 根据商品id 获取商品表信息(默认为可售)
     * @param goodsId
     * @return
     */
    List<GetGoodsInfoVo> selectGoodsInfoByGoodsId(int goodsId);


    /**
     * 根据店铺 id获取指定店铺所有商品信息 (下架状态，包含所有商品类型)
     * @param shopId
     * @return
     */
    List<GetGoodsInfoVo> selectGoodsSoldOutInfoByGoodsInfo(int shopId);

    /**
     * 根据店铺 id获取指定店铺所有商品信息（可售状态，包含所有商品类型）
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getAllGoodsInfoByShopId(int shopId);


    /**
     * 根据店铺id获取指定店铺所有商品类型为单品的商品信息
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getAllSingleGoodsInfoByShopId(int shopId);


    /**
     * 根据店铺id获取指定店铺所有商品类型为套餐的商品信息
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getAllSetMealGoodsInfoByShopId(int shopId);


    /**
     * 修改商品信息
     * @param goods
     * @return
     */
    int updateGoodsInfo(Goods goods);

    /**
     * 根据商品id删除对应的商品信息(伪删除)
     * @param goodsId
     * @return
     */
    int deleteGoodsInfoByGoodsInfo(int goodsId);

    /**
     * 根据商品id 恢复对应的商品信息
     * @param goodsId
     * @return
     */
    int regainGoodsInfoByGoodsInfo(int goodsId);


    /**
     * 根据店铺id获取指定店铺所有商品信息,根据商品价格降序
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getShopGoodsByPriceAsc(int shopId);


    /**
     * 根据店铺id获取指定店铺所有商品信息,根据商品价格升序
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getShopGoodsByPriceDesc(int shopId);

    /**
     * 根据店铺id获取指定店铺所有商品信息,根据商品打包费降序
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getShopGoodsByPackMoneyAsc(int shopId);


    /**
     * 根据店铺id获取指定店铺所有商品信息,根据商品打包费升序
     * @param shopId
     * @return
     */
    List<ShopGoodsVo> getShopGoodsByPackMoneyDesc(int shopId);

    /**
     * 根据商品的名字模糊查询对应的商品信息
     * @param goodsName
     * @return
     */
    List<ShopGoodsVo> getShopGoodsByLikeName(String goodsName);


    /**
     * 根据指定的商铺id 一键将下架的商品上架
     * @param shopId
     * @return
     */
    int updateAllGoodsPull(int shopId);



    /**
     * 根据指定的商铺id 一键将上架的商品下架
     * @param shopId
     * @return
     */
    int updateAllGoodsSoldOut(int shopId);

    /**
     * 根据指定的商品id，单独已下架的商品进行上架
     * @param goodsId
     * @return
     */
    int updateSingleGoodsPull(int goodsId);


    /**
     * 根据指定的商品id，单独将已上架的商品进行下架
     * @param goodsId
     * @return
     */
    int updateSingleGoodsSoldOut(int goodsId);




}