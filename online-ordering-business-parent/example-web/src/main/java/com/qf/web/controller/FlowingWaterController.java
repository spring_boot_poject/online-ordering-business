package com.qf.web.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.web.common.qo.FlowingWaterRequestParams;
import com.qf.web.common.vo.FlowingWaterVo;
import com.qf.web.service.FlowingWaterService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/flowing")
public class FlowingWaterController {
    @Resource
    private FlowingWaterService flowingWaterService;

    @GetMapping("/select/flowing/water")
    public ResponseResult<List> selectFlowingWater(@RequestBody FlowingWaterRequestParams flowingWaterRequestParams){
        List<FlowingWaterVo> flowingWaterVos = flowingWaterService.selectFlowingWater(flowingWaterRequestParams);
        return ResponseResult.success(flowingWaterVos);
    }

    @GetMapping("/select/date/flowing/water")
    public ResponseResult<List> selectDateFlowingWater(@RequestBody FlowingWaterRequestParams flowingWaterRequestParams){
        List<FlowingWaterVo> flowingWaterVos = flowingWaterService.selectDateFlowingWater(flowingWaterRequestParams);
        return ResponseResult.success(flowingWaterVos);
    }

    @GetMapping("/select/up/flowing/water")
    public ResponseResult<List> selectUpFlowingWater(@RequestBody FlowingWaterRequestParams flowingWaterRequestParams){
        List<FlowingWaterVo> flowingWaterVos = flowingWaterService.selectUpFlowingWater(flowingWaterRequestParams);
        return ResponseResult.success(flowingWaterVos);
    }

    @GetMapping("/select/date/up/flowing/water")
    public ResponseResult<List> selectDateUpFlowingWater(@RequestBody FlowingWaterRequestParams flowingWaterRequestParams){
        List<FlowingWaterVo> flowingWaterVos = flowingWaterService.selectDateUpFlowingWater(flowingWaterRequestParams);
        return ResponseResult.success(flowingWaterVos);
    }
}
