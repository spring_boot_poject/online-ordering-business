package com.qf.web.controller;


import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.LegalPersonRequestParams;
import com.qf.web.common.utils.ConstantUtils;
import com.qf.web.common.vo.LegalPersonVo;
import com.qf.web.service.LegalPersonService;
import com.qf.web.service.UploadService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * @author banxiaowen
 */
@RestController
@RequestMapping("/legal")
public class LegalPersonController {
    @Resource
    private LegalPersonService legalPersonService;
    @Resource
    private UploadService uploadService;

    @PostMapping("/legal/register")
    public ResponseResult<LegalPersonVo> registerLegalPerson(@RequestBody LegalPersonRequestParams legalPersonRequestParams){
        LegalPersonVo legalPersonVo = legalPersonService.registerLegalPerson(legalPersonRequestParams);
        return ResponseResult.success(legalPersonVo);
    }

    @PostMapping("/legal/upload/start")
    public ResponseResult<ResultCode> registerLegalImgStart(@RequestBody MultipartFile multipartFile,
                                                       LegalPersonRequestParams legalPersonRequestParams){
        String url = uploadService.uploadImage(multipartFile, ConstantUtils.LEGAL_IMAGE_BUCKET_NAME_START);
        legalPersonRequestParams.setReverseImageUrl(url);
        Integer imgStart = legalPersonService.registerLegalImgStart(legalPersonRequestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    @PostMapping("/legal/upload/end")
    public ResponseResult<ResultCode> registerLegalImgEnd(@RequestBody MultipartFile multipartFile,
                                                       LegalPersonRequestParams legalPersonRequestParams){
        String url = uploadService.uploadImage(multipartFile, ConstantUtils.LEGAL_IMAGE_BUCKET_NAME_END);
        legalPersonRequestParams.setObverseImageUrl(url);
        Integer imgEnd = legalPersonService.registerLegalImgEnd(legalPersonRequestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
