package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.ShopManageRequestParams;
import com.qf.web.common.qo.ShopRequestParams;
import com.qf.web.common.vo.ShopManageVo;
import com.qf.web.entity.Shop;
import com.qf.web.mapper.ShopManageMapper;
import com.qf.web.service.ShopManageService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

@Service
public class ShopManageServiceImpl implements ShopManageService {

    @Resource
    private ShopManageMapper shopManageMapper;
    @Override
    public ShopManageVo selectAllShopManageInfo(ShopManageRequestParams shopManageRequestParams) {
        ShopManageVo shopManageVo = shopManageMapper.selectAllShopManageInfo(shopManageRequestParams.getShopId());
        if (ObjectUtils.isEmpty(shopManageVo)){
            throw new ServiceException(ResultCode.ERROR);
        }
        return shopManageVo;
    }

    @Override
    public Integer updateShopManageInfo(ShopRequestParams shopRequestParams) {
        //qo转po
        Shop shop = new Shop();
        BeanUtils.copyProperties(shopRequestParams,shop);
        Integer rSet = shopManageMapper.updateShopManage(shop);
        if (rSet == 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        return rSet;
    }
}
