package com.qf.web.entity;

import lombok.Data;

@Data
public class GoodsPo {
    /**
     * 商品id
     */
    private Integer goodsId;

    /**
     * 商品名
     */
    private String goodsName;

    /**
     * 商品简介
     */
    private String goodsIntroduce;

    /**
     * 商品类别id
     */
    private Integer goodsTypeId;

    /**
     * 商品价格
     */
    private Integer goodsPrice;

    /**
     * 规格模板id
     */
    private Integer templateId;

    /**
     * 包装费
     */
    private Integer goodsPackingFee;





    public static final String COL_GOODS_ID = "goods_id";

    public static final String COL_GOODS_NAME = "goods_name";

    public static final String COL_GOODS_INTRODUCE = "goods_introduce";

    public static final String COL_GOODS_TYPE_ID = "goods_type_id";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_GOODS_PRICE = "goods_price";

    public static final String COL_TEMPLATE_ID = "template_id";

    public static final String COL_GOODS_PACKING_FEE = "goods_packing_fee";

}
