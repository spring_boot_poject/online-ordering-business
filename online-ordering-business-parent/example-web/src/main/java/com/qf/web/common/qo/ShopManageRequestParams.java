package com.qf.web.common.qo;

import com.qf.web.common.vo.ShopManageVo;
import lombok.Data;

@Data
public class ShopManageRequestParams extends ShopManageVo {
}
