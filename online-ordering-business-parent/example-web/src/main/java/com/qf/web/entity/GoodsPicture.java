package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName(value = "goods_picture")
public class GoodsPicture {
    /**
     * 商品图片id
     */
    @TableId(value = "goods_picture_id", type = IdType.INPUT)
    private Integer goodsPictureId;

    /**
     * 图片url
     */
    @TableField(value = "goods_picture_img")
    private String goodsPictureImg;

    /**
     * 主图
     */
    @TableField(value = "goods_picture_main")
    private String goodsPictureMain;

    /**
     * 商品id
     */
    @TableField(value = "goods_id")
    private Integer goodsId;

    /**
     * 0失效，1存在
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_GOODS_PICTURE_ID = "goods_picture_id";

    public static final String COL_GOODS_PICTURE_IMG = "goods_picture_img";

    public static final String COL_GOODS_PICTURE_MAIN = "goods_picture_main";

    public static final String COL_GOODS_ID = "goods_id";

    public static final String COL_IS_DELETE = "is_delete";
}