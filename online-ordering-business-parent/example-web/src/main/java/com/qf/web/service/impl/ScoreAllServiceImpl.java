package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.vo.ScoreAllVo;
import com.qf.web.common.vo.ScoreCountVo;
import com.qf.web.common.vo.ScoreInfoVo;
import com.qf.web.mapper.ScoreAllMapper;
import com.qf.web.service.ScoreAllService;
import org.apache.ibatis.annotations.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class ScoreAllServiceImpl implements ScoreAllService {

    //1根据商店id查看所有评价信息
    @Resource
    ScoreAllMapper scoreAllMapper;
    @Override
    public List<ScoreAllVo> getScoreAllByShopId(int shopId){
        List<ScoreAllVo> scoreAllPoList = scoreAllMapper.checkScoreAllByShopId(shopId);
        if (!ObjectUtils.isEmpty(scoreAllPoList)) {
            return scoreAllPoList;
        }else {
            throw new ServiceException(ResultCode.ERROR);
        }

    }

    //2根据评论id查看评论详情
    @Override
    public ScoreInfoVo getScoreInfoByScoreId(int scoreId) {
        ScoreInfoVo scoreInfoVo = scoreAllMapper.selectScoreInfoByScoreId(scoreId);
        if (!ObjectUtils.isEmpty(scoreInfoVo)){
            return scoreInfoVo;
        }else {
            throw new ServiceException(ResultCode.ERROR);
        }
    }

    //3根据店铺id查看未回复的所有评价
    @Override
    public List<ScoreAllVo> getScoreAllByShopIdNo(int shopId) {
        List<ScoreAllVo> scoreAllVoList = scoreAllMapper.selectScoreAllByShopIdNo(shopId);
            if (!ObjectUtils.isEmpty(scoreAllVoList)){
                return scoreAllVoList;
            }else {
                throw new ServiceException(ResultCode.ERROR);
            }


    }

    //4根据评价id查询未回复的评论详情
    @Override
    public ScoreInfoVo getNoScoreInfoByScoreId(int scoreId) {
        ScoreInfoVo scoreInfoVo = scoreAllMapper.selectNoScoreInfoByScoreId(scoreId);
        if (!ObjectUtils.isEmpty(scoreInfoVo)){
            return scoreInfoVo;
        }else {
            throw new ServiceException(ResultCode.ERROR);
        }
    }


    //5根据店铺id获取最近30天的平均分（店铺评分）
    @Override
    public ScoreAllVo getScoreAvg(int shopId) {
        ScoreAllVo scoreAllVo = scoreAllMapper.selectScoreAvg(shopId);
        if (!ObjectUtils.isEmpty(scoreAllVo)){
            return scoreAllVo;
        }else {
            throw new ServiceException(ResultCode.ERROR);
        }
    }


    //6根据店铺id查看指定时间段的所有评论
    @Override
    public List<ScoreAllVo> getScoreAllTimeByShopId(int shopId,  String endDate, String beginDate) {
        List<ScoreAllVo> scoreAllVoList = scoreAllMapper.selectScoreAllTimeByShopId(shopId, endDate, beginDate);
            if (!ObjectUtils.isEmpty(scoreAllVoList)){
                return scoreAllVoList;
            }else {
                throw new ServiceException(ResultCode.ERROR);
            }


    }

    //7根据店铺id查看指定的星级评价（所有评价）
    @Override
    public List<ScoreAllVo> getScoreByShopId(@Param("shopId") int shopId, @Param("scoreStarId") int scoreStarId) {
        List<ScoreAllVo> scoreAllVoList = scoreAllMapper.selectScoreByShopId(shopId, scoreStarId);
        if (!ObjectUtils.isEmpty(scoreAllVoList)){
            return scoreAllVoList;
        }else {
            throw new ServiceException(ResultCode.ERROR);
        }
    }

    //8根据店铺id查看指定的星级评价（未回复的评价）
    @Override
    public List<ScoreAllVo> getScoreNoByShopId(@Param("shopId") int shopId, @Param("scoreStarId") int scoreStarId) {
        List<ScoreAllVo> scoreAllVoList = scoreAllMapper.selectScoreNoByShopId(shopId, scoreStarId);
        if (!ObjectUtils.isEmpty(scoreAllVoList)){
            return scoreAllVoList;
        }else {
            throw new ServiceException(ResultCode.ERROR);
        }
    }

    //9根据店铺id查看中差评的总数
    @Override
    public Integer getBadScoreCountByShopId(int shopId) {
        Integer integer = scoreAllMapper.selectBadScoreCountByShopId(shopId);
        if (!ObjectUtils.isEmpty(integer)){
            return integer;
        }else {
            throw new ServiceException(ResultCode.ERROR);
        }
    }

    //10根据店铺id获取中差评和好评的总数
    @Override
    public ScoreCountVo getScoreCountByShopId(int shopId) {
        ScoreCountVo scoreCountVo = new ScoreCountVo();
        Integer badCount = scoreAllMapper.selectBadScoreCountByShopId(shopId);
        scoreCountVo.setBadScoreCount(badCount);
        Integer goodCount= scoreAllMapper.selectGoodScoreCountByShopId(shopId);
        scoreCountVo.setGoodScoreCount(goodCount);
        return scoreCountVo;
    }
}

