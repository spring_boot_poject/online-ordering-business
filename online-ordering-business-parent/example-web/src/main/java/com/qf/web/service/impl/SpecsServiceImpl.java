package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.SpecsRequestParams;
import com.qf.web.common.vo.SpecsVo;
import com.qf.web.entity.SpecsPo;
import com.qf.web.mapper.SpecsMapper;
import com.qf.web.service.SpecsService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class SpecsServiceImpl implements SpecsService {
    private static final Integer NUMBER_ZERO=0;
    @Resource
    SpecsMapper specsMapper;


    @Override
    public Integer addSpercs(SpecsRequestParams specsRequestParams) {
        SpecsPo specsPo = new SpecsPo();
        BeanUtils.copyProperties(specsRequestParams,specsPo);

        Integer integer = specsMapper.inserspecs(specsPo);
        if (integer==NUMBER_ZERO || integer==null){
            throw new ServiceException(ResultCode.ERROR);
        }
        return integer;
    }

    @Override
    public Integer removeSpecs(Integer specsId) {
        Integer integer = specsMapper.deleteSpecsBySpecsId(specsId);
        if (integer==NUMBER_ZERO || integer==null){
            throw new ServiceException(ResultCode.ERROR);
        }
        return integer;
    }

    @Override
    public Integer updateSpecs(SpecsRequestParams specsRequestParams) {
        SpecsPo specsPo = new SpecsPo();
        BeanUtils.copyProperties(specsRequestParams,specsPo);
        Integer integer = specsMapper.updateSpecsById(specsPo);
        if (integer==NUMBER_ZERO || integer==null){
            throw new ServiceException(ResultCode.ERROR);
        }
        return integer;
    }

    @Override
    public List<SpecsVo> selectSpecsList(Integer goodsSpecsId) {
        List<SpecsPo> specsPosLit = specsMapper.selectSpecsListByGoodsSpecsId(goodsSpecsId);
        List<SpecsVo> specsVosList = new ArrayList<>();
        if (specsPosLit.size()==NUMBER_ZERO){
            throw new ServiceException(ResultCode.ERROR);
        }
        for (SpecsPo specsPo1:specsPosLit) {
            SpecsVo specsVo = new SpecsVo();
            BeanUtils.copyProperties(specsPo1,specsVo);
            specsVosList.add(specsVo);
        }
        return specsVosList;
    }


}
