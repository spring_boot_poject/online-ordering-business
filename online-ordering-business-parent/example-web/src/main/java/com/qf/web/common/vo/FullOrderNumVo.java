package com.qf.web.common.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @ClassName FullOrderNumVo
 * @Description 订单总数统计
 * @Author zhenyang
 * @Date 2022/3/31 16:11
 **/
@Data
@ToString
public class FullOrderNumVo {

    /**
     * 订单总数量
     */
    Integer OrderNum;

    /**
     * 订单总收入
     */
    Double OrderNumMoney;
}
