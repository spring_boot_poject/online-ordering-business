package com.qf.web.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class ScoreVo {
    /**
     * 评价id
     */
    @TableId(value = "score_id", type = IdType.INPUT)
    private Integer scoreId;

    /**
     * 回复内容
     */
    @TableField(value = "reply_message")
    private String replyMessage;

    /**
     * 回复状态：1=已回复；0=未回复
     */
    @TableField(value = "reply_message_state")
    private Integer replyMessageState;

    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_SCORE_ID = "score_id";

    public static final String COL_REPLY_MESSAGE = "reply_message";

    public static final String COL_REPLY_MESSAGE_STATE = "reply_message_state";

    public static final String COL_IS_DELETE = "is_delete";
}
