package com.qf.web.controller;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.ComboRuleRequestParams;
import com.qf.web.common.vo.ComboRuleVo;
import com.qf.web.service.ComboRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @author 谢积威
 */
@RestController
@RequestMapping("/rule")
public class ComboRuleController {

    @Autowired
    ComboRuleService comboRuleService;

    /**
     * 店铺添加使用规则
     * @param requestParams
     * @return
     */
    @PutMapping("/add")
    public ResponseResult<ResultCode> addRule(ComboRuleRequestParams requestParams){
        comboRuleService.addComboRule(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 查询店中的使用规则
     * @param requestParams
     * @return
     */
    @GetMapping("/query")
    public ResponseResult<List<ComboRuleVo>> getRuleByShop(@RequestBody ComboRuleRequestParams requestParams){
        List<ComboRuleVo> comboRuleVoList = comboRuleService.queryRuleByShop(requestParams.getShopId());
        return ResponseResult.success(comboRuleVoList);
    }

    /**
     * 更新使用规则内容
     * @param requestParams
     * @return
     */
    @PostMapping("/update")
    public ResponseResult<ResultCode> updateRule(@RequestBody ComboRuleRequestParams requestParams){
        comboRuleService.updateRule(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 删除使用规则
     * @param requestParams
     * @return
     */
    @DeleteMapping("/del")
    public ResponseResult<ResultCode> deleteRule(@RequestBody ComboRuleRequestParams requestParams){
        comboRuleService.delRule(requestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

}
