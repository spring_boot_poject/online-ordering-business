package com.qf.web.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author 51993
 */
@Data
@Component
@ConfigurationProperties("aliyun.msg")
public class MsgProperites {
    private String regionId;
    private String accessKeyId;
    private String accessKeySecret;
    private String signName;
    private String templateCode;
}
