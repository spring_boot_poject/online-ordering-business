package com.qf.web.controller;



import com.qf.common.base.result.ResponseResult;
import com.qf.web.common.qo.*;
import com.qf.web.common.vo.GoodsSpcesVo;
import com.qf.web.common.vo.SpecsTemplateVo;
import com.qf.web.common.vo.SpecsVo;
import com.qf.web.service.GoodsSpecsService;
import com.qf.web.service.SpecsService;
import com.qf.web.service.SpecsTemplateService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/specs")
public class  SpecsController {
    @Resource
    SpecsTemplateService specsTemplateService;

    @Resource
    GoodsSpecsService goodsSpecsService;

    @Resource
    SpecsService specsService;

    /**
     * 获取通用的规格模板
     * @return
     */
    @GetMapping("show/common/template")
    public ResponseResult<List<SpecsTemplateVo>> showCommonSpecsTemplate(){
        List<SpecsTemplateVo> list = specsTemplateService.showCommonSpecsTemplateList();
        return ResponseResult.success(list);
    };

    /**
     * 获取店铺的规格模板
     * @return
     */
    @GetMapping("show/shop/template")
    public ResponseResult<List<SpecsTemplateVo>> showShopSpecsTemplate(Integer shopId){
        List<SpecsTemplateVo> list = specsTemplateService.showShopSpecsTemplateList(shopId);
        return ResponseResult.success(list);
    };

    /**
     * 根据规格模板id查询对应的规格模板及其规格和详细信息
     * @param templateId
     * @return
     */
    @GetMapping("/select/template")
    public ResponseResult<SpecsTemplateVo> selectTemplate(Integer templateId){
        SpecsTemplateVo specsTemplateVo = specsTemplateService.selectTSpecsTemplate(templateId);
        return ResponseResult.success(specsTemplateVo);
    }

    /**
     * 修改规格模板信息
     * @param specsTemplateRequestParams
     * @return
     */
    @PostMapping("/updateTemplate")
    public ResponseResult<Integer> updateSpecsTemplate(SpecsTemplateRequestParams specsTemplateRequestParams){
        Integer integer = specsTemplateService.updateTemplate(specsTemplateRequestParams);
        return ResponseResult.success(integer);
    }

    /**
     * 添加模板和规格的中间表关系
     * @param TemplateGoodsSpecsRequestParamsList
     * @return
     */
    @PutMapping("/add/template/relation")
    public ResponseResult<Integer> addSpecsTemplateRelation(@RequestBody List<TemplateGoodsSpecsRequestParams> TemplateGoodsSpecsRequestParamsList){
        Integer integer = specsTemplateService.addGoodsSpecsTemplateRelation(TemplateGoodsSpecsRequestParamsList);
        return ResponseResult.success(integer);
    };

    /**
     * 添加商品规格模板（在通用模板基础上）
     * @param specsTemplateRequestParams
     * @return 返回模板id
     */
    @PutMapping("/insert/template")
    public ResponseResult<Integer> addTemplate(@RequestBody SpecsTemplateRequestParams specsTemplateRequestParams){
        Integer integer = specsTemplateService.insertTemplate(specsTemplateRequestParams);
        return ResponseResult.success(integer);
    };

/*    *//**
     * 添加商品规格模板（自定义模板）
     * @param specsTemplateRequestParams
     * @return 返回模板id
     *//*
    @PutMapping("/insert/template/custom")
    public ResponseResult addTemplateCustom(@RequestBody SpecsTemplateQo specsTemplateQo){
        Integer integer = specsTemplateService.insertTemplateCustom(specsTemplateQo);
        return ResponseResult.success(integer);
    };*/

    /**
     * 添加自定义模板并返回模板id
     * @param specsTemplateRequestParams
     * @return
     */
    @PutMapping("/add/user/template")
    public ResponseResult<Integer> addUserSpecsTemplate(@RequestBody SpecsTemplateRequestParams specsTemplateRequestParams){
        Integer specsTemplateId = specsTemplateService.addUserTemplate(specsTemplateRequestParams);
        return ResponseResult.success(specsTemplateId);
    }

    /**
     * 查询规格：通用的、商家特有的、模板特有的(冗余)
     */

    /**
     * 查询所有通用的商品规格及其详细信息
     * @return
     */
    @GetMapping("select/Common/specs/list")
    public ResponseResult<List<GoodsSpcesVo>> selectCommonGoodsSpecsList(){
        List<GoodsSpcesVo> goodsSpcesVos = goodsSpecsService.selectCommonGoodsSpeces();
        return ResponseResult.success(goodsSpcesVos);
    }

    /**
     *查询店铺特有的商品规格及其详细信息
     * @return
     */
    @GetMapping("/select/shop/goods/specs.list")
    public ResponseResult<List<GoodsSpcesVo>> selectShopGoodsSpecsList(GoodsSpecsRequestParams goodsSpecsRequestParams){
        List<GoodsSpcesVo> goodsSpcesVos = goodsSpecsService.selectShopGoodsSpeces(goodsSpecsRequestParams);
        return ResponseResult.success(goodsSpcesVos);
    }


    /**
     * 添加商品规格及详细信息
     * @param goodsSpecsRequestParams
     * @return
     */
    @PutMapping("/add/goods/specs")
    public ResponseResult<Integer> addGoodsSpecs(@RequestBody GoodsSpecsRequestParams goodsSpecsRequestParams){
        Integer integer = goodsSpecsService.addGoodsSpecs(goodsSpecsRequestParams);
        return ResponseResult.success(integer);
    }

    /**
     * 删除、批量删除规格信息
     * @param
     * @return
     */
    @DeleteMapping("delete/goods/specs")
    public ResponseResult<Integer> removeGoodsSpecs(@RequestBody List<GoodsSpecsRequestParams> goodsSpecsRequestParamsList){
        Integer integer = goodsSpecsService.removeGoodsSpecs(goodsSpecsRequestParamsList);
        return ResponseResult.success(integer);
    }

    /**
     *修改规格基本信息
     * @param goodsSpecsRequestParams
     * @return
     */
    @PostMapping("/update/goods/specs")
    public ResponseResult<Integer> updateGoodsSpecs(@RequestBody GoodsSpecsRequestParams goodsSpecsRequestParams){
        Integer integer = goodsSpecsService.updateGoodsSpecs(goodsSpecsRequestParams);
        return ResponseResult.success(integer);
    }

    /**
     * 添加规格详细信息
     * @param specsRequestParams
     * @return
     */
    @PutMapping("/add/specs")
    public ResponseResult<Integer> addSpecs(@RequestBody SpecsRequestParams specsRequestParams){
        Integer integer = specsService.addSpercs(specsRequestParams);
        return ResponseResult.success(integer);
    }

    /**
     * 伪删除规格详细信息
     * @param specsId
     * @return
     */
    @DeleteMapping("/delete/specs")
    public ResponseResult<Integer> removeSpecs(Integer specsId){
        Integer integer = specsService.removeSpecs(specsId);

        return ResponseResult.success(integer);
    }

    /**
     * 修改规格详细信息
     * @param specsRequestParams
     * @return
     */
    @PostMapping("/update/specs")
    public ResponseResult<Integer> updateSpecs(@RequestBody SpecsRequestParams specsRequestParams){
        Integer integer = specsService.updateSpecs(specsRequestParams);
        return ResponseResult.success(integer);
    }

    /**
     * 删除规格模板
     * @param templateIdsList
     * @return
     */
    @DeleteMapping("/delete/template")
    public ResponseResult<Integer> deleteTemplate(@RequestParam("templateIdsList") List<Integer> templateIdsList){
        Integer integer = specsTemplateService.deleteTemplate(templateIdsList);
        return ResponseResult.success(integer);
    }

    /**
     * 根据规格id获取规格对应所有详细信息
     * @param goodsSpecsId
     * @return
     */
    @GetMapping("/select/specs/list")
    public ResponseResult<List<SpecsVo>> selectSpecsList(Integer goodsSpecsId){
        List<SpecsVo> specsVosList = specsService.selectSpecsList(goodsSpecsId);
        return ResponseResult.success(specsVosList);
    }

}
