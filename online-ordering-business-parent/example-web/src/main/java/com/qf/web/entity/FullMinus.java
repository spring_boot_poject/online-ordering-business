package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import lombok.Data;

@Data
@TableName(value = "full_minus")
public class FullMinus {
    @TableId(value = "full_minus_id", type = IdType.INPUT)
    private Integer fullMinusId;

    /**
     * 满减门槛
     */
    @TableField(value = "full_minus_threshold")
    private Double fullMinusThreshold;

    /**
     * 满减金额
     */
    @TableField(value = "full_minus_money")
    private Double fullMinusMoney;

    /**
     * 创建时间
     */
    @TableField(value = "full_minus_create")
    private Date fullMinusCreate;

    /**
     * 店铺id
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 满减标题
     */
    @TableField(value = "full_minus_title")
    private String fullMinusTitle;

    /**
     * 满减状态。1代表开始满减，0代表关闭满减
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    public static final String COL_FULL_MINUS_ID = "full_minus_id";

    public static final String COL_FULL_MINUS_THRESHOLD = "full_minus_threshold";

    public static final String COL_FULL_MINUS_MONEY = "full_minus_money";

    public static final String COL_FULL_MINUS_CREATE = "full_minus_create";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_FULL_MINUS_TITLE = "full_minus_title";

    public static final String COL_IS_DELETE = "is_delete";
}