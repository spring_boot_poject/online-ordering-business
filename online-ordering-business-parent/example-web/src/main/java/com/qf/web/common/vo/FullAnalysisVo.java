package com.qf.web.common.vo;

import lombok.Data;
import lombok.ToString;

/**
 * @ClassName FullAnalysis
 * @Description  满减活动查询
 * @Author zhenyang
 * @Date 2022/3/31 14:37
 **/
@Data
@ToString
public class FullAnalysisVo {

    /**
     * 活动订单数量
     */
    Integer fullOrdersNum;

    /**
     * 活动订单净收入
     */
    Double fullPrice;

    /**
     * 活动补贴支出
     */
    Double fullExpend;

    /**
     *总客单量
     */
    Double fullArpa;


}
