package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.OrdersRequestParams;
import com.qf.web.common.vo.*;
import com.qf.web.entity.EnvironmentPicture;
import com.qf.web.entity.Orders;
import com.qf.web.common.vo.OrderBeanVo;
import com.qf.web.common.vo.OrderBeanListVo;
import com.qf.web.mapper.OrdersMapper;
import com.qf.web.service.OrderService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    OrdersMapper ordersMapper;

    /**
     * 查询所有的订单
     * @param orderId
     * @return
     */
    @Override
    public ResponseResult selectOrderBean(Integer orderId) {

        OrderBeanVo orderBeanVo = new OrderBeanVo();

        try {
            orderBeanVo = ordersMapper.selectOrderBean(orderId);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS, orderBeanVo);
    }

    /**
     * 查询所有的订单
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectOrderBeanList(Integer shopId) {

        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();
        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectOrderBeanList(shopId);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
       return ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }


    /**
     * 获取
     * @param state 状态值 0 代表未接单 1 代表 已接单 2 代表配送中 3 代表订单完成 4 代表 取消订单
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectOrderByState(Integer shopId, Integer state) {

        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();
        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectOrderBystate(shopId,state);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }

    /**
     * 定义订单的退款状态
     * @param shopId
     * @param refund 退款状态 退款状态 0 正常订单  1 顾客退款  2 商家拒绝退款  3 商家同意退款
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectOrderByRefund(Integer shopId, Integer refund) {

        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();

        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectOrderByRefund(shopId, refund);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }


    /**
     * 查询历史订单的完成
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectOrderSuccess(Integer shopId) {
        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();
        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectOrderSuccess(shopId);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }

        return ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }


    /**
     * 商家订单未处理或者忽略
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectOrderIgnore(Integer shopId) {

        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();
        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectOrderIgnore(shopId);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }

        return  ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }


    /**
     * 历史的配送数据
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectHistoryOrder(Integer shopId) {
        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();
        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectHistoryOrder(shopId);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }



    /**
     * 查询所有的订单
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectOrderBeanListTake(Integer shopId) {

        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();
        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectOrderBeanListTake(shopId);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }


    /**
     * 获取
     * @param state 状态值 0 代表未接单 1 代表 已接单 2 代表配送中 3 代表订单完成 4 代表 取消订单
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectOrderByStateTake(Integer shopId, Integer state) {

        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();
        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectOrderBystateTake(shopId,state);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }

    /**
     * 定义订单的退款状态
     * @param shopId
     * @param refund 退款状态 退款状态 0 正常订单  1 顾客退款  2 商家拒绝退款  3 商家同意退款
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectOrderByRefundTake(Integer shopId, Integer refund) {

        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();

        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectOrderByRefundTake(shopId, refund);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }


    /**
     * 查询历史订单的完成
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectOrderSuccessTake(Integer shopId) {
        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();
        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectOrderSuccessTake(shopId);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }

        return ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }


    /**
     * 商家订单未处理或者忽略
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectOrderIgnoreTake(Integer shopId) {

        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();
        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectOrderIgnoreTake(shopId);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }

        return  ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }


    /**
     * 历史的配送数据
     * @param shopId
     * @return
     */
    @Override
    public ResponseResult<OrderBeanListVo> selectHistoryOrderTake(Integer shopId) {
        OrderBeanListVo orderBeanListVo = new OrderBeanListVo();
        try {
            List<OrderBeanVo> orderBeanVos = ordersMapper.selectHistoryOrderTake(shopId);
            orderBeanListVo.setOrderBeanVos(orderBeanVos);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS, orderBeanListVo);
    }

    /**
     * 商家取消订单
     * @param orderId
     * @return
     */
    @Override
    public ResponseResult updateOrderCancel(Integer orderId) {

        int i = 0;
        try {
            i = ordersMapper.updateOrderCancel(orderId);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        if (i==0){
         return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }


    /**
     * 商家接受订单
     * @param orderId
     * @return
     */
    @Override
    public ResponseResult updateOrderAccept(Integer orderId) {
        int i = 0;
        try {
             i = ordersMapper.updateOrderAccept(orderId);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        if (i==0){
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    /**
     * 商家同意退款
     * @param orderId
     * @return
     */
    @Override
    public ResponseResult updateOrderAgree(Integer orderId) {
        int i = 0;
        try {
            i = ordersMapper.updateOrderAgree(orderId);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        if (i == 0) {
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }


    @Override
    public ResponseResult updateOrderDisagree(Integer orderId) {
        int i = 0;
        try {
            i = ordersMapper.updateOrderDisagree(orderId);
        } catch (Exception e) {
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        if (i == 0) {
            return ResponseResult.error(ResultCode.SYSTEM_UNKNOW_ERROR);
        }
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    @Override
    public List<ShopOrdersVo> selectShopOrders(OrdersRequestParams ordersRequestParams) {
        List<ShopOrdersVo> ordersList = ordersMapper.selectShopOrders(ordersRequestParams.getShopId());
        if (ordersList.size()==0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //要存储的返回对象vo集合
        ArrayList<ShopOrdersVo> ordersVos = new ArrayList<>();
        //遍历实体类（po）对象然后依次加进vo集合里
        for (ShopOrdersVo orders:ordersList) {
            ShopOrdersVo ordersVo = new ShopOrdersVo();
            //po转vo
            BeanUtils.copyProperties(orders,ordersVo);
            boolean flag = ordersVos.add(ordersVo);
            if (!flag){
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        return ordersVos;
    }

    @Override
    public List<ShopOrdersVo> selectDayOrders(OrdersRequestParams ordersRequestParams) {
        List<ShopOrdersVo> ordersList = ordersMapper.selectDayDetailOrders(ordersRequestParams.getTime(),
                ordersRequestParams.getShopId());
        if (ordersList.size()==0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //要存储的返回对象vo集合
        ArrayList<ShopOrdersVo> ordersVos = new ArrayList<>();
        //遍历实体类（po）对象然后依次加进vo集合里
        for (ShopOrdersVo orders:ordersList) {
            ShopOrdersVo ordersVo = new ShopOrdersVo();
            //po转vo
            BeanUtils.copyProperties(orders,ordersVo);
            boolean flag = ordersVos.add(ordersVo);
            if (!flag){
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        return ordersVos;
    }

    @Override
    public ShopOrdersVo selectIncomeAdvance(OrdersRequestParams ordersRequestParams) {
        ShopOrdersVo shopOrdersVo = ordersMapper.selectIncomeAdvance(ordersRequestParams.getShopId());
        if (ObjectUtils.isEmpty(shopOrdersVo)){
            throw new ServiceException(ResultCode.ERROR);
        }
        return shopOrdersVo;
    }

    @Override
    public List<ShopOrdersVo> selectRangeOrders(OrdersRequestParams ordersRequestParams) {
        List<ShopOrdersVo> ordersList = ordersMapper.selectRangeOrders(ordersRequestParams.getStartTimeDate(),
                ordersRequestParams.getEndTimeDate(),
                ordersRequestParams.getShopId());
        if (ordersList.size()==0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //要存储的返回对象vo集合
        ArrayList<ShopOrdersVo> ordersVos = new ArrayList<>();
        //遍历实体类（po）对象然后依次加进vo集合里
        for (ShopOrdersVo orders:ordersList) {
            ShopOrdersVo ordersVo = new ShopOrdersVo();
            //po转vo
            BeanUtils.copyProperties(orders,ordersVo);
            boolean flag = ordersVos.add(ordersVo);
            if (!flag){
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        return ordersVos;
    }

    @Override
    public List<ShopOrdersVo> selectMonthOrders(OrdersRequestParams ordersRequestParams) {
        List<ShopOrdersVo> ordersList = ordersMapper.selectMonthOrders(ordersRequestParams.getShopId());
        if (ordersList.size()==0){
            throw new ServiceException(ResultCode.ERROR);
        }
        //要存储的返回对象vo集合
        ArrayList<ShopOrdersVo> ordersVos = new ArrayList<>();
        //遍历实体类（po）对象然后依次加进vo集合里
        for (ShopOrdersVo orders:ordersList) {
            ShopOrdersVo ordersVo = new ShopOrdersVo();
            //po转vo
            BeanUtils.copyProperties(orders,ordersVo);
            boolean flag = ordersVos.add(ordersVo);
            if (!flag){
                throw new ServiceException(ResultCode.ERROR);
            }
        }
        return ordersVos;
    }
}

