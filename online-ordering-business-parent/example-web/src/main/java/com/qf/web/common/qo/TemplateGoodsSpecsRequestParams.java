package com.qf.web.common.qo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateGoodsSpecsRequestParams {

    private Integer templateId;

    private Integer goodsSpecsId;
}
