package com.qf.web.common.vo;

import com.qf.web.entity.FlowingWater;
import lombok.Data;

@Data
public class FlowingWaterVo extends FlowingWater {
}
