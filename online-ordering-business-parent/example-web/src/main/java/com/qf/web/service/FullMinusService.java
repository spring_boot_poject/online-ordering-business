package com.qf.web.service;

import com.qf.common.base.result.ResponseResult;
import com.qf.web.common.qo.FullMinusRequestParams;

public interface FullMinusService {

    /**
     * 创建优惠活动
     * @param fullMinusRequestParams
     * @return
     */
    public ResponseResult insertFull(FullMinusRequestParams fullMinusRequestParams);


    /**
     * 查询优惠使用情况
     * @param shopId
     * @return
     */
    public ResponseResult selectOrderFull(Integer shopId);


    /**
     * 查询所有的记录订单
     * @param shopId
     * @return
     */
    public ResponseResult selectFullOrderNow(Integer shopId);

    /**
     * 查询所有的记录订单
     * @param shopId
     * @return
     */
    public ResponseResult selectFullOrderYETday(Integer shopId);


    /**
     * 查询7天的记录订单
     * @param shopId
     * @return
     */
    public ResponseResult   selectFullOrderSevenDay(Integer shopId);
    /**
     * 查询30天的记录订单
     * @param shopId
     * @return
     */
    public ResponseResult   selectFullOrderThirtyDay(Integer shopId);

    /**
     * 查询所有的优惠信息
     * @param shopId
     * @return
     */
    public ResponseResult selectAllFull(Integer shopId);

    /**
     * 删除优惠活动
     * @param fullMinusId
     * @return
     */
    public ResponseResult deleteFull(Integer fullMinusId);

}
