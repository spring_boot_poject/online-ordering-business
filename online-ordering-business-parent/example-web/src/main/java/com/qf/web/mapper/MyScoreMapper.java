package com.qf.web.mapper;

import com.qf.web.common.vo.MyScoreVo;

import java.util.List;

public interface MyScoreMapper {
    //1根据店铺id获取我的所有评价（评价配送员）
    List<MyScoreVo> selectMyScoreByShopId(int shopId);
}
