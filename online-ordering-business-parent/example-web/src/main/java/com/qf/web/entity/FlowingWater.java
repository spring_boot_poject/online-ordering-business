package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "flowing_water")
public class FlowingWater {
    public static final String COL_BANK_ID = "bank_id";
    /**
     * 交易流水
     */
    @TableId(value = "flowing_water_id", type = IdType.INPUT)
    private Integer flowingWaterId;

    /**
     * 金额
     */
    @TableField(value = "flowing_water_money")
    private Integer flowingWaterMoney;

    /**
     * 时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "flowing_water_time")
    private Date flowingWaterTime;

    /**
     * 流水类型。0代表提现，1代表转账，2代表充值
     */
    @TableField(value = "flowing_water_type")
    private Integer flowingWaterType;

    /**
     * 来源
     */
    @TableField(value = "flowing_water_address")
    private String flowingWaterAddress;

    /**
     * 伪删除
     */
    @TableField(value = "is_delete")
    private Integer isDelete;

    @TableField(value = "shop_id")
    private Integer shopId;

    @TableField(value = "shopkeeper_id")
    private Integer shopkeeperId;

    public static final String COL_FLOWING_WATER_ID = "flowing_water_id";

    public static final String COL_FLOWING_WATER_MONEY = "flowing_water_money";

    public static final String COL_FLOWING_WATER_TIME = "flowing_water_time";

    public static final String COL_FLOWING_WATER_TYPE = "flowing_water_type";

    public static final String COL_FLOWING_WATER_ADDRESS = "flowing_water_address";

    public static final String COL_IS_DELETE = "is_delete";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_SHOPKEEPER_ID = "shopkeeper_id";
}