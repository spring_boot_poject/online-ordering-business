package com.qf.web.controller;
import com.qf.common.base.exception.ControllerException;
import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.vo.ScoreCountVo;
import com.qf.web.service.ScoreAllService;
import com.qf.web.common.vo.ScoreAllVo;
import com.qf.web.common.vo.ScoreInfoVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 *
 *        李晓晴
 */
@RestController
@RequestMapping("/score/all")
public class ScoreAllController {
    @Resource
    ScoreAllService scoreAllService;

    //1根据商店id查看所有评价信息
    @GetMapping("/get/score/all")
    public ResponseResult <List<ScoreAllVo>> getScoreAllByShopId(int shopId){
        List<ScoreAllVo> scoreAllVoList = scoreAllService.getScoreAllByShopId(shopId);
        if (scoreAllVoList.size()==0){
            throw new ControllerException(ResultCode.ERROR);
        }
        return ResponseResult.success(scoreAllVoList);
    }

    //2根据评论id查看评论详情
    @GetMapping("/get/score/info")
    public ResponseResult <ScoreInfoVo> getScoreInfoByScoreId(int scoreId){
        ScoreInfoVo scoreInfoVo = scoreAllService.getScoreInfoByScoreId(scoreId);
        return ResponseResult.success(scoreInfoVo);
    }

    //3根据店铺id查看未回复的所有评价
    @GetMapping("/get/no/score/all")
    public ResponseResult <List<ScoreAllVo>> getScoreAllByShopIdNo(int shopId){
        List<ScoreAllVo> scoreAllVoList = scoreAllService.getScoreAllByShopIdNo(shopId);
        if (scoreAllVoList.size()==0){
            throw new ControllerException(ResultCode.ERROR);
        }
        return ResponseResult.success(scoreAllVoList);

    }

    //4根据评价id查询未回复的评论详情
    @GetMapping("/get/no/score/info")
    public ResponseResult <ScoreInfoVo> getNoScoreInfoByScoreId(int scoreId){
        ScoreInfoVo noScoreInfoByScoreId = scoreAllService.getNoScoreInfoByScoreId(scoreId);
        return ResponseResult.success(noScoreInfoByScoreId);
    }

    //5根据店铺id获取最近30天的平均分（店铺评分）
    @GetMapping("/get/score/avg")
    public ResponseResult<ScoreAllVo> getScoreAvg(int shopId){
        ScoreAllVo scoreAllVo = scoreAllService.getScoreAvg(shopId);
        return ResponseResult.success(scoreAllVo);

    }

//    //6根据店铺id查看指定时间段的所有评论（店铺评论）
//    @GetMapping("/get/score/time")
//    public ResponseResult <List<ScoreAllVo>> getScoreAllTimeByShopId(int shopId, @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate, @DateTimeFormat(pattern = "yyyy-MM-dd") Date beginDate){
//        List<ScoreAllVo> scoreAllVoList = scoreAllService.getScoreAllTimeByShopId(shopId, endDate, beginDate);
//        return ResponseResult.success(scoreAllVoList);
//    }
    //6根据店铺id查看指定时间段的所有评论（店铺评论）
    @GetMapping("/get/score/time")
    public ResponseResult <List<ScoreAllVo>> getScoreAllTimeByShopId(int shopId, String endDate, String beginDate){
        List<ScoreAllVo> scoreAllVoList = scoreAllService.getScoreAllTimeByShopId(shopId, endDate, beginDate);
        return ResponseResult.success(scoreAllVoList);
    }


    //7根据店铺id查看指定的星级评价（所有评价）
    @GetMapping("/get/score/")
    public ResponseResult<List<ScoreAllVo>> getScoreByShopId(@Param("shopId") int shopId, @Param("scoreStarId") int scoreStarId){
        List<ScoreAllVo> scoreAllVoList = scoreAllService.getScoreByShopId(shopId, scoreStarId);
        return ResponseResult.success(scoreAllVoList);
    }

    //8根据店铺id查看指定的星级评价（未回复的评价）
    @GetMapping("/get/no/score")
    public ResponseResult <List<ScoreAllVo>> getScoreNoByShopId(@Param("shopId") int shopId, @Param("scoreStarId") int scoreStarId){
        List<ScoreAllVo> scoreAllVoList = scoreAllService.getScoreNoByShopId(shopId, scoreStarId);
        return ResponseResult.success(scoreAllVoList);
    }


    //9根据店铺id获取中差评和好评的总数
    @GetMapping("/get/score/count")
    public ResponseResult<ScoreCountVo> getScoreCountByShopId(int shopId){
        ScoreCountVo  scoreCountVo = scoreAllService.getScoreCountByShopId(shopId);
        return ResponseResult.success(scoreCountVo);
    }


}
