package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.TradingCertificateRequestParams;
import com.qf.web.common.vo.TradingCertificateVo;
import com.qf.web.entity.TradingCertificate;
import com.qf.web.mapper.TradingCertificateMapper;
import com.qf.web.service.TradingCertificateService;
import com.qf.web.service.UploadService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;

@Service
public class TradingCertificateServiceImpl implements TradingCertificateService {
    @Resource
    private TradingCertificateMapper tradingCertificateMapper;


    @Override
    public TradingCertificateVo registerTradingCertificate(TradingCertificateRequestParams tradingCertificateRequestParams) {
        TradingCertificate trading = tradingCertificateMapper.selectTrading(tradingCertificateRequestParams.getTradingCertificateNumber());
        if (ObjectUtils.isEmpty(trading)){
            trading = new TradingCertificate();
            //qo转po
            BeanUtils.copyProperties(tradingCertificateRequestParams,trading);
            int i = tradingCertificateMapper.insert(trading);
            if (i > 0){
                //po转vo
                TradingCertificateVo vo = new TradingCertificateVo();
                BeanUtils.copyProperties(trading,vo);
                return vo;
            }else {
                throw new ServiceException(ResultCode.ERROR);
            }
        }else {
            throw new ServiceException(ResultCode.ACCOUNT_IS_EXISTENT);
        }
    }

    @Override
    public Integer registerTradingImg(TradingCertificateRequestParams tradingCertificateRequestParams) {
        Integer rSet = tradingCertificateMapper.registerTradingImg(tradingCertificateRequestParams.getTradingImageUrl(),
                tradingCertificateRequestParams.getShopId());
        if (rSet == 0){
            throw new ServiceException(ResultCode.ERROR);
        }
        return rSet;
    }
}
