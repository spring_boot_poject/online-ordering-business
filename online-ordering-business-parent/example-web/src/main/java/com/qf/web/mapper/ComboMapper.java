package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.common.vo.ComboVo;
import com.qf.web.entity.Combo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author 谢积威
 */
public interface ComboMapper extends BaseMapper<Combo> {

    /**
     * 套餐添加
     * @param combo 套餐
     * @return
     */
    int insertCombo(Combo combo);

    /**
     * 添加商品和套餐的关系
     * @param gid
     * @param cid
     * @param number
     * @return
     */
    int insertComboGoods(@Param("gid") Integer gid, @Param("cid") Integer cid, @Param("number") Integer number);

    /**
     * 查询店铺的套餐信息
     * @param id    店铺ID
     * @return
     */
    List<ComboVo> selectComboByShop(Integer id);

    /**
     * 更新套餐信息
     * @param combo
     * @return
     */
    Integer updateComboDetails(Combo combo);

    /**
     * 根据套餐ID查询套餐详情
     * @param id
     * @return
     */
    ComboVo selectComboByComboId(Integer id);

    /**
     * 删除套餐和商品的关系的记录
     * @param comboId
     * @return
     */
    Integer deleteComboGoodsRelation(Integer comboId);

    /**
     * 将套餐状态改成被删除
     * @param comboId
     * @return
     */
    Integer updateComboToDel(Integer comboId);

    /**
     * 条件查询
     * @param combo
     * @return
     */
    List<ComboVo> selectComboByCondition(Combo combo);
}