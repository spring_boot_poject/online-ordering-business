package com.qf.web.common.vo;
import lombok.Data;

/**
    * 存储套餐图片
    */
@Data
public class ComboImageVo {
    /**
     * 主键
     */
    private Integer imageId;

    /**
     * 图片URL
     */
    private String imageUrl;

    /**
     * 是否主图 1 是 0不是
     */
    private Integer mainImage;

    /**
     * 套餐ID
     */
    private Integer comboId;

}