package com.qf.web.controller;

import com.qf.common.base.result.ResponseResult;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.ShopRequestParams;
import com.qf.web.common.utils.ConstantUtils;
import com.qf.web.common.vo.ShopVo;
import com.qf.web.service.ShopService;
import com.qf.web.entity.Shop;
import com.qf.web.service.UploadService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author banxiaowen
 */
@RestController
@RequestMapping("/shop")
public class ShopController {
    @Resource
    UploadService uploadService;

    @Resource
    ShopService shopService;

    @GetMapping("/shop/register")
    public ResponseResult<List<ShopVo>> registerShop(@RequestBody ShopRequestParams shopRequestParams){
        List<ShopVo> shopVos = shopService.selectShopInfo(shopRequestParams);
        return ResponseResult.success(shopVos);
    }

    @PostMapping("/shop/type/register")
    public ResponseResult<ShopVo> registerShopType(@RequestBody ShopRequestParams shopRequestParams){
        ShopVo type = shopService.selectShopType(shopRequestParams);
        return ResponseResult.success(type);
    }

    @GetMapping("/select/shop/status")
    public ResponseResult<ShopVo> selectShopStatus(@RequestBody ShopRequestParams shopRequestParams){
        ShopVo shopVo = shopService.selectShopStatus(shopRequestParams);
        return ResponseResult.success(shopVo);
    }

    @PostMapping("/update/shop/status")
    public ResponseResult<ResultCode> updateShopStatus(@RequestBody ShopRequestParams shopRequestParams){
        Integer i = shopService.updateShopStatus(shopRequestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    @PostMapping("/upload/shop/img")
    public ResponseResult<ResultCode> uploadShopImg(@RequestBody MultipartFile multipartFile,
                                                    ShopRequestParams shopRequestParams){
        String imgUrl = uploadService.uploadImage(multipartFile, ConstantUtils.SHOP_IMAGE_BUCKET_NAME);
        shopRequestParams.setShopImg(imgUrl);
        Integer rSet = shopService.updateShopImg(shopRequestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }

    @GetMapping("/select/shop/account")
    public ResponseResult<ShopVo> selectShopAccount(@RequestBody ShopRequestParams shopRequestParams){
        ShopVo shopVo = shopService.selectShopAccount(shopRequestParams);
        return ResponseResult.success(shopVo);
    }

    @PostMapping("/update/shop/account")
    public ResponseResult<ResultCode> updateShopAccount(@RequestBody ShopRequestParams shopRequestParams){
        Integer rSet = shopService.updateShopAccount(shopRequestParams);
        return ResponseResult.success(ResultCode.SUCCESS);
    }
}
