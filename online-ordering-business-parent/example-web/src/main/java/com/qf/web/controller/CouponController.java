package com.qf.web.controller;


import com.qf.common.base.result.ResponseResult;
import com.qf.web.common.qo.CouponRequestParams;
import com.qf.web.common.vo.CouponVo;
import com.qf.web.entity.ActiveOrder;
import com.qf.web.entity.CouponInfo;
import com.qf.web.service.CouponService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/coupon")
@Validated
public class CouponController {


    @Resource
    CouponService couponService;

    /**
     * 创建代金券
     * @param couponRequestParams
     * @return
     */
    @PutMapping("/add/coupon")
    public ResponseResult<Integer> addCoupon(@Validated @RequestBody CouponRequestParams couponRequestParams){
        Integer integer = couponService.addCoupon(couponRequestParams);
        return ResponseResult.success(integer);
    }

    /**
     * 获取当前店铺所有代金券
     * @return
     */
    @GetMapping("/select/coupon/list")
    public ResponseResult<List<CouponVo>> selectCouponList(Integer shopId){
        List<CouponVo> couponVos = couponService.selectCouponList(shopId);
        return ResponseResult.success(couponVos);
    }

    /**
     *  根据状态和创建时间（开始结束）查询当前店铺代金券
     * @param couponRequestParams
     * @return
     */
    @GetMapping("/select/specific/coupon/list")
    public ResponseResult<List<CouponVo>> selectSpecificCouponList(@RequestBody CouponRequestParams couponRequestParams){
        List<CouponVo> list = couponService.selectSpecificCouponList(couponRequestParams);
        return ResponseResult.success(list);
    }

    /**
     *  查询店铺下的所有代金券使用记录
     * @param shopId
     * @return
     */
    @GetMapping("/select/coupon/info")
    public ResponseResult<List<CouponInfo>> selectCouponInfo(Integer shopId){
        List<CouponInfo> couponInfos = couponService.selectCouponInfo(shopId);
        return ResponseResult.success(couponInfos);
    }
    /**
     *  数据分析
     * @param shopId
     * @return
     */
    @GetMapping("/select/active/order/info")
    public ResponseResult<ActiveOrder> selectActiveOrderInfo(Integer shopId,Integer dateType){
        ActiveOrder activeOrder = couponService.selectActiveOrderInfo(shopId, dateType);
        return ResponseResult.success(activeOrder);
    }

    /**
     * 修改发放状态
     * @param couponId
     * @param isDelete
     * @return
     */
    @PostMapping("/update/give/out/status")
    public ResponseResult<Integer> updateGiveOutStatus(Integer couponId,Integer isDelete){
        Integer integer = couponService.updateGiveOutStatus(couponId, isDelete);
        return ResponseResult.success(integer);
    }

}
