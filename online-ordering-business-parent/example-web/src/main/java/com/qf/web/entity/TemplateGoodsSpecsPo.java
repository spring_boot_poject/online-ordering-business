package com.qf.web.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateGoodsSpecsPo {

    private Integer templateId;

    private Integer goodsSpecsId;
}
