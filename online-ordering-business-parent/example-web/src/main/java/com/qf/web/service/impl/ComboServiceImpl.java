package com.qf.web.service.impl;
import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.qo.ComboImageRequestParams;
import com.qf.web.common.qo.ComboRequestParams;
import com.qf.web.common.utils.ConstantUtils;
import com.qf.web.common.vo.ComboImageVo;
import com.qf.web.common.vo.ComboVo;
import com.qf.web.entity.Combo;
import com.qf.web.entity.ComboImage;
import com.qf.web.mapper.ComboMapper;
import com.qf.web.service.ComboImageService;
import com.qf.web.service.ComboRuleService;
import com.qf.web.service.ComboService;
import com.qf.web.service.UploadService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author 51993
 */
@Service
public class ComboServiceImpl implements ComboService {

    @Autowired
    ComboMapper comboMapper;

    @Autowired
    UploadService uploadService;

    @Autowired
    ComboImageService comboImageService;

    @Autowired
    ComboRuleService comboRuleService;

    @Override
    public Integer addCombo(ComboRequestParams requestParams) {
        Combo combo = new Combo();
        //QO 转 PO
        BeanUtils.copyProperties(requestParams,combo);
        //添加套餐基本信息，并回填主键ID
        int res = comboMapper.insertCombo(combo);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }

        //拿到了主键ID添加套餐图片
        requestParams.getImages().stream().forEach(image -> {
            //让图片和套餐关联，修改图片的combo_id字段为当前套餐id
            image.setComboId(combo.getComboId());
            comboImageService.updateComboImage(image);
        });

        //让套餐和使用规则关联
        requestParams.getRuleIds().stream().forEach(id -> {
            comboRuleService.addComboRuleRelation(combo.getComboId(), id);
        });

        //让套餐和商品关联
        requestParams.getGoodsList().stream().forEach(goods -> {
            int result = comboMapper.insertComboGoods(goods.getGoodsId(), combo.getComboId(), goods.getGoodsNumber());
            if (result < 0){
                throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
            }
        });
        return res;
    }

    @Override
    public ComboImageVo addComboImage(MultipartFile multipartFile) {
        //先将图片上传到服务器上面，并且指定目录
        String imageUrl = uploadService.uploadImage(multipartFile, ConstantUtils.COMBO_IMAGE_BUCKET_NAME);
        //将图片存储到数据库中
        ComboImageRequestParams requestParams = new ComboImageRequestParams();
        requestParams.setImageUrl(imageUrl);
        ComboImageVo imageVo = comboImageService.addComboImage(requestParams);
        return imageVo;
    }

    @Override
    public List<ComboVo> queryAllComboByShop(Integer id) {
        List<ComboVo> comboList = comboMapper.selectComboByShop(id);
        return comboList;
    }

    @Override
    public List<ComboVo> getComboByShop(Integer id) {
        List<ComboVo> comboVoList = comboMapper.selectComboByShop(id);
        if (ObjectUtils.isEmpty(comboVoList)){
            throw new ServiceException(ResultCode.PARAMS_IS_INVALID);
        }
        return comboVoList;
    }

    @Override
    public Integer updateComboDetails(ComboRequestParams requestParams) {
        //先更新套餐的基本信息
        //QO 转成 PO对象
        Combo combo = new Combo();
        BeanUtils.copyProperties(requestParams,combo);
        Integer res = comboMapper.updateComboDetails(combo);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        //更新套餐的图片信息
        //先查询出当前套餐的图片
        List<ComboImageVo> comboImageVos = comboImageService.getImagesByCombo(requestParams.getComboId());

        //获取请求参数中图片的id集合
        List<Integer> requestIds = requestParams.getImages().stream().map(image -> image.getImageId()).collect(Collectors.toList());
        comboImageVos.forEach(comboImage -> {
            //如果请求参数中的图片不包含数据库中已有图片的id，则说明这个图片需要被删除
            //批量将需要删除的图片的is_delete设置为1
            if (!requestIds.contains(comboImage.getImageId())){
                comboImageService.removeComboImage(comboImage.getImageId());
            }
        });
        //更新图片信息
        requestParams.getImages().forEach(image -> {
            image.setComboId(combo.getComboId());
            comboImageService.updateComboImage(image);
        });

        //更新套餐的使用规则
        //直接删除数据库中指定comboId的中间关系
        comboRuleService.delComboRule(requestParams.getComboId());
        //添加套餐和使用规则的关系
        requestParams.getRuleIds().forEach(id -> {
            comboRuleService.addComboRuleRelation(combo.getComboId(),id);
        });

        //更新套餐的商品信息
        //删除数据库中套餐和商品的关联关系
        comboMapper.deleteComboGoodsRelation(requestParams.getComboId());
        //重新添加商品和套餐之间的关系
        requestParams.getGoodsList().forEach(goodsQo -> {
            comboMapper.insertComboGoods(goodsQo.getGoodsId(),combo.getComboId(),goodsQo.getGoodsNumber());
        });
        return res;
    }

    @Override
    public ComboVo getComboDetails(Integer id) {
        ComboVo comboVo = comboMapper.selectComboByComboId(id);
        if (ObjectUtils.isEmpty(comboVo)){
            throw new ServiceException(ResultCode.PARAMS_IS_INVALID);
        }
        return comboVo;
    }

    @Override
    public Integer delCombo(Integer comboId) {
        Integer res = comboMapper.updateComboToDel(comboId);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return res;
    }

    @Override
    public Integer updateComboEnable(ComboRequestParams requestParams) {
        Combo combo = new Combo();
        BeanUtils.copyProperties(requestParams,combo);
        Integer res = comboMapper.updateComboDetails(combo);
        if (res < 0){
            throw new ServiceException(ResultCode.BUSINESS_UNKNOW_ERROR);
        }
        return res;
    }

    @Override
    public List<ComboVo> searchCombo(ComboRequestParams requestParams) {
        //QO 转 PO
        Combo combo = new Combo();
        BeanUtils.copyProperties(requestParams,combo);
        List<ComboVo> comboVoList = comboMapper.selectComboByCondition(combo);
        return comboVoList;
    }
}
