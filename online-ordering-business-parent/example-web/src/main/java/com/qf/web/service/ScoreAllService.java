package com.qf.web.service;

import com.qf.web.common.vo.ScoreAllVo;
import com.qf.web.common.vo.ScoreCountVo;
import com.qf.web.common.vo.ScoreInfoVo;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface ScoreAllService {
    //1根据商店id查看所有评价信息
    List<ScoreAllVo> getScoreAllByShopId(int shopId);
    //2根据评论id查看评论详情
    ScoreInfoVo getScoreInfoByScoreId(int scoreId);
    //3根据店铺id查看未回复的所有评价
    List<ScoreAllVo> getScoreAllByShopIdNo(int shopId);

    //4根据评价id查询未回复的评论详情
    ScoreInfoVo getNoScoreInfoByScoreId(int scoreId);

    //5根据店铺id获取最近30天的平均分（店铺评分）
    ScoreAllVo getScoreAvg(int shopId);

    //6根据店铺id查看指定时间段的所有评论
    List<ScoreAllVo> getScoreAllTimeByShopId(@Param("shopId") int shopId, @Param("endDate") String endDate, @Param("beginDate") String beginDate);

    //7根据店铺id查看指定的星级评价（所有评价）
    List<ScoreAllVo> getScoreByShopId(@Param("shopId") int shopId, @Param("scoreStarId") int scoreStarId);

    //8根据店铺id查看指定的星级评价（未回复的评价）
    List<ScoreAllVo> getScoreNoByShopId(@Param("shopId") int shopId, @Param("scoreStarId") int scoreStarId);

    //9根据店铺id查看中差评的总数
    Integer getBadScoreCountByShopId(@Param("shopId") int shopId);

    //10根据店铺id获取对应的评论总数（好评和中差评）
    ScoreCountVo getScoreCountByShopId(int shopId);





}
