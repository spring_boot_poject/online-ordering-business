package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "trading_certificate")
public class TradingCertificate {
    /**
     * 主键
     */
    @TableId(value = "trading_certificate_id", type = IdType.INPUT)
    private Integer tradingCertificateId;

    /**
     * 营业执照编号
     */
    @TableField(value = "trading_certificate_number")
    private String tradingCertificateNumber;

    /**
     * 营业执照开始有效期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "trading_start_validity")
    private Date tradingStartValidity;

    /**
     * 营业执照结束有效期
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "trading_end_validity")
    private Date tradingEndValidity;

    /**
     * 营业执照正面图片url
     */
    @TableField(value = "trading_image_url")
    private String tradingImageUrl;

    /**
     * 店铺ID
     */
    @TableField(value = "shop_id")
    private Integer shopId;

    /**
     * 是否启用，1启用，0禁用，默认1
     */
    @TableField(value = "`enable`")
    private Integer enable;

    public static final String COL_TRADING_CERTIFICATE_ID = "trading_certificate_id";

    public static final String COL_TRADING_CERTIFICATE_NUMBER = "trading_certificate_number";

    public static final String COL_TRADING_START_VALIDITY = "trading_start_validity";

    public static final String COL_TRADING_END_VALIDITY = "trading_end_validity";

    public static final String COL_TRADING_IMAGE_URL = "trading_image_url";

    public static final String COL_SHOP_ID = "shop_id";

    public static final String COL_ENABLE = "enable";
}