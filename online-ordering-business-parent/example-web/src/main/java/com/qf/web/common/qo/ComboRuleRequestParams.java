package com.qf.web.common.qo;
import lombok.Data;

@Data
public class ComboRuleRequestParams {

    /**
     * 主键
     */
    private Integer ruleId;

    /**
     * 店铺ID，记录哪个店铺创建的规则
     */
    private Integer shopId;

    /**
     * 规则内容
     */
    private String ruleContext;
}
