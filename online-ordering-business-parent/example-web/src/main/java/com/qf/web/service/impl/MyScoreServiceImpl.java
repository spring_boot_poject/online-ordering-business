package com.qf.web.service.impl;

import com.qf.common.base.exception.ServiceException;
import com.qf.common.base.result.ResultCode;
import com.qf.web.common.vo.MyScoreVo;
import com.qf.web.mapper.MyScoreMapper;
import com.qf.web.service.MyScoreService;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.Resource;
import java.util.List;
@Service
public class MyScoreServiceImpl implements MyScoreService {
    @Resource
    MyScoreMapper myScoreMapper;

    //1根据店铺id获取我的所有评价（评价配送员）
    @Override
    public List<MyScoreVo> getMyScoreByShopId(int shopId) {
        List<MyScoreVo> myScoreVoList = myScoreMapper.selectMyScoreByShopId(1);
        if (!ObjectUtils.isEmpty(myScoreVoList)){
            return myScoreVoList;
        }else {
            throw new ServiceException(ResultCode.ERROR);
        }
    }
}
