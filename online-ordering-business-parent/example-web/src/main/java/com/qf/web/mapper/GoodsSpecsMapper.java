package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.GoodsSpecsPo;import com.qf.web.entity.TemplateGoodsSpecsPo;import com.qf.web.entity.GoodsSpecs;import java.util.List;

public interface GoodsSpecsMapper extends BaseMapper<GoodsSpecs> {

    List<GoodsSpecsPo> selectSpecsById(int goodsSpecsId);

    List<GoodsSpecsPo> selectSpecsByTemplateId(int templateId);


    Integer insertGoodSpecsTemplateRelation(List<TemplateGoodsSpecsPo> list);

    /**
     * 插入商品规格
     *
     * @param goodsSpecsPo
     * @return
     */
    Integer insertGoodsSpecs(GoodsSpecsPo goodsSpecsPo);

    /**
     * 根据店铺id查询对应的规格
     *
     * @return
     */
    List<GoodsSpecsPo> selectCommonGoodsSpecsList(Integer shopId);

    /**
     * 根据商品规格id修改规格基本信息
     *
     * @param goodsSpecsPo
     * @return
     */
    Integer updateGoodsSpecsById(GoodsSpecsPo goodsSpecsPo);

    /**
     * 为删除多个商品规格以及规格详细信息
     *
     * @param goodsSpecsPos
     * @return
     */
    Integer deleteGoodsSpecsById(List<GoodsSpecsPo> goodsSpecsPos);

    /**
     * 根据商品规格id删除规格模板中间表的信息
     *
     * @param goodsSpecsPos
     * @return
     */
    Integer deleteTemplateGoodsSpecsRelation(List<GoodsSpecsPo> goodsSpecsPos);


}