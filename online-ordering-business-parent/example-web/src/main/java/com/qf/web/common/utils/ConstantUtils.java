package com.qf.web.common.utils;

/**
 * @author 51993
 *
 * 键的前缀
 */
public class ConstantUtils {

    public static final String PHONE_LOGIN_KEY_PREFIX = "msg:login:phone:";
    public static final String PHONE_UPDATE_KEY_PREFIX = "msg:update:phone:";
    public static final String PAY_FORGET_KEY_PREFIX = "msg:pay:phone:";
    public static final String ACCOUNT_FORGET_KEY_PREFIX = "msg:account:phone:";
    public static final String COMBO_IMAGE_BUCKET_NAME = "image/combo";
    public static final String GOODS_IMAGE_BUCKET_NAME = "image/goods";
    public static final String SHOP_IMAGE_BUCKET_NAME = "image/shop";
    public static final String ENVIRONMENT_IMAGE_BUCKET_NAME = "image/environment";
    public static final String LEGAL_IMAGE_BUCKET_NAME_START = "image/legal/start";
    public static final String LEGAL_IMAGE_BUCKET_NAME_END = "image/legal/end";
    public static final String TRADING_IMAGE_BUCKET_NAME = "image/trading";
    public static final String CURRENT_SHOP_KEY_PREFIX = "shop:id:";

    /**
     * 缓存的key前缀统一定义
     */
    public static final String GOODS_CACHE_KEY_PREFIX = "shop:goods:";
    public static final String COMBO_CACHE_KEY_PREFIX = "shop:combo:";
    /**
     * 查询多个商品信息使用
     */
    public static final String GOODS_CACHE_MORE_KEY_PREFIX = "shop:goods:more:";

}
