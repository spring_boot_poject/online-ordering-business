package com.qf.web.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "legal_person")
public class LegalPerson {
    @TableId(value = "legal_person_id", type = IdType.INPUT)
    private Integer legalPersonId;

    /**
     * 性别，1男，0女
     */
    @TableField(value = "gender")
    private Integer gender;

    /**
     * 证件类型
     */
    @TableField(value = "certificate_type")
    private String certificateType;

    /**
     * 证件号码
     */
    @TableField(value = "id_number")
    private String idNumber;

    /**
     * 有效期开始时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "person_start_validity")
    private Date personStartValidity;

    /**
     * 有效期结束时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @TableField(value = "person_end_validity")
    private Date personEndValidity;

    /**
     * 是否启用，1启用，0禁用，默认1
     */
    @TableField(value = "`enable`")
    private Integer enable;

    /**
     * 证件反面url
     */
    @TableField(value = "reverse_image_url")
    private String reverseImageUrl;

    /**
     * 证件正面url
     */
    @TableField(value = "obverse_image_url")
    private String obverseImageUrl;

    public static final String COL_LEGAL_PERSON_ID = "legal_person_id";

    public static final String COL_GENDER = "gender";

    public static final String COL_CERTIFICATE_TYPE = "certificate_type";

    public static final String COL_ID_NUMBER = "id_number";

    public static final String COL_PERSON_START_VALIDITY = "person_start_validity";

    public static final String COL_PERSON_END_VALIDITY = "person_end_validity";

    public static final String COL_ENABLE = "enable";

    public static final String COL_REVERSE_IMAGE_URL = "reverse_image_url";

    public static final String COL_OBVERSE_IMAGE_URL = "obverse_image_url";
}