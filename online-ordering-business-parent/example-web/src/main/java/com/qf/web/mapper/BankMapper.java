package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.entity.Bank;

import java.util.List;

/**
 * @author 51993
 */
public interface BankMapper extends BaseMapper<Bank> {

    /**
     * 添加银行卡
     * @param bank
     * @return
     */
    Integer addBank(Bank bank);

    /**
     * 解绑银行卡
     * @param id
     * @return
     */
    Integer deleteBank(Integer id);

    /**
     * 查询店铺银行卡
     * @param id    店铺ID
     * @return
     */
    List<Bank> selectBankList(Integer id);

    /**
     * 更新银行卡
     * @param bank
     * @return
     */
    Integer updateBank(Bank bank);
}