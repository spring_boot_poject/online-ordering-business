package com.qf.web.service;

import com.qf.web.common.qo.ShopTypeRequestParams;
import com.qf.web.common.vo.ShopTypeVo;

public interface ShopTypeService {
    ShopTypeVo registerShopType(ShopTypeRequestParams shopTypeRequestParams);
}
