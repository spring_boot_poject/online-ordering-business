package com.qf.web.service;

import com.qf.web.common.qo.ShopRequestParams;
import com.qf.web.common.vo.ShopVo;

import java.util.List;

public interface ShopService {
    /**
     * 根据商家id查询商家店铺信息
     * @param shopRequestParams
     * @return
     */
    List<ShopVo> selectShopInfo(ShopRequestParams shopRequestParams);

    /**
     * 选择店铺类型
     */
    ShopVo selectShopType(ShopRequestParams shopRequestParams);

    /**
     * 根据店铺id来查找店铺的状态
     * @param shopRequestParams
     * @return
     */
    ShopVo selectShopStatus(ShopRequestParams shopRequestParams);

    /**
     * 根据店铺id来修改店铺的状态值
     * @param shopRequestParams
     * @return
     */
    Integer updateShopStatus(ShopRequestParams shopRequestParams);

    /**
     * 根据店铺id来修改店铺头像
     * @param shopRequestParams
     * @return
     */
    Integer updateShopImg(ShopRequestParams shopRequestParams);

    /**
     * 根据店铺id来查询店铺账户的余额
     * @return
     */
    ShopVo selectShopAccount(ShopRequestParams shopRequestParams);

    /**
     * 提现操作中店铺id和提现金钱来实现店铺余额减少
     * @return
     */
    Integer updateShopAccount(ShopRequestParams shopRequestParams);
}
