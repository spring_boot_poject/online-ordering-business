package com.qf.web.service;

import com.qf.web.common.vo.BankVo;
import com.qf.web.common.qo.BankRequestParams;

import java.util.List;

/**
 * @author 51993
 */
public interface BankService {

    /**
     * 绑定银行卡
     * @param requestParams
     * @return
     */
    Integer addBank(BankRequestParams requestParams);

    /**
     * 解绑银行卡
     * @param requestParams
     * @return
     */
    Integer unbind(BankRequestParams requestParams);

    /**
     * 查看店铺绑定的银行卡
     * @param id
     * @return
     */
    List<BankVo> selectBanks(Integer id);
}
