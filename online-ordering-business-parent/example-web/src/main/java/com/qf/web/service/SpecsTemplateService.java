package com.qf.web.service;


import com.qf.web.common.qo.SpecsTemplateRequestParams;
import com.qf.web.common.qo.TemplateGoodsSpecsRequestParams;
import com.qf.web.common.vo.SpecsTemplateVo;

import java.util.List;

public interface SpecsTemplateService {
    //展示默认通用的模板
    List<SpecsTemplateVo> showCommonSpecsTemplateList();
    //展示店铺规格模板
    public List<SpecsTemplateVo> showShopSpecsTemplateList(Integer shopId);
    //添加个人模板
    Integer insertTemplate(SpecsTemplateRequestParams specsTemplateRequestParams);
    //添加自定义模板
    public Integer insertTemplateCustom(SpecsTemplateRequestParams specsTemplateRequestParams);

    //根据规格模板id查询规格模板信息以及规格和规格详细信息
    public SpecsTemplateVo selectTSpecsTemplate(Integer templateId);
    //添加模板和规格的中间表关系
    public Integer addGoodsSpecsTemplateRelation(List<TemplateGoodsSpecsRequestParams> TemplateGoodsSpecsRequestParamsList);

    //修改规格模板信息
    public Integer updateTemplate(SpecsTemplateRequestParams specsTemplateRequestParams);

    //删除模板及相关信息
    public Integer deleteTemplate(List<Integer> templateIds);

    //添加自定义模板
    public Integer addUserTemplate(SpecsTemplateRequestParams specsTemplateRequestParams);
}
