package com.qf.web.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.qf.web.common.vo.ComboImageVo;
import com.qf.web.entity.ComboImage;

import java.util.List;

public interface ComboImageMapper extends BaseMapper<ComboImage> {
    /**
     * 添加套餐图片
     * @param comboImage
     * @return
     */
    Integer insertComboImage(ComboImage comboImage);

    /**
     * 修改图片信息
     * @param comboImage
     * @return
     */
    Integer updateComboImage(ComboImage comboImage);

    /**
     * 查询指定套餐的图片
     * @param id    套餐ID
     * @return
     */
    List<ComboImage> selectImgByCombo(Integer id);

    /**
     * 根据套餐ID将图片设置为已删除状态
     * @param id
     * @return
     */
    Integer updateComboImageByCombo(Integer id);
}