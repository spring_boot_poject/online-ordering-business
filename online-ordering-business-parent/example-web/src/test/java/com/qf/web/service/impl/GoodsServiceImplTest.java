package com.qf.web.service.impl;

import com.qf.web.common.vo.GetGoodsInfoVo;
import com.qf.web.common.vo.ShopGoodsVo;
import com.qf.web.entity.GetGoodsInfoPo;
import com.qf.web.entity.GoodsPo;
import com.qf.web.mapper.GoodsMapper;
import com.qf.web.service.GoodsService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.ValueOperations;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

@SpringBootTest
@Slf4j
class GoodsServiceImplTest {
    @Resource
    private GoodsMapper goodsMapper;

    @Resource
    private GoodsService goodsService;

    @Resource
    private ValueOperations<String,Object> valueOperations;


    @Test
    void selectGoodsInfoByGoodsId() {
        int id = 1;
        List<GetGoodsInfoVo> list = goodsMapper.selectGoodsInfoByGoodsId(id);
        log.info("结果为："+list);
    }

    @Test
    void selectGoodsSoldOutInfoByGoodsInfo() {
        int id = 1;
        List<GetGoodsInfoVo> list = goodsMapper.selectGoodsSoldOutInfoByGoodsInfo(id);
        log.info("结果为："+list);
    }

    @Test
    void addGoodsRedis(){
        GoodsPo goodsPo = new GoodsPo();
        goodsPo.setGoodsId(4);
        valueOperations.setIfAbsent("add:goodsInfo",goodsPo,2,TimeUnit.MINUTES);
    }

    @Test
    void addGoodsTimeRedis(){
        GoodsPo goodsPo = new GoodsPo();
        goodsPo.setGoodsId(5);
        valueOperations.setIfPresent("add:goodsInfo",goodsPo,2,TimeUnit.MINUTES);
    }

    @Test
    void select(){
        List<ShopGoodsVo> list = goodsService.getAllGoodsInfoByShopId(1);
        log.info("结果为："+list);
    }

}