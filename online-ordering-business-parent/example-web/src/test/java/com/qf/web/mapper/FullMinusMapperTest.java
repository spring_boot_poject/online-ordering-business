package com.qf.web.mapper;

import com.qf.web.WebApp;
import com.qf.web.common.vo.FullAnalysisVo;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = WebApp.class)
class FullMinusMapperTest {

    @Resource
    FullMinusMapper fullMinusMapper;
    @Test
    void selectDayFull() {
        FullAnalysisVo fullAnalysisVo = fullMinusMapper.selectDayFull(1,0);
        System.out.println(fullAnalysisVo);
    }
}