package com.qf.web.mapper;

import com.qf.web.entity.SpecsPo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
@Slf4j
class SpecsMapperTest {

    @Resource
    SpecsMapper specsMapper;

    @Test
    public void testInsertSpecs(){
        SpecsPo specsPo = new SpecsPo();
        specsPo.setSpecsInfo("asd");
        specsPo.setGoodsSpecsId(1);
        Integer integer = specsMapper.inserspecs(specsPo);
        log.info(integer.toString());
    }



}