package com.qf.web.mapper;

import com.qf.web.entity.ActiveOrder;
import com.qf.web.entity.Coupon;
import com.qf.web.entity.CouponInfo;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.SimpleFormatter;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class CouponMapperTest {

    @Resource
    CouponMapper couponMapper;

    @Test
    void testSelectAllCoupon() {
        List<Coupon> list = couponMapper.selectAllCoupon(1);
        list.forEach(item->log.info(item.toString()));
    }

    @Test
    public void testSelectCouponInfoByShopId(){
        List<CouponInfo> couponInfos = couponMapper.selectCouponInfoByShopId(1);
        couponInfos.forEach(item->log.info(item.toString()));
    }
    @Test
    public void testSelectActiveOrdersCountByShopIdAndOrderDate(){
        ActiveOrder activeOrder = couponMapper.selectActiveOrdersCountByShopIdAndOrderDate(1, "2022-03-30");
        log.info(activeOrder.toString());
    }

    @Test
    public void testSelectOrderInfoByShopIdAndOrderTime(){
        ActiveOrder activeOrder = couponMapper.selectOrderInfoByShopIdAndOrderTime(1, "2022-03-30");
        log.info(activeOrder.toString());
    }

    @Test
    public void testSelectActiveOrderInfoByShopIdAndNearTime(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -6);
        String before = format.format(c.getTime())+" 00:00:00";

        Date date = new Date();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTime = format1.format(date);


        ActiveOrder activeOrder = couponMapper.selectActiveOrderInfoByShopIdAndNearTime(1, before,nowTime);
        log.info(activeOrder.toString());
    }

    @Test
    public void testSelectOrderInfoByShopIdAndNearTime(){
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -6);
        String before = format.format(c.getTime())+" 00:00:00";

        Date date = new Date();
        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String nowTime = format1.format(date);


        ActiveOrder activeOrder = couponMapper.selectOrderInfoByShopIdAndNearTime(1, before,nowTime);
        log.info(activeOrder.toString());
    }

}