package com.qf.web.mapper;

import com.qf.web.common.vo.ComboVo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
@Slf4j
public class ComboMapperTest {

    @Autowired
    ComboMapper comboMapper;

    @Test
    public void testSelectComboByShop(){
        List<ComboVo> comboList = comboMapper.selectComboByShop(1);
        comboList.stream().forEach(comboVo -> {
            log.info("comboVO:{}",comboVo.toString());
        });
    }

}
