package com.qf.web.mapper;

import com.qf.web.entity.GoodsSpecsPo;
import com.qf.web.entity.SpecsTemplatePo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.List;

@Slf4j
@SpringBootTest
class SpecsTemplateMapperTest {

    @Resource
    SpecsTemplateMapper specsTemplateMapper;

    @Resource
    GoodsSpecsMapper goodsSpecsMapper;

    @Test
    public void testSelectSpecsTemplate() {
        SpecsTemplatePo specsTemplatePo = specsTemplateMapper.selectSpecsTemplateByTemolateId(1);
       log.info(specsTemplatePo.toString());

    }
    @Test
    public void testSelectSpecsById() {
        List<GoodsSpecsPo> list = goodsSpecsMapper.selectSpecsById(1);
        list.forEach(item->log.info(item.toString()));
    }

    @Test
    public void testSelectSpecsTemplateAndGoodsSpecs() {
        List<SpecsTemplatePo> list = specsTemplateMapper.selectSpecsTemplateAndGoodsSpecs(1);
        list.forEach(item->log.info(item.toString()));
    }


    @Test
    public void testSelectSpecsTemplateByShopId() {
        List<SpecsTemplatePo> list = specsTemplateMapper.selectSpecsTemplateByShopId(0);
        list.forEach(item->log.info(item.toString()));

    }
    @Test
    public void testinsertTemplate() {
        SpecsTemplatePo specsTemplatePo = new SpecsTemplatePo();
        specsTemplatePo.setSpecsTemplateName("asd");
        specsTemplatePo.setShopId(3);
        Integer integer = specsTemplateMapper.insertTemplate(specsTemplatePo);
        log.info(integer.toString());
        log.info(specsTemplatePo.getSpecsTemplateId().toString());
    }

}