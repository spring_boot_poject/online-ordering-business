package com.qf.web.mapper;


import com.qf.web.entity.GoodsSpecsPo;
import com.qf.web.entity.TemplateGoodsSpecsPo;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@Slf4j
public class GoodSpecsMapperTest {

    @Resource
    GoodsSpecsMapper goodsSpecsMapper;

    @Test
    public void testSelectSpecsByTemplateId(){
        List<GoodsSpecsPo> list = goodsSpecsMapper.selectSpecsByTemplateId(1);
        list.forEach(item->log.info(item.toString()));
    }

    @Test
    public void testInsertGoodSpecsTemplateRelation(){
        ArrayList<TemplateGoodsSpecsPo> list = new ArrayList<>();
        list.add(new TemplateGoodsSpecsPo(3,1));
        list.add(new TemplateGoodsSpecsPo(3,2));
        Integer integer = goodsSpecsMapper.insertGoodSpecsTemplateRelation(list);
        log.info(integer.toString());
    }

    @Test
    public void testInsertGoodsSpecs(){
        GoodsSpecsPo goodsSpecsPo = new GoodsSpecsPo();
        goodsSpecsPo.setGoodsSpecsName("qa");
        Integer integer = goodsSpecsMapper.insertGoodsSpecs(goodsSpecsPo);
        log.info(integer.toString());
        log.info(goodsSpecsPo.getGoodsSpecsId().toString());
    }

    @Test
    public void testSelectCommonGoodsSpecsList(){
        List<GoodsSpecsPo> list = goodsSpecsMapper.selectCommonGoodsSpecsList(0);
        list.forEach(item->log.info(item.toString()));
    }

    @Test
    public void testDeleteTemplateGoodsSpecsRelation(){
        GoodsSpecsPo goodsSpecsPo = new GoodsSpecsPo();
        GoodsSpecsPo goodsSpecsPo1 = new GoodsSpecsPo();
        ArrayList<GoodsSpecsPo> goodsSpecsPos = new ArrayList<>();
        goodsSpecsPo.setGoodsSpecsId(21);
        goodsSpecsPo1.setGoodsSpecsId(23);
        goodsSpecsPos.add(goodsSpecsPo);
        goodsSpecsPos.add(goodsSpecsPo1);
        Integer integer = goodsSpecsMapper.deleteTemplateGoodsSpecsRelation(goodsSpecsPos);
        log.info(integer.toString());
    }
}
