package com.qf.web.mapper;

import com.qf.web.entity.GoodsPicture;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
@Slf4j
public class GoodsPictureMapperTest {
    @Resource
    private GoodsPictureMapper goodsPictureMapper;
    @Test
    public void testInsertPicture(){
        GoodsPicture goodsPicture = new GoodsPicture();
        goodsPicture.setGoodsId(2);
        goodsPicture.setGoodsPictureImg("321");
        int count = goodsPictureMapper.insertPicture(goodsPicture);
        Integer goodsPictureId = goodsPicture.getGoodsPictureId();
        log.info("影响行数为："+count);
        log.info("主图片的Id为："+goodsPictureId);
    }
}
