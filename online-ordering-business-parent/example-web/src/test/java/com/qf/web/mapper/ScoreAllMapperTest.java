package com.qf.web.mapper;

import com.qf.web.entity.Score;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@Slf4j
class ScoreAllMapperTest {

    @Resource
    ScoreAllMapper scoreAllMapper;
    @Resource
    ScoreMapper scoreMapper;

//    @Test
//    void checkScoreAllByShopId() {
//        List<ScoreAllPo> scoreAllPo = scoreAllMapper.checkScoreAllByShopId(1);
//        scoreAllPo.forEach(item ->log.info(item.toString()));
//    }
//
//    @Test
//    void selectScoreInfoByScoreId(){
//        ScoreInfoPo scoreInfoPo = scoreAllMapper.selectScoreInfoByScoreId(2);
//        System.out.println(scoreInfoPo);
//    }

    @Test
    void updateReplyMessageByScoreId() {
        Score score = new Score();
        score.setScoreId(1);
        score.setReplyMessageState(1);
        score.setReplyMessage("hhhhh");
        Integer integer = scoreMapper.updateReplyMessageByScoreId(score);
        if (integer==0){
            System.out.println("失败");
        }else {
            System.out.println("成功");
        }
    }

}